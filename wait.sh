#!/bin/bash

printLogAndExit() {
    cat fx.log
    return 1
}

nohup java $JACOCO_ARGS -jar target/fx-0.0.1-SNAPSHOT.jar &> fx.log &

timeout 120 bash -c '
    while [[ "$(curl -s -o /dev/null -w ''%{http_code}'' localhost:8080/api/system/status)" != "200" ]]; do
        echo "Waiting server up..."
        sleep 5
    done
    echo "Server is up!"
' || printLogAndExit