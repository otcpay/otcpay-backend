java -jar src/main/resources/lib/org.jacoco.cli-0.8.6-nodeps.jar merge target/jacoco.exec target/jacoco-e2e.exec --destfile jacoco.exec
java -jar src/main/resources/lib/org.jacoco.cli-0.8.6-nodeps.jar report jacoco.exec --html target/coverage --classfiles target/classes/com/otcpay/fx/FxApplication.class --classfiles target/classes/com/otcpay/fx/util --classfiles target/classes/com/otcpay/fx/config --classfiles target/classes/com/otcpay/fx/service --classfiles target/classes/com/otcpay/fx/rest --classfiles target/classes/com/otcpay/fx/exception --sourcefiles src/main/java
coverage=$(cat target/coverage/index.html | grep -o 'Total[^%]*%' | grep -o '[^ ]*%'| sed 's/class="ctr2">//g')
echo 'Total: '$coverage
