/* eslint-disable no-unused-vars */
const fs = require('fs');
const path = require('path');
const frisby = require('frisby');

const { SERVER_API, setup } = require('./common');

setup();

describe('Other REST endpoint', () => {
  it('System Endpints', () => {
    const promiseArray = [];

    promiseArray.push(
      frisby.setup({
        request: {
          headers: {
            'Content-Type': 'application/json',
            Authorization: 'token',
          },
        },
      }, true).get(`${SERVER_API}/system/info`)
        .expect('status', 200),

      frisby.setup({
        request: {
          headers: {
            'Content-Type': 'application/json',
            Authorization: 'token',
          },
        },
      }, true).get(`${SERVER_API}/system/incident`)
        .expect('status', 200)
        .expect('jsonTypes', '*', {
          begin: frisby.Joi.string(),
          created: frisby.Joi.string(),
          end: frisby.Joi.string(),
          externalDesc: frisby.Joi.string(),
          modified: frisby.Joi.string(),
          number: frisby.Joi.number(),
          serviceKey: frisby.Joi.string(),
          serviceName: frisby.Joi.string(),
          source: frisby.Joi.string(),
          severity: frisby.Joi.string(),
          url: frisby.Joi.string(),
        }),
    );

    return Promise.all(promiseArray);
  }, 120000);
});
