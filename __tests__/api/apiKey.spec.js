/* eslint-disable no-unused-vars */
const frisby = require('frisby');

const { SERVER_API, setup } = require('./common');

setup();

const resource = 'apiKey';

let id = '';

describe('API Key REST endpoint', () => {
  it('POST requst with success', () => frisby.setup({
    request: {
      headers: {
        'Content-Type': 'application/json',
        Authorization: 'token',
      },
    },
  }, true)
    .post(`${SERVER_API}/v1/${resource}`, {
      data: 'test-data',
    })
    .expect('status', 200)
    .then((res) => {
      expect(res.json.id).not.toBe('');
      expect(res.json.revoked).toBe(false);
      expect(res.json.key).not.toBe('');
      expect(res.json.updatedDate).not.toBe('');
      expect(res.json.createdDate).not.toBe('');
      id = res.json.id;
    }));

  it('GET, UPDATE requst with success', () => frisby.setup({
    request: {
      headers: {
        'Content-Type': 'application/json',
        Authorization: 'token',
      },
    },
  }, true)
    .get(`${SERVER_API}/v1/${resource}/${id}`)
    .expect('status', 200)
    .then((res) => {
      expect(res.json.revoked).toBe(false);

      return frisby.setup({
        request: {
          headers: {
            'Content-Type': 'application/json',
            Authorization: 'token',
          },
        },
      }, true)
        .put(`${SERVER_API}/v1/${resource}/${id}`)
        .expect('status', 200)
        .expect('json', 'result', true)
        .then((r) => frisby.setup({
          request: {
            headers: {
              'Content-Type': 'application/json',
              Authorization: 'token',
            },
          },
        }, true)
          .get(`${SERVER_API}/v1/${resource}/${id}`)
          .expect('status', 200)
          .expect('json', 'revoked', true));
    }));

  it('DELETE request with success', () => frisby.setup({
    request: {
      headers: {
        'Content-Type': 'application/json',
        Authorization: 'token',
      },
    },
  }, true)
    .delete(`${SERVER_API}/v1/${resource}/${id}`)
    .expect('status', 200)
    .expect('json', 'result', true)
    .then((innerR) => frisby.setup({
      request: {
        headers: {
          'Content-Type': 'application/json',
          Authorization: 'token',
        },
      },
    }, true)
      .get(`${SERVER_API}/v1/${resource}/${id}`)
      .expect('status', 404)
      .expect('json', 'message', 'Cannot find Api Key')));
});
