/* eslint-disable no-unused-vars */
const frisby = require('frisby');

const { SERVER_API, setup } = require('./common');

setup();

const resource = 'qrCode';

let id = '';

describe('QR Code REST endpoint', () => {
  it('POST request with success', () => frisby.setup({
    request: {
      headers: {
        'Content-Type': 'application/json',
        Authorization: 'token',
      },
    },
  }, true)
    .post(`${SERVER_API}/v1/${resource}`)
    .expect('status', 200)
    .then((res) => {
      expect(res.json.id).not.toBe('');
      expect(res.json.updatedDate).not.toBe('');
      expect(res.json.createdDate).not.toBe('');
      expect(res.json.expiredDate).not.toBe('');
      id = res.json.id;
    }));

  it('GET request with success', () => frisby.setup({
    request: {
      headers: {
        'Content-Type': 'application/json',
        Authorization: 'token',
      },
    },
  }, true)
    .get(`${SERVER_API}/v1/${resource}/${id}`)
    .expect('status', 200)
    .then((res) => {
      expect(res.json.id).not.toBe('');
      expect(res.json.updatedDate).not.toBe('');
      expect(res.json.createdDate).not.toBe('');
      expect(res.json.expiredDate).not.toBe('');
      id = res.json.id;
    }));

  it('PUT request with success', () => frisby.setup({
    request: {
      headers: {
        'Content-Type': 'application/json',
        Authorization: 'token',
      },
    },
  }, true)
    .put(`${SERVER_API}/v1/${resource}/${id}`)
    .expect('status', 200)
    .expect('json', 'result', false));

  it('DELETE request with success', () => frisby.setup({
    request: {
      headers: {
        'Content-Type': 'application/json',
        Authorization: 'token',
      },
    },
  }, true)
    .delete(`${SERVER_API}/v1/${resource}/${id}`)
    .expect('status', 200)
    .expect('json', 'result', true));
});
