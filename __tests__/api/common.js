const frisby = require('frisby');

const SERVER_API = 'http://localhost:8080/api';

const setup = () => {
  frisby.globalSetup({
    timeout: 60000,
    request: {
      timeout: 60000,
      inspectOnFailure: true,
      inspectRequestOnFailure: true,
    },
  });
};

module.exports = {
  SERVER_API,
  setup,
};
