/* eslint-disable no-unused-vars */
const frisby = require('frisby');

const { SERVER_API, setup } = require('./common');

setup();

const resource = 'users';
const token = 'eyJhbGciOiJSUzI1NiIsImtpZCI6IjY5NmFhNzRjODFiZTYwYjI5NDg1NWE5YTVlZTliODY5OGUyYWJlYzEiLCJ0eXAiOiJKV1QifQ.eyJuYW1lIjoid2FpIGtldW5nIHdvbmciLCJwaWN0dXJlIjoiaHR0cHM6Ly9saDMuZ29vZ2xldXNlcmNvbnRlbnQuY29tL2EtL0FPaDE0R2lfY1JuSFJFMHpPcEhiRVpnX2NkQWFBRmZlN2JNMnZPTnFlV2h4NHM4PXM5Ni1jIiwiaXNzIjoiaHR0cHM6Ly9zZWN1cmV0b2tlbi5nb29nbGUuY29tL3Bhd24tZGV2LTEwMWM0IiwiYXVkIjoicGF3bi1kZXYtMTAxYzQiLCJhdXRoX3RpbWUiOjE2MDc5MTk1ODAsInVzZXJfaWQiOiI0bzZWMTVMWW1WY012b3lWTVIzUkxyblprYmgyIiwic3ViIjoiNG82VjE1TFltVmNNdm95Vk1SM1JMcm5aa2JoMiIsImlhdCI6MTYwNzkxOTU4MCwiZXhwIjoxNjA3OTIzMTgwLCJlbWFpbCI6ImFpZXNlYy52aXRhQGdtYWlsLmNvbSIsImVtYWlsX3ZlcmlmaWVkIjp0cnVlLCJmaXJlYmFzZSI6eyJpZGVudGl0aWVzIjp7Imdvb2dsZS5jb20iOlsiMTA2OTE3MjE3NzIyMzk5MTI4NzMyIl0sImVtYWlsIjpbImFpZXNlYy52aXRhQGdtYWlsLmNvbSJdfSwic2lnbl9pbl9wcm92aWRlciI6Imdvb2dsZS5jb20ifX0.XNI0oZNePc-We7AsDyu__zZqGXemz3AM1xE54HDEPTUIMgOtNzVZDWbpxB5cVFhYlrzYtloLKgQ0o260VuOTTrNvkzm0h4M-O6nq6-7Ra-IFnCqoFrFkuVyaPgh_V5oEN9KG5ru_Bic5LbOmM8ocwDIaLwVZbVzsfxcGEJwjtLJPD8GSfcEoJQSV9eIPVlqzfoKSH5CpDVyD9qsE-FT2x6qTedP7mrIKeWg4vxoBKm4ZR26MTvkP6bPuQ7rb_514QJ-nY3YO0fgmX-_pOtp01p7Gr6A9qvBx_5rhTjmMd1HDNq9on8OVVJ5kguI0ShYIHi5x2ekEohNwQyzf76slBw';

describe('User REST endpoint', () => {
  it('Authentication request with failure', () => {
    const promiseArray = [];

    promiseArray.push(
      frisby.setup({
        request: {
          headers: {
            'Content-Type': 'application/json',
          },
        },
      }, true)
        .post(`${SERVER_API}/authenticate?provider=GOOGLE`, {
          token,
        })
        .expect('status', 400)
        .expect('json', 'result', false)
        .expect('json', 'message', 'Please provide both username and password, or Id Token with user detail'),

      frisby.setup({
        request: {
          headers: {
            'Content-Type': 'application/json',
          },
        },
      }, true)
        .post(`${SERVER_API}/authenticate?provider=GOOGLE`, {
          token,
          user: {},
        })
        .expect('status', 401)
        .expect('json', 'result', false)
        .expect('json', 'message', 'User is not valid'),

      frisby.setup({
        request: {
          headers: {
            'Content-Type': 'application/json',
          },
        },
      }, true)
        .post(`${SERVER_API}/authenticate?provider=GOOGLE`, {
          password: token,
          username: 'test@test.com',
        })
        .expect('status', 500)
        .expect('json', 'result', false)
        .expect('json', 'message', 'Exception occured during runtime. For error detail, please check log.'),
    );

    return Promise.all(promiseArray);
  });

  it('GET request with failure', () => {
    const promiseArray = [];

    promiseArray.push(
      frisby.get(`${SERVER_API}/v1/${resource}`)
        .expect('status', 400),

      frisby.setup({
        request: {
          headers: {
            'Content-Type': 'application/json',
            Authorization: 'token',
          },
        },
      }, true)
        .get(`${SERVER_API}/v1/${resource}`)
        .expect('status', 400),

      frisby.get(`${SERVER_API}/v1/${resource}/test-id`)
        .expect('status', 400),

      frisby.setup({
        request: {
          headers: {
            'Content-Type': 'application/json',
            Authorization: 'token',
          },
        },
      }, true)
        .get(`${SERVER_API}/v1/${resource}/test-id`)
        .expect('status', 404)
        .expect('json', 'result', false)
        .expect('json', 'message', 'Resource not found'),

      frisby.get(`${SERVER_API}/v1/user`)
        .expect('status', 400),

      frisby.setup({
        request: {
          headers: {
            'Content-Type': 'application/json',
            Authorization: 'token',
          },
        },
      }, true)
        .get(`${SERVER_API}/v1/user`)
        .expect('status', 400),

      frisby.setup({
        request: {
          headers: {
            'Content-Type': 'application/json',
            Authorization: 'token',
          },
        },
      }, true)
        .get(`${SERVER_API}/v1/user?id=test-id`)
        .expect('status', 400),

      frisby.setup({
        request: {
          headers: {
            'Content-Type': 'application/json',
            Authorization: 'token',
          },
        },
      }, true)
        .get(`${SERVER_API}/v1/user?fields=id,name`)
        .expect('status', 400),

      frisby.setup({
        request: {
          headers: {
            'Content-Type': 'application/json',
            Authorization: 'token',
          },
        },
      }, true)
        .get(`${SERVER_API}/v1/${resource}?page=1&size=1&fields=id,name,`)
        .expect('status', 400)
        .expect('json', 'result', false)
        .expect('json', 'message', 'Custom field(s) must be a single field or a list separated by comma'),

      frisby.setup({
        request: {
          headers: {
            'Content-Type': 'application/json',
            Authorization: 'token',
          },
        },
      }, true)
        .get(`${SERVER_API}/v1/user?id=test-id&fields=,id,name`)
        .expect('status', 400)
        .expect('json', 'result', false)
        .expect('json', 'message', 'Custom field(s) must be a single field or a list separated by comma'),

      frisby.setup({
        request: {
          headers: {
            'Content-Type': 'application/json',
            Authorization: 'token',
          },
        },
      }, true)
        .get(`${SERVER_API}/v1/user?id=test-id&fields=id,name,`)
        .expect('status', 400)
        .expect('json', 'result', false)
        .expect('json', 'message', 'Custom field(s) must be a single field or a list separated by comma'),

      frisby.setup({
        request: {
          headers: {
            'Content-Type': 'application/json',
            Authorization: 'token',
          },
        },
      }, true)
        .get(`${SERVER_API}/v1/user?id=test-id&fields=id,name`)
        .expect('status', 404)
        .expect('json', 'result', false)
        .expect('json', 'message', 'Resource not found'),

      frisby.setup({
        request: {
          headers: {
            'Content-Type': 'application/json',
            Authorization: 'token',
          },
        },
      }, true)
        .get(`${SERVER_API}/v1/${resource}?page=1&size=1&fields=id,field&keywords=test-id`)
        .expect('status', 400)
        .expect('json', 'result', false)
        .expect('json', 'message', 'Request fields not exist'),
    );

    return Promise.all(promiseArray);
  });

  it('POST request with failure', () => {
    const promiseArray = [];

    promiseArray.push(
      frisby.post(`${SERVER_API}/v1/${resource}`)
        .expect('status', 400),

      frisby.setup({
        request: {
          headers: {
            'Content-Type': 'application/json',
            Authorization: 'token',
          },
        },
      }, true)
        .post(`${SERVER_API}/v1/${resource}`, {})
        .expect('status', 400),
    );

    return Promise.all(promiseArray);
  });

  it('PUT request with failure', () => {
    const promiseArray = [];

    promiseArray.push(
      frisby.put(`${SERVER_API}/v1/${resource}/test-id`)
        .expect('status', 400),

      frisby.setup({
        request: {
          headers: {
            'Content-Type': 'application/json',
            Authorization: 'token',
          },
        },
      }, true)
        .put(`${SERVER_API}/v1/${resource}/test-id`, {})
        .expect('status', 400)
        .expect('json', 'result', false)
        .expect('json', 'message', 'Payload cannot be empty'),

      frisby.setup({
        request: {
          headers: {
            'Content-Type': 'application/json',
            Authorization: 'token',
          },
        },
      }, true)
        .put(`${SERVER_API}/v1/${resource}/test-id`, { notExistField: '' })
        .expect('status', 400)
        .expect('json', 'result', false)
        .expect('json', 'message', 'Request fields not exist'),

      frisby.setup({
        request: {
          headers: {
            'Content-Type': 'application/json',
            Authorization: 'token',
          },
        },
      }, true)
        .put(`${SERVER_API}/v1/${resource}/test-id`, { id: '' })
        .expect('status', 403)
        .expect('json', 'result', false)
        .expect('json', 'message', 'Request fields are forbidden to modify'),

      frisby.setup({
        request: {
          headers: {
            'Content-Type': 'application/json',
            Authorization: 'token',
          },
        },
      }, true)
        .put(`${SERVER_API}/v1/${resource}?ids=test-id`, { id: '' })
        .expect('status', 403)
        .expect('json', 'result', false)
        .expect('json', 'message', 'Request fields are forbidden to modify'),

      frisby.setup({
        request: {
          headers: {
            'Content-Type': 'application/json',
            Authorization: 'token',
          },
        },
      }, true)
        .put(`${SERVER_API}/v1/${resource}?ids=test-id`, {})
        .expect('status', 400)
        .expect('json', 'result', false)
        .expect('json', 'message', 'Payload cannot be empty'),

      frisby.setup({
        request: {
          headers: {
            'Content-Type': 'application/json',
            Authorization: 'token',
          },
        },
      }, true)
        .put(`${SERVER_API}/v1/${resource}?ids=test-id`, { notExistField: '' })
        .expect('status', 400)
        .expect('json', 'result', false)
        .expect('json', 'message', 'Request fields not exist'),

      frisby.setup({
        request: {
          headers: {
            'Content-Type': 'application/json',
            Authorization: 'token',
          },
        },
      }, true)
        .put(`${SERVER_API}/v1/${resource}/test-id`, { username: 'abc' })
        .expect('status', 200)
        .expect('json', 'result', false),
    );

    return Promise.all(promiseArray);
  });

  it('DELETE request with failure', () => {
    const promiseArray = [];

    promiseArray.push(
      frisby.delete(`${SERVER_API}/v1/${resource}/test-id`)
        .expect('status', 400),

      frisby.setup({
        request: {
          headers: {
            'Content-Type': 'application/json',
            Authorization: 'token',
          },
        },
      }, true)
        .delete(`${SERVER_API}/v1/${resource}/test-id`)
        .expect('status', 200)
        .expect('json', 'result', false),

      frisby.setup({
        request: {
          headers: {
            'Content-Type': 'application/json',
            Authorization: 'token',
          },
        },
      }, true)
        .delete(`${SERVER_API}/v1/${resource}?ids=test-id`)
        .expect('status', 200)
        .expect('json', 'result', false),
    );

    return Promise.all(promiseArray);
  });
});
