package com.otcpay.fx.service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutionException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import com.google.api.core.ApiFuture;
import com.google.cloud.firestore.DocumentReference;
import com.google.cloud.firestore.DocumentSnapshot;
import com.google.cloud.firestore.QueryDocumentSnapshot;
import com.google.cloud.firestore.QuerySnapshot;
import com.google.cloud.firestore.WriteResult;
import com.otcpay.fx.exception.RestOperationException;
import com.otcpay.fx.model.User;
import com.otcpay.fx.model.common.Field;
import com.otcpay.fx.model.common.search.PaginatedRequest;
import com.otcpay.fx.model.common.search.Searchable;
import com.otcpay.fx.model.internal.Statistic;
import com.otcpay.fx.model.secret.jwt.JwtResponse;
import com.otcpay.fx.service.common.async.AsyncNotifiableService;
import com.otcpay.fx.service.internal.WebSocketService;
import com.otcpay.fx.util.ErrorUtil;
import com.otcpay.fx.util.TimeUtil;
import com.otcpay.fx.util.UuidUtil;
import com.otcpay.fx.util.secret.impl.JwtTokenUtil;

@Service
public class UserService extends AsyncNotifiableService<User> {
	private static final Logger LOG = LoggerFactory.getLogger(UserService.class);

	public static final String COL_NAME = "Users";

	private static final String USERNAME = "username";
	private static final String ACCOUNT = "account";
	protected static final String DEFAULT_BANK = "defaultBanks";
	protected static final String DEFAULT_WALLET = "defaultWallets";
	protected static final String TYPE = "type";;
	public static final String PASSWORD = "password";
	public static final String NOTIFICATION_TOKEN = "notificationToken";

	private static final String SOCKET_MESSAGE = "New User Joined - ";

	@Autowired
	private WebSocketService webSocketService;

	@Autowired
	private JwtTokenUtil jwtTokenUtil;

	@Autowired
	private BCryptPasswordEncoder encoder;

	private int cachedCount = 0;

	@Override
	public String getCollectionName() {
		return COL_NAME;
	}

	@Override
	public User create(User user) {
		try {
			QuerySnapshot snapshot = getFireStore().collection(getCollectionName())
					.whereEqualTo(ACCOUNT, user.getAccount()).get().get();
			if (!snapshot.isEmpty()) {
				throw RestOperationException.createError(
						String.format("User with account [%s] already created", user.getAccount()),
						HttpStatus.CONFLICT);
			}

			populateFields(user);
			ApiFuture<WriteResult> result = getFireStore().collection(getCollectionName()).document(user.getId())
					.create(user);

			LOG.info("User [{}] created, update time: {}", user.getId(), result.get().getUpdateTime());
			updateCount(getCollectionName(), Statistic.COUNTMODE.INCREASE);
			sendNotification(user);

			return user;
		} catch (InterruptedException | ExecutionException e) {
			LOG.error(ErrorUtil.getErrorTitle("UserService.create"), e);
		}

		return null;
	}

	@Override
	public boolean update(String id, Map<String, Object> user) {
		user.put(Field.UPDATED_DATE, TimeUtil.currentTime());
		ApiFuture<WriteResult> result = getFireStore().collection(getCollectionName()).document(id).update(user);
		try {
			LOG.info("User [{}] updated, update time: {}", id, result.get().getUpdateTime());
			return true;
		} catch (InterruptedException | ExecutionException e) {
			LOG.error(ErrorUtil.getErrorTitle("UserService.update"), e);
		}

		return false;
	}

	@Override
	public User readById(String id) {
		DocumentReference ref = getFireStore().collection(getCollectionName()).document(id);
		ApiFuture<DocumentSnapshot> future = ref.get();
		try {
			DocumentSnapshot document = future.get();
			if (document.exists()) {
				return document.toObject(User.class);
			}
		} catch (InterruptedException | ExecutionException e) {
			LOG.error(ErrorUtil.getErrorTitle("UserService.readById"), e);
		}

		return null;
	}

	@Override
	public List<Map<String, Object>> readAll(int page, int size, String fields, String keywords, String orderBy,
			String direction) {
		return getCustomData(getCollectionName(),
				PaginatedRequest.builder().page(page).size(size).fields(fields).keywords(keywords).orderBy(orderBy)
						.direction(direction).searchableFields(getSearchableFields()).build());
	}

	@Override
	public boolean delete(String id) {
		try {
			DocumentSnapshot snapshot = getFireStore().collection(getCollectionName()).document(id).get().get();
			if (snapshot.exists()) {
				ApiFuture<WriteResult> result = getFireStore().collection(getCollectionName()).document(id).delete();
				LOG.info("User [{}] deleted, update time: {}", id, result.get().getUpdateTime());
				updateCount(getCollectionName(), Statistic.COUNTMODE.DECREASE);
				return true;
			}
		} catch (InterruptedException | ExecutionException e) {
			LOG.error(ErrorUtil.getErrorTitle("UserService.delete"), e);
		}

		return false;
	}

	@Override
	public Map<String, Object> readFields(String id, String... fields) {
		ApiFuture<QuerySnapshot> future = getFireStore().collection(getCollectionName()).whereEqualTo(Field.ID, id)
				.select(fields).get();

		QueryDocumentSnapshot snapshot = getSnapshot(future, getCollectionName(), "UserService.readFields");
		if (snapshot != null) {
			return snapshot.getData();
		}

		return null;
	}

	@Override
	public int getCount(boolean refresh) {
		if (refresh || cachedCount == 0)
			cachedCount = getCount(getCollectionName());
		return cachedCount;
	}

	@Override
	public List<Searchable> getSearchableFields() {
		return new ArrayList<Searchable>(Arrays.asList(new Searchable(Field.ID, Searchable.Type.STRING),
				new Searchable(USERNAME, Searchable.Type.STRING), new Searchable(ACCOUNT, Searchable.Type.STRING)));
	}

	@Override
	public void populateFields(User user) {
		user.setCreatedDate(TimeUtil.currentTime());
		user.setDefaultBanks(-1);
		user.setDefaultWallets(-1);
		user.setUpdatedDate(TimeUtil.currentTime());
		user.setPassword(encoder.encode(user.getPassword()));
		user.setId(UuidUtil.generate());
	}

	@Override
	public void populateFields(Map<String, Object> map) {
		map.put(Field.CREATED_DATE, TimeUtil.currentTime());
		map.put(Field.UPDATED_DATE, TimeUtil.currentTime());
		map.put(DEFAULT_BANK, -1);
		map.put(DEFAULT_WALLET, -1);
		map.put(PASSWORD, encoder.encode((String) map.get(PASSWORD)));
		map.put(Field.ID, UuidUtil.generate());
	}

	@Override
	public void sendNotification(User data) {
		webSocketService.sendWebSocketMessage(SOCKET_MESSAGE + data.getId());
	}

	public boolean resetPassword(Map<String, Object> payload) {
		User user = findUser((String) payload.get(ACCOUNT));
		if (user != null) {
			payload.remove(ACCOUNT);
			return update(user.getId(), payload);
		}

		return false;
	}

	public User findUser(String uniqueField) {
		User user = null;

		ApiFuture<QuerySnapshot> future = getFireStore().collection(getCollectionName())
				.whereEqualTo(ACCOUNT, uniqueField).get();
		QueryDocumentSnapshot snapshot = getSnapshot(future, getCollectionName(), "UserService.findUser");
		if (snapshot != null) {
			user = snapshot.toObject(User.class);
		}

		return user;
	}

	public JwtResponse register(User user) {
		User newUser = create(user);
		if (newUser != null) {
			final String token = jwtTokenUtil.generateToken(user.getAccount(), user.getType().name(), user.getId());
			return JwtResponse.builder().jwttoken(token).role(user.getType()).userId(user.getId()).build();
		}

		return null;
	}

	public String encryptPassword(String password) {
		return encoder.encode(password);
	}

}
