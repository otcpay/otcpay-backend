package com.otcpay.fx.service.internal;

import java.util.Map;
import java.util.Properties;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.otcpay.fx.model.common.Field;
import com.otcpay.fx.model.internal.EmailRequest;

@Service
public class EmailService {
	private static final Logger LOG = LoggerFactory.getLogger(EmailService.class);

	private static final String EMAIL_TEMPLATE_START = "<!DOCTYPE html><html><head><style>#customers {  font-family: Arial, Helvetica, sans-serif;  border-collapse: collapse;  width: 100%;}#customers td, #customers th {  border: 1px solid #ddd;  padding: 8px;}#customers tr:nth-child(even){background-color: #f2f2f2;}#customers tr:hover {background-color: #ddd;}#customers th {  padding-top: 12px;  padding-bottom: 12px;  text-align: left;  background-color: #04AA6D;  color: white;}</style></head><body><table id=\"customers\"><tr><th colspan=\"2\">";
	private static final String EMAIL_TEMPLATE_END = "</body></html>";

	private static final String HOST = "smtpout.secureserver.net";
	private static final String USERNAME = "no-reply@otcpay.asia";
	private static final String PASSWORD = "hi!OTC1234";
	public static final String TO_ORDERS_EMAIL = "orders@otcpay.asia";

	private final ObjectMapper mapper = new ObjectMapper();

	private Session session;

	public EmailService() {
		Properties properties = System.getProperties();
		properties.put("mail.smtp.host", HOST);
		properties.put("mail.smtp.port", "465");
		properties.put("mail.smtp.ssl.enable", "true");
		properties.put("mail.smtp.auth", "true");

		session = Session.getInstance(properties, new javax.mail.Authenticator() {
			protected PasswordAuthentication getPasswordAuthentication() {
				return new PasswordAuthentication(USERNAME, PASSWORD);
			}
		});
	}

	public void sendEmail(EmailRequest request) {
		try {
			MimeMessage message = new MimeMessage(session);
			message.setFrom(new InternetAddress(USERNAME));
			message.addRecipient(Message.RecipientType.TO, new InternetAddress(request.getTo()));
			message.setSubject(request.getSubject());
			message.setContent(convertObjectToTable(request, "Email"), "text/html");
			Transport.send(message);
		} catch (MessagingException mex) {
			LOG.error("Failed to send email");
		}
	}

	@Async
	public void sendEmail(EmailRequest request, Object object, String title) {
		try {
			MimeMessage message = new MimeMessage(session);
			message.setFrom(new InternetAddress(USERNAME));
			message.addRecipient(Message.RecipientType.TO, new InternetAddress(request.getTo()));
			message.setSubject(request.getSubject());
			message.setContent(convertObjectToTable(object, title), "text/html");
			Transport.send(message);
		} catch (MessagingException mex) {
			LOG.error("Failed to send email");
		}
	}

	@SuppressWarnings("unchecked")
	public String convertObjectToTable(Object object, String title) {
		Map<String, Object> map = mapper.convertValue(object, Map.class);
		StringBuilder builder = new StringBuilder();
		builder.append(EMAIL_TEMPLATE_START + title + "</th></tr>");
		map.keySet().stream().filter((k) -> !k.equals(Field.CREATED_DATE) || !k.equals(Field.UPDATED_DATE))
				.forEach((k) -> {
					builder.append("<tr><td>");
					builder.append(k);
					builder.append("</td><td>");
					builder.append(map.get(k));
					builder.append("</td></tr>");
				});
		builder.append(EMAIL_TEMPLATE_END);
		return builder.toString();
	}

}
