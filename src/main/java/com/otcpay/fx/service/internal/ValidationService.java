package com.otcpay.fx.service.internal;

import java.util.Arrays;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.ValidationException;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

@Service
public class ValidationService {
	private static final Logger LOG = LoggerFactory.getLogger(ValidationService.class);

	private Validator validator;

	public ValidationService() {
		ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
		validator = factory.getValidator();
	}

	public <T> List<String> validateEntity(T object) {
		try {
			Set<ConstraintViolation<T>> violations = validator.validate(object);
			return violations.stream().map((v) -> v.getMessage()).collect(Collectors.toList());
		} catch (ValidationException e) {
			String msg = "Failed to validate entity";
			LOG.error(msg);
			return Arrays.asList(msg);
		}
	}
}
