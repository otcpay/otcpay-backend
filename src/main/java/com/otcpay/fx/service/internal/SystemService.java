package com.otcpay.fx.service.internal;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.ExecutionException;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.CacheManager;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.api.core.ApiFuture;
import com.google.api.core.ApiFutures;
import com.google.cloud.firestore.DocumentSnapshot;
import com.google.cloud.firestore.QuerySnapshot;
import com.google.cloud.firestore.WriteResult;
import com.otcpay.fx.model.common.Field;
import com.otcpay.fx.model.internal.Banner;
import com.otcpay.fx.model.internal.DetailStatistic;
import com.otcpay.fx.model.internal.FirebaseStatus;
import com.otcpay.fx.model.internal.LastUpdatedTime;
import com.otcpay.fx.model.internal.Statistic;
import com.otcpay.fx.model.rate.DailyRate;
import com.otcpay.fx.model.rate.DefaultFiatRate;
import com.otcpay.fx.model.rate.DefaultOtcRate;
import com.otcpay.fx.service.AdvertisementService;
import com.otcpay.fx.service.BuySellRecordService;
import com.otcpay.fx.service.InvoicePaymentService;
import com.otcpay.fx.service.SettlementService;
import com.otcpay.fx.service.UserService;
import com.otcpay.fx.service.google.firebase.FireStoreService;
import com.otcpay.fx.util.ErrorUtil;
import com.otcpay.fx.util.TimeUtil;

@Service
public class SystemService extends FireStoreService {
	private static final Logger LOG = LoggerFactory.getLogger(SystemService.class);

	private static final String FIREBASE_STATUS = "https://status.firebase.google.com/incidents.json";

	private static final String CONFIG_COL_NAME = "GlobalConfig";
	private static final String COUNTRY_MAPPING_DOC_NAME = "country";
	private static final String CURRENCY_DOC_NAME = "currency";
	private static final String BANNER_DOC_NAME = "banner";

	private static final String LAST_UPDATED_TIME = "lastUpdatedTime";
	private static final String DEFAULT_CURRENCY_KEY = "defaultCurrency";
	private static final String DEFAULT_CRYPTO_KEY = "defaultCrypto";
	private static final String DEFAULT_OTC_KEY = "defaultOtc";

	private static final String MAPPING_CACHE_KEY = "systemMapping";
	private static final String CURRENCY_CACHE_KEY = "systemCurrency";
	private static final String CRYPTO_CACHE_KEY = "systemCrypto";

	private static final long WITH_IN_ONE_WEEK = 1000 * 60 * 60 * 24 * 7;
	private static final long WITH_IN_ONE_DAY = 1000 * 60 * 60 * 24;

	private final ObjectMapper mapper = new ObjectMapper();

	@Autowired
	private CacheManager cacheManager;

	private List<String> getSystemObject() {
		List<String> resourcesList = new ArrayList<String>();
		resourcesList.add(UserService.COL_NAME);
		resourcesList.add(AdvertisementService.COL_NAME);
		resourcesList.add(InvoicePaymentService.COL_NAME);
		resourcesList.add(SettlementService.COL_NAME);
		resourcesList.add(BuySellRecordService.COL_NAME);
		return resourcesList;
	}

	/**
	 * Count number of items that its creation time is after a specific time
	 * 
	 * @param objects   List of items (Implicit type expect to be String)
	 * @param startTime String representation of UNIX time
	 * @return number of items
	 */
	private long countByDateRange(List<Object> objects, String startTime) {
		return objects.stream().filter((o) -> {
			String creationTime = (String) o;
			return creationTime.compareTo(startTime) > 0;
		}).count();
	}

	/**
	 * Count number of items that its creation time is in between of two specific
	 * time
	 * 
	 * @param objects   List of items (Implicit type expect to be String)
	 * @param startTime String representation of UNIX time
	 * @param endTime   String representation of UNIX time
	 * @return number of items
	 */
	private long countByDateBetween(List<Object> objects, String startTime, String endTime) {
		return objects.stream().filter((o) -> {
			String creationTime = (String) o;
			return creationTime.compareTo(startTime) > 0 && creationTime.compareTo(endTime) < 0;
		}).count();
	}

	/**
	 * Get detailed statistic of each collection in FireStore. Including total
	 * count, latest 1 month, 1 week, 24 hours count, first day and last time of 1
	 * month, 3 months count.
	 * 
	 * @return key-value mapping of statistic, key = collection name, value =
	 *         {@link DetailStatistic}
	 */
	public Map<String, DetailStatistic> getSystemStatistic() {
		List<String> resourcesList = getSystemObject();
		List<ApiFuture<DocumentSnapshot>> futures = resourcesList.stream()
				.map(key -> getFireStore().collection(key).document(Statistic.DOC_NAME).get())
				.collect(Collectors.toList());

		Map<String, DetailStatistic> stats = new HashMap<String, DetailStatistic>();

		String todayInitialTime = TimeUtil.getNDayEpochTime(0);
		String yesterdayInitialTime = TimeUtil.getNDayEpochTime(-1);
		String twoDaysBeforeInitialTime = TimeUtil.getNDayEpochTime(-2);
		String threeDaysBeforeInitialTime = TimeUtil.getNDayEpochTime(-3);
		String fourDaysBeforeInitialTime = TimeUtil.getNDayEpochTime(-4);
		String fiveDaysBeforeInitialTime = TimeUtil.getNDayEpochTime(-5);
		String sixDaysBeforeInitialTime = TimeUtil.getNDayEpochTime(-6);
		String firstDayOfMonthTime = TimeUtil.getFirstDayOfMonth();
		String firstDayOfWeekTime = TimeUtil.getFirstDayOfWeek();
		String threeMonthsAgoTime = TimeUtil.getNMonthEpochTime(-3);
		String twoMonthsAgoTime = TimeUtil.getNMonthEpochTime(-2);
		String oneMonthAgoTime = TimeUtil.getNMonthEpochTime(-1);
		String fourWeeksAgoTime = TimeUtil.currentTimeWithOffset(TimeUtil.Type.MINUS, WITH_IN_ONE_WEEK * 4);
		String twoWeeksAgoTime = TimeUtil.currentTimeWithOffset(TimeUtil.Type.MINUS, WITH_IN_ONE_WEEK * 2);
		String oneWeekAgoTime = TimeUtil.currentTimeWithOffset(TimeUtil.Type.MINUS, WITH_IN_ONE_WEEK);
		String oneDayAgoTime = TimeUtil.currentTimeWithOffset(TimeUtil.Type.MINUS, WITH_IN_ONE_DAY);

		try {
			List<DocumentSnapshot> snapshots = Optional.ofNullable(ApiFutures.allAsList(futures).get())
					.orElse(new ArrayList<DocumentSnapshot>());
			Map<String, Statistic> statistics = IntStream.range(0, snapshots.size()).boxed().collect(
					Collectors.toMap(resourcesList::get, e -> snapshots.get(e.intValue()).toObject(Statistic.class)));
			Map<String, List<Object>> dateRangeCount = getDateRangeStatistic();
			dateRangeCount.keySet().forEach((k) -> {
				long hallfYear = dateRangeCount.get(k).size();
				long threeMonthCount = countByDateRange(dateRangeCount.get(k), threeMonthsAgoTime);
				long twoMonthCount = countByDateRange(dateRangeCount.get(k), twoMonthsAgoTime);
				long oneMonthCount = countByDateRange(dateRangeCount.get(k), oneMonthAgoTime);
				long fourWeekCount = countByDateRange(dateRangeCount.get(k), fourWeeksAgoTime);
				long twoWeekCount = countByDateRange(dateRangeCount.get(k), twoWeeksAgoTime);
				long weekCount = countByDateRange(dateRangeCount.get(k), oneWeekAgoTime);
				long dayCount = countByDateRange(dateRangeCount.get(k), oneDayAgoTime);
				long thisMonthCount = countByDateRange(dateRangeCount.get(k), firstDayOfMonthTime);
				long thisWeekCount = countByDateRange(dateRangeCount.get(k), firstDayOfWeekTime);
				long thisDayCount = countByDateRange(dateRangeCount.get(k), todayInitialTime);
				long yesterdayCount = countByDateBetween(dateRangeCount.get(k), yesterdayInitialTime, todayInitialTime);
				long twoDaysBeforeCount = countByDateBetween(dateRangeCount.get(k), twoDaysBeforeInitialTime,
						yesterdayInitialTime);
				long threeDaysBeforeCount = countByDateBetween(dateRangeCount.get(k), threeDaysBeforeInitialTime,
						twoDaysBeforeInitialTime);
				long fourDaysBeforeCount = countByDateBetween(dateRangeCount.get(k), fourDaysBeforeInitialTime,
						threeDaysBeforeInitialTime);
				long fiveDaysBeforeCount = countByDateBetween(dateRangeCount.get(k), fiveDaysBeforeInitialTime,
						fourDaysBeforeInitialTime);
				long sixDaysBeforeCount = countByDateBetween(dateRangeCount.get(k), sixDaysBeforeInitialTime,
						fiveDaysBeforeInitialTime);
				DetailStatistic ds = DetailStatistic.builder().lastHalfYear(hallfYear).lastThreeMonth(threeMonthCount)
						.lastTwoMonth(twoMonthCount).lastMonth(oneMonthCount).lastFourWeek(fourWeekCount)
						.lastTwoWeek(twoWeekCount).lastWeek(weekCount).today(dayCount).absMonth(thisMonthCount)
						.absWeek(thisWeekCount).yesterday(yesterdayCount).twoDaysBefore(twoDaysBeforeCount)
						.threeDaysBefore(threeDaysBeforeCount).fourDaysBefore(fourDaysBeforeCount)
						.fiveDaysBefore(fiveDaysBeforeCount).sixDaysBefore(sixDaysBeforeCount).absToday(thisDayCount)
						.total(statistics.get(k).getCount()).build();
				stats.put(k, ds);
			});
			return stats;
		} catch (InterruptedException | ExecutionException | NullPointerException e) {
			LOG.error(ErrorUtil.getErrorTitle("SystemService.getSystemStatistic"), e);
		}

		return null;
	}

	/**
	 * Get Detailed Statistic of different type of document in specific collection
	 * 
	 * @return key-value mapping of statistic, key = collection name, value = nested
	 *         key-value mapping; nested key = name of type, nested value = count of
	 *         type
	 */
	public Map<String, Map<String, Object>> getSystemStatisticDetail() {
		List<String> resourcesList = Arrays.asList(SettlementService.COL_NAME, AdvertisementService.COL_NAME,
				BuySellRecordService.COL_NAME);
		List<ApiFuture<DocumentSnapshot>> futures = resourcesList.stream()
				.map(key -> getFireStore().collection(key).document(Statistic.SUB_DOC_NAME).get())
				.collect(Collectors.toList());

		try {
			List<DocumentSnapshot> snapshots = Optional.ofNullable(ApiFutures.allAsList(futures).get())
					.orElse(new ArrayList<DocumentSnapshot>());
			return IntStream.range(0, snapshots.size()).boxed()
					.collect(Collectors.toMap(resourcesList::get, e -> snapshots.get(e.intValue()).getData()));

		} catch (InterruptedException | ExecutionException | NullPointerException e) {
			LOG.error(ErrorUtil.getErrorTitle("SystemService.getSystemStatisticDetail"), e);
		}

		return null;
	}

	/**
	 * Get all data from last 6 months of all resources Data will be retrieved from
	 * FireStore simultaneously and separated with their collection name
	 * 
	 * @return return a key-value mapping, where key is the collection name, value
	 *         is their list of data within the time range
	 */
	private Map<String, List<Object>> getDateRangeStatistic() {
		List<String> resourcesList = getSystemObject();
		List<ApiFuture<QuerySnapshot>> futures = resourcesList.stream()
				.map(key -> getFireStore().collection(key)
						.whereGreaterThanOrEqualTo(Field.CREATED_DATE, TimeUtil.getNMonthEpochTime(-6)).get())
				.collect(Collectors.toList());

		try {
			List<QuerySnapshot> snapshots = Optional.ofNullable(ApiFutures.allAsList(futures).get())
					.orElse(new ArrayList<QuerySnapshot>());
			return IntStream.range(0, snapshots.size()).boxed()
					.collect(Collectors.toMap(resourcesList::get, e -> snapshots.get(e.intValue()).getDocuments()
							.stream().map((d) -> d.get(Field.CREATED_DATE)).collect(Collectors.toList())));
		} catch (InterruptedException | ExecutionException | NullPointerException e) {
			LOG.error(ErrorUtil.getErrorTitle("SystemService.getDateRangeStatistic"), e);
		}

		return null;
	}

	public FirebaseStatus[] getCoreServiceStatus() {
		RestTemplate template = new RestTemplate();
		return template.getForObject(FIREBASE_STATUS, FirebaseStatus[].class);
	}

	@Cacheable(value = MAPPING_CACHE_KEY)
	public Map<String, Object> getSystemMapping() {
		try {
			List<ApiFuture<DocumentSnapshot>> futures = new ArrayList<ApiFuture<DocumentSnapshot>>();
			futures.add(getFireStore().collection(CONFIG_COL_NAME).document(COUNTRY_MAPPING_DOC_NAME).get());
			futures.add(getFireStore().collection(CONFIG_COL_NAME).document(BANNER_DOC_NAME).get());
			List<DocumentSnapshot> snapshots = ApiFutures.allAsList(futures).get();

			Map<String, Object> mapping = new HashMap<String, Object>();
			snapshots.forEach((s) -> {
				mapping.putAll(s.getData());
			});
			return mapping;
		} catch (InterruptedException | ExecutionException | NullPointerException e) {
			LOG.error(ErrorUtil.getErrorTitle("SystemService.getSystemMapping"), e);
		}

		return null;
	}

	@Cacheable(value = CURRENCY_CACHE_KEY)
	public List<DailyRate> getSystemCurrency() {
		try {
			Object defaultCurrency = getFireStore().collection(CONFIG_COL_NAME).document(CURRENCY_DOC_NAME).get().get()
					.getData().get(DEFAULT_CURRENCY_KEY);
			List<DailyRate> rates = mapper.readValue(mapper.writeValueAsString(defaultCurrency),
					new TypeReference<List<DailyRate>>() {
					});
			return rates;
		} catch (InterruptedException | ExecutionException | NullPointerException | JsonProcessingException e) {
			LOG.error(ErrorUtil.getErrorTitle("SystemService.getSystemCurrency"), e);
		}

		return null;
	}

	@Cacheable(value = CRYPTO_CACHE_KEY)
	public String[] getSystemCrypto() {
		try {
			Object defaultCurrency = getFireStore().collection(CONFIG_COL_NAME).document(CURRENCY_DOC_NAME).get().get()
					.getData().get(DEFAULT_CRYPTO_KEY);
			String[] cryptos = mapper.readValue(mapper.writeValueAsString(defaultCurrency), String[].class);
			return cryptos;
		} catch (InterruptedException | ExecutionException | NullPointerException | JsonProcessingException e) {
			LOG.error(ErrorUtil.getErrorTitle("SystemService.getSystemCrypto"), e);
		}

		return null;
	}

	public List<DefaultOtcRate> getSystemDefautlOtcRate() {
		try {
			Object defaultCurrency = getFireStore().collection(CONFIG_COL_NAME).document(CURRENCY_DOC_NAME).get().get()
					.getData().get(DEFAULT_OTC_KEY);
			List<DefaultOtcRate> rates = mapper.readValue(mapper.writeValueAsString(defaultCurrency),
					new TypeReference<List<DefaultOtcRate>>() {
					});
			return rates;
		} catch (InterruptedException | ExecutionException | NullPointerException | JsonProcessingException e) {
			LOG.error(ErrorUtil.getErrorTitle("SystemService.getSystemDefautlOtcRate"), e);
		}

		return null;
	}

	public boolean updateCurrencyConfig(List<DefaultFiatRate> rate) {
		try {
			Map<String, Object> config = getFireStore().collection(CONFIG_COL_NAME).document(CURRENCY_DOC_NAME).get()
					.get().getData();
			config.put(DEFAULT_CURRENCY_KEY, rate);
			ApiFuture<WriteResult> result = getFireStore().collection(CONFIG_COL_NAME).document(CURRENCY_DOC_NAME)
					.update(config);
			LOG.info("Default currenct config updated, update time: {}", result.get().getUpdateTime());

			cacheManager.getCache(CURRENCY_CACHE_KEY).clear();

			return true;
		} catch (InterruptedException | ExecutionException | NullPointerException e) {
			LOG.error(ErrorUtil.getErrorTitle("SystemService.updateCurrencyConfig"), e);
		}

		return false;
	}

	public boolean updateBannerConfig(List<Banner> banner, String target) {
		try {
			Map<String, Object> config = new HashMap<String, Object>();
			config.put(target, banner);
			ApiFuture<WriteResult> result = getFireStore().collection(CONFIG_COL_NAME).document(BANNER_DOC_NAME)
					.update(config);
			LOG.info("Default banner config updated, update time: {}", result.get().getUpdateTime());

			cacheManager.getCache(MAPPING_CACHE_KEY).clear();

			return true;
		} catch (InterruptedException | ExecutionException | NullPointerException e) {
			LOG.error(ErrorUtil.getErrorTitle("SystemService.updateBannerConfig"), e);
		}

		return false;
	}

	public boolean updateOtcConfig(List<DefaultOtcRate> rate) {
		try {
			Map<String, Object> config = getFireStore().collection(CONFIG_COL_NAME).document(CURRENCY_DOC_NAME).get()
					.get().getData();
			config.put(DEFAULT_OTC_KEY, rate);
			ApiFuture<WriteResult> result = getFireStore().collection(CONFIG_COL_NAME).document(CURRENCY_DOC_NAME)
					.update(config);
			LOG.info("Default otc config updated, update time: {}", result.get().getUpdateTime());
			return true;
		} catch (InterruptedException | ExecutionException | NullPointerException e) {
			LOG.error(ErrorUtil.getErrorTitle("SystemService.updateCurrencyConfig"), e);
		}

		return false;
	}

	public boolean addCurrencyConfig(DefaultFiatRate rate) {
		try {
			Map<String, Object> config = getFireStore().collection(CONFIG_COL_NAME).document(CURRENCY_DOC_NAME).get()
					.get().getData();
			List<DefaultFiatRate> rates = mapper.convertValue(config.get(DEFAULT_CURRENCY_KEY),
					new TypeReference<List<DefaultFiatRate>>() {
					});
			rates.add(rate);
			config.put(DEFAULT_CURRENCY_KEY, rates);
			ApiFuture<WriteResult> result = getFireStore().collection(CONFIG_COL_NAME).document(CURRENCY_DOC_NAME)
					.update(config);
			LOG.info("Default currenct config added, update time: {}", result.get().getUpdateTime());

			cacheManager.getCache(CURRENCY_CACHE_KEY).clear();

			return true;
		} catch (InterruptedException | ExecutionException | NullPointerException e) {
			LOG.error(ErrorUtil.getErrorTitle("SystemService.addCurrencyConfig"), e);
		}

		return false;
	}

	public boolean addBannerConfig(Banner banner, String target) {
		try {
			Map<String, Object> config = getFireStore().collection(CONFIG_COL_NAME).document(BANNER_DOC_NAME).get()
					.get().getData();
			List<Banner> banners = mapper.convertValue(config.get(target), new TypeReference<List<Banner>>() {
			});
			banners.add(banner);
			config.put(target, banners);
			ApiFuture<WriteResult> result = getFireStore().collection(CONFIG_COL_NAME).document(BANNER_DOC_NAME)
					.update(config);
			LOG.info("Default banner config added, update time: {}", result.get().getUpdateTime());

			cacheManager.getCache(MAPPING_CACHE_KEY).clear();

			return true;
		} catch (InterruptedException | ExecutionException | NullPointerException e) {
			LOG.error(ErrorUtil.getErrorTitle("SystemService.addBannerConfig"), e);
		}

		return false;
	}

	public LastUpdatedTime getStatisticLastUpdatedTime() {
		try {
			return getFireStore().collection(CONFIG_COL_NAME).document(LAST_UPDATED_TIME).get().get()
					.toObject(LastUpdatedTime.class);
		} catch (InterruptedException | ExecutionException | NullPointerException e) {
			LOG.error(ErrorUtil.getErrorTitle("SystemService.getStatisticLastUpdatedTime"), e);
		}

		return null;
	}

	public boolean updateStatisticLastUpdatedTime(String newTime) {
		try {
			getFireStore().collection(CONFIG_COL_NAME).document(LAST_UPDATED_TIME).update(LAST_UPDATED_TIME, newTime)
					.get();

			return true;
		} catch (InterruptedException | ExecutionException | NullPointerException e) {
			LOG.error(ErrorUtil.getErrorTitle("SystemService.updateStatisticLastUpdatedTime"), e);
		}

		return false;
	}

	public ObjectMapper getObjectMapper() {
		return mapper;
	}
}
