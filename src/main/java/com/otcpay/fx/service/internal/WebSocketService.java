package com.otcpay.fx.service.internal;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.otcpay.fx.model.internal.WebSocketMessage;
import com.otcpay.fx.util.SystemUtil;

@Service
public class WebSocketService {
	private static final Logger LOG = LoggerFactory.getLogger(WebSocketService.class);

	private static final String NOTIFICATION_ENDPOINT = "/notification";

	private String endpoint;

	public WebSocketService() {
		this.endpoint = SystemUtil.getEnv(SystemUtil.CMS_ENDPOINT);
	}

	public void sendWebSocketMessage(String message) {
		try {
			RestTemplate restTemplate = new RestTemplate();
			HttpHeaders headers = new HttpHeaders();
			headers.setContentType(MediaType.APPLICATION_JSON);
			restTemplate.postForObject(endpoint + NOTIFICATION_ENDPOINT,
					WebSocketMessage.builder().message(message).build(), Void.class);
		} catch (RuntimeException e) {
			LOG.error("Failed to send websocket message", e);
		}
	}

}
