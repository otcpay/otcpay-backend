package com.otcpay.fx.service.internal;

import java.time.Duration;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.Cache;
import org.springframework.cache.CacheManager;
import org.springframework.stereotype.Service;

import io.github.bucket4j.Bandwidth;
import io.github.bucket4j.Bucket;
import io.github.bucket4j.Bucket4j;
import io.github.bucket4j.Refill;

@Service
public class BucketService {
	private static final int IP_BUCKET_LIMIT = 60;
	private static final int USERNAME_BUCKET_LIMIT = 30;
	private static final String IP_BUCKET = "ipBuckets";
	private static final String USERNAME_BUCKET = "usernameBuckets";

	@Autowired
	private CacheManager cacheManager;

	public Bucket ipBucket(String ip) {
		Cache cache = cacheManager.getCache(IP_BUCKET);
		if (cache != null) {
			return cache.get(ip, () -> Bucket4j.builder().addLimit(
					Bandwidth.classic(IP_BUCKET_LIMIT, Refill.intervally(IP_BUCKET_LIMIT, Duration.ofMinutes(1))))
					.build());
		}

		return null;
	}

	public Bucket usernameBucket(String username) {
		Cache cache = cacheManager.getCache(USERNAME_BUCKET);
		if (cache != null) {
			return cache.get(username, () -> Bucket4j.builder().addLimit(Bandwidth.classic(USERNAME_BUCKET_LIMIT,
					Refill.intervally(USERNAME_BUCKET_LIMIT, Duration.ofMinutes(1)))).build());
		}

		return null;
	}
}
