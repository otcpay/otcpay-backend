package com.otcpay.fx.service.internal;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ExecutionException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import com.google.api.core.ApiFuture;
import com.google.cloud.firestore.DocumentSnapshot;
import com.google.cloud.firestore.QuerySnapshot;
import com.google.cloud.firestore.WriteResult;
import com.otcpay.fx.model.internal.Authorization.AuthorizationGroup;
import com.otcpay.fx.model.internal.Authorization.AuthroizationMapping;
import com.otcpay.fx.service.common.DBService;
import com.otcpay.fx.service.google.firebase.FireStoreService;
import com.otcpay.fx.util.ErrorUtil;

@Service
public class AuthorizationService extends FireStoreService implements DBService {
	private static final Logger LOG = LoggerFactory.getLogger(AuthorizationService.class);

	public static final String COL_NAME = "Group";

	@Override
	public String getCollectionName() {
		return COL_NAME;
	}

	public Map<String, AuthroizationMapping> getAllAuthorizationMapping() {
		Map<String, AuthroizationMapping> mapping = new HashMap<String, AuthroizationMapping>();
		try {
			ApiFuture<QuerySnapshot> future = getFireStore().collection(getCollectionName()).get();
			future.get().getDocuments().forEach((doc) -> {
				mapping.put(doc.getId(), doc.toObject(AuthroizationMapping.class));
			});
		} catch (InterruptedException | ExecutionException e) {
			LOG.error(ErrorUtil.getErrorTitle("AuthorizationService.getAllAuthorizationMapping"), e);
		}

		return mapping;
	}

	public AuthroizationMapping getAuthorizationMapping(AuthorizationGroup group) {
		try {
			ApiFuture<DocumentSnapshot> future = getFireStore().collection(getCollectionName())
					.document(StringUtils.capitalize(group.name().toLowerCase())).get();
			return future.get().toObject(AuthroizationMapping.class);
		} catch (InterruptedException | ExecutionException e) {
			LOG.error(ErrorUtil.getErrorTitle("AuthorizationService.getAuthorizationMapping"), e);
		}

		return null;
	}

	public boolean updateGroupAccess(AuthorizationGroup group, String userId) {
		try {
			String groupName = group.name();
			ApiFuture<DocumentSnapshot> future = getFireStore().collection(getCollectionName())
					.document(StringUtils.capitalize(groupName.toLowerCase())).get();

			AuthroizationMapping mapping = future.get().toObject(AuthroizationMapping.class);
			if (mapping.getUser().contains(userId)) {
				mapping.getUser().remove(userId);
			} else {
				mapping.getUser().add(userId);
			}

			ApiFuture<WriteResult> result = getFireStore().collection(getCollectionName())
					.document(StringUtils.capitalize(group.name().toLowerCase())).set(mapping);
			LOG.info("Group [{}] updated, update time: {}", groupName, result.get().getUpdateTime());
			return true;
		} catch (InterruptedException | ExecutionException e) {
			LOG.error(ErrorUtil.getErrorTitle("AuthorizationService.grantUserToGroup"), e);
		}

		return false;
	}

}
