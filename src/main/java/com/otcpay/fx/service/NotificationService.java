package com.otcpay.fx.service;

import java.util.Comparator;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import com.google.api.core.ApiFuture;
import com.google.cloud.firestore.DocumentReference;
import com.google.cloud.firestore.DocumentSnapshot;
import com.google.cloud.firestore.QueryDocumentSnapshot;
import com.google.cloud.firestore.QuerySnapshot;
import com.google.cloud.firestore.WriteResult;
import com.otcpay.fx.model.message.Notification;
import com.otcpay.fx.model.message.NotificationRequest;
import com.otcpay.fx.model.message.StatusNotification;
import com.otcpay.fx.service.common.DBService;
import com.otcpay.fx.service.google.firebase.FireStoreService;
import com.otcpay.fx.util.ErrorUtil;
import com.otcpay.fx.util.TimeUtil;

@Service
public class NotificationService extends FireStoreService implements DBService {
	private static final Logger LOG = LoggerFactory.getLogger(NotificationService.class);

	public static final String SUB_COL_NAME = "Notifications";

	private static final String DEFAULT_STATUS_NOTI_TITLE = "Status Update";

	private static final String READ = "read";

	@Override
	public String getCollectionName() {
		return UserService.COL_NAME;
	}

	public String getSubCollectionName() {
		return SUB_COL_NAME;
	}

	/**
	 * Push notification to Firebase Notification (For mobile) and persist
	 * notification as data per user for in-app display
	 * 
	 * @param notificationRequest {@link NotificationRequest}
	 */
	@Async
	public void pushSingleNotification(NotificationRequest notificationRequest) {
		Notification notification = Notification.builder().body(notificationRequest.getBody())
				.title(notificationRequest.getTitle()).createdAt(TimeUtil.currentTime()).build();

		sendNotification(notificationRequest.getId(), notification);
	}

	@Async
	public void pushMultipleNotification(NotificationRequest notificationRequest) {
		Notification notification = Notification.builder().body(notificationRequest.getBody())
				.title(notificationRequest.getTitle()).createdAt(TimeUtil.currentTime()).build();
		sendNotification(notificationRequest.getId(), notification);
	}

	/**
	 * Mark all notification of a particular user as read
	 * 
	 * @param userId user's Id
	 */
	@Async
	public void readAllNotification(String userId) {
		ApiFuture<QuerySnapshot> future = getFireStore().collection(getCollectionName()).document(userId)
				.collection(getSubCollectionName()).whereEqualTo(READ, false).get();

		try {
			List<String> ids = future.get().getDocuments().stream().map((doc) -> doc.getId())
					.collect(Collectors.toList());
			for (String id : ids) {
				ApiFuture<WriteResult> result = getFireStore().collection(getCollectionName()).document(userId)
						.collection(getSubCollectionName()).document(id).update(READ, true);
				LOG.info("Notification [{}] updated, update time: {}", id, result.get().getUpdateTime());
			}
		} catch (InterruptedException | ExecutionException e) {
			LOG.error(ErrorUtil.getErrorTitle("NotificationService.realAllNotification"), e);
		}
	}

	/**
	 * Pull all persisted notification(s) of a particular user from FireStore
	 * 
	 * @param id user's Id
	 * @return List of {@link Notification}
	 */
	public List<Notification> pullNotification(String id) {
		ApiFuture<QuerySnapshot> future = getFireStore().collection(getCollectionName()).document(id)
				.collection(getSubCollectionName()).get();

		try {
			List<Notification> notifications = future.get().getDocuments().stream()
					.map((doc) -> doc.toObject(Notification.class)).collect(Collectors.toList());
			notifications.sort(Comparator.comparing(Notification::getCreatedAt, Comparator.reverseOrder()));
			return notifications;
		} catch (InterruptedException | ExecutionException e) {
			LOG.error(ErrorUtil.getErrorTitle("NotificationService.pullNotification"), e);
		}
		return null;
	}

	/**
	 * Delete all persisted notification(s) of a particular user from FireStore
	 * 
	 * @param id user's Id
	 */
	public void clearNotification(String id) {
		try {
			List<QueryDocumentSnapshot> docs = getFireStore().collection(getCollectionName()).document(id)
					.collection(getSubCollectionName()).get().get().getDocuments();
			for (QueryDocumentSnapshot doc : docs) {
				ApiFuture<WriteResult> result = doc.getReference().delete();
				LOG.info("Notification [{}] deleted,  update time: {}", doc.getId(), result.get().getUpdateTime());
			}
		} catch (InterruptedException | ExecutionException e) {
			LOG.error(ErrorUtil.getErrorTitle("NotificationService.pullNotification"), e);
		}
	}

	/**
	 * Send notification to user when status of Advertisement, OTC records and
	 * Invoice Payment is updated. Normally, it should be triggered by CMS operation
	 * 
	 * @param notification
	 * @return
	 */
	public NotificationRequest pushStatusNotificationByUserId(StatusNotification notification) {
		try {
			DocumentSnapshot snapshot = getFireStore().collection(getCollectionName())
					.document(notification.getUserId()).get().get();
			String token = snapshot.get(UserService.NOTIFICATION_TOKEN, String.class);

			NotificationRequest request = NotificationRequest.builder().body(notification.getBody())
					.id(notification.getUserId()).target(token).title(DEFAULT_STATUS_NOTI_TITLE).build();
			pushSingleNotification(request);
			return request;
		} catch (InterruptedException | ExecutionException e) {
			LOG.error(ErrorUtil.getErrorTitle("NotificationService.pushNotificationByUserId"), e);
		}

		return null;
	}

	private void sendNotification(String id, Notification notification) {
		ApiFuture<DocumentReference> sendFuture = getFireStore().collection(getCollectionName()).document(id)
				.collection(getSubCollectionName()).add(notification);

		try {
			LOG.info("Notification [{}] created", sendFuture.get().getId());
		} catch (InterruptedException | ExecutionException e) {
			LOG.error(ErrorUtil.getErrorTitle("NotificationService.pushNotification"), e);
		}
	}
}
