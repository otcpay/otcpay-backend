package com.otcpay.fx.service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutionException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.api.core.ApiFuture;
import com.google.cloud.firestore.DocumentReference;
import com.google.cloud.firestore.DocumentSnapshot;
import com.google.cloud.firestore.QueryDocumentSnapshot;
import com.google.cloud.firestore.QuerySnapshot;
import com.google.cloud.firestore.WriteResult;
import com.otcpay.fx.model.Advertisement;
import com.otcpay.fx.model.BuySellRecord;
import com.otcpay.fx.model.MatchingResult;
import com.otcpay.fx.model.common.Field;
import com.otcpay.fx.model.common.search.PaginatedRequest;
import com.otcpay.fx.model.common.search.Searchable;
import com.otcpay.fx.model.internal.EmailRequest;
import com.otcpay.fx.model.internal.Statistic;
import com.otcpay.fx.service.common.async.AsyncNotifiableService;
import com.otcpay.fx.service.internal.EmailService;
import com.otcpay.fx.service.internal.WebSocketService;
import com.otcpay.fx.util.ErrorUtil;
import com.otcpay.fx.util.TimeUtil;
import com.otcpay.fx.util.UuidUtil;

@Service
public class BuySellRecordService extends AsyncNotifiableService<BuySellRecord> {
	private static final Logger LOG = LoggerFactory.getLogger(BuySellRecordService.class);

	public static final String COL_NAME = "BuySellRecords";

	protected static final String STATUS = "status";
	private static final String DEFAULT_TITLE = "[OTCPAY System] New %s Created";
	private static final String SOCKET_MESSAGE = "New BuySellRecords Created - ";

	private int cachedCount = 0;

	private final ObjectMapper mapper = new ObjectMapper();

	@Autowired
	private AdvertisementService advertisementService;

	@Autowired
	private EmailService emailService;

	@Autowired
	private WebSocketService webSocketService;

	@Override
	public String getCollectionName() {
		return COL_NAME;
	}

	@Override
	public BuySellRecord create(BuySellRecord data) {
		try {
			populateFields(data);
			ApiFuture<WriteResult> result = getFireStore().collection(getCollectionName()).document(data.getId())
					.create(data);

			LOG.info("BuySellRecord [{}] created, update time: {}", data.getId(), result.get().getUpdateTime());
			increaseCountAndSendNotification(data);

			return data;
		} catch (InterruptedException | ExecutionException e) {
			LOG.error(ErrorUtil.getErrorTitle("BuySellRecordService.create"), e);
		}

		return null;
	}

	@Override
	public boolean update(String id, Map<String, Object> data) {
		data.put(Field.UPDATED_DATE, TimeUtil.currentTime());
		ApiFuture<WriteResult> result = getFireStore().collection(getCollectionName()).document(id).update(data);
		try {
			LOG.info("BuySellRecord [{}] updated, update time: {}", id, result.get().getUpdateTime());
			return true;
		} catch (InterruptedException | ExecutionException e) {
			LOG.error(ErrorUtil.getErrorTitle("BuySellRecordService.update"), e);
		}

		return false;
	}

	@Override
	public BuySellRecord readById(String id) {
		DocumentReference ref = getFireStore().collection(getCollectionName()).document(id);
		ApiFuture<DocumentSnapshot> future = ref.get();
		try {
			DocumentSnapshot document = future.get();
			if (document.exists()) {
				return document.toObject(BuySellRecord.class);
			}
		} catch (InterruptedException | ExecutionException e) {
			LOG.error(ErrorUtil.getErrorTitle("BuySellRecordService.readById"), e);
		}

		return null;
	}

	@Override
	public List<Map<String, Object>> readAll(int page, int size, String fields, String keywords, String orderBy,
			String direction) {
		return getCustomData(getCollectionName(),
				PaginatedRequest.builder().page(page).size(size).fields(fields).keywords(keywords).orderBy(orderBy)
						.direction(direction).searchableFields(getSearchableFields()).build());
	}

	@Override
	public boolean delete(String id) {
		try {
			DocumentSnapshot snapshot = getFireStore().collection(getCollectionName()).document(id).get().get();
			if (snapshot.exists()) {
				ApiFuture<WriteResult> result = getFireStore().collection(getCollectionName()).document(id).delete();

				LOG.info("BuySellRecord [{}] deleted, update time: {}", id, result.get().getUpdateTime());
				updateCount(Statistic.COUNTMODE.DECREASE);

				return true;
			}
		} catch (InterruptedException | ExecutionException e) {
			LOG.error(ErrorUtil.getErrorTitle("BuySellRecordService.delete"), e);
		}

		return false;
	}

	@Override
	public Map<String, Object> readFields(String id, String... fields) {
		ApiFuture<QuerySnapshot> future = getFireStore().collection(getCollectionName()).whereEqualTo(Field.ID, id)
				.select(fields).get();

		QueryDocumentSnapshot snapshot = getSnapshot(future, getCollectionName(), "BuySellRecordService.readFields");
		if (snapshot != null) {
			return snapshot.getData();
		}

		return null;
	}

	@Override
	public int getCount(boolean refresh) {
		if (refresh || cachedCount == 0)
			cachedCount = getCount(getCollectionName());
		return cachedCount;
	}

	@Override
	public List<Searchable> getSearchableFields() {
		return new ArrayList<Searchable>(Arrays.asList(new Searchable(Field.ID, Searchable.Type.STRING),
				new Searchable(Field.USER_ID, Searchable.Type.STRING),
				new Searchable(Field.STATUS, Searchable.Type.STRING),
				new Searchable(Field.TYPE, Searchable.Type.STRING),
				new Searchable(Field.CRYPTOCURRENCY, Searchable.Type.STRING)));
	}

	@Override
	public void populateFields(BuySellRecord data) {
		data.setCreatedDate(TimeUtil.currentTime());
		data.setUpdatedDate(TimeUtil.currentTime());
		data.setStatus(BuySellRecord.Status.Processing);
		data.setId(UuidUtil.generate());
	}

	@Override
	public void populateFields(Map<String, Object> map) {
		map.put(Field.CREATED_DATE, TimeUtil.currentTime());
		map.put(Field.UPDATED_DATE, TimeUtil.currentTime());
		map.put(Field.STATUS, BuySellRecord.Status.Processing);
		map.put(Field.ID, UuidUtil.generate());
	}

	@Override
	public void sendNotification(BuySellRecord data) {
		try {
			String title = String.format(DEFAULT_TITLE, "OTC Request Matched");

			if (data.getAdId() != null && !data.getAdId().isEmpty()) {
				MatchingResult result = mapper.convertValue(data, MatchingResult.class);
				Advertisement ad = advertisementService.readById(data.getAdId());
				result.setAdContact(ad.getContact());
				result.setAdUserId(ad.getUserId());
				result.setAdUserName(ad.getName());

				emailService.sendEmail(EmailRequest.builder().to(EmailService.TO_ORDERS_EMAIL)
						.subject(title + " - " + TimeUtil.currentTime()).build(), result, getCollectionName());
			} else {
				title = data.getType().equals(Advertisement.Type.BUY)
						? String.format(DEFAULT_TITLE, "Direct Buy Request")
						: String.format(DEFAULT_TITLE, "Direct Sell Request");
				title += " (*Daily Rate*)";

				emailService.sendEmail(EmailRequest.builder().to(EmailService.TO_ORDERS_EMAIL)
						.subject(title + " - " + TimeUtil.currentTime()).build(), data, getCollectionName());
			}

			webSocketService.sendWebSocketMessage(SOCKET_MESSAGE + data.getId());
		} catch (RuntimeException e) {
			LOG.error("Failed to send {} notification", getCollectionName(), e);
		}
	}
}
