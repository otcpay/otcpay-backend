package com.otcpay.fx.service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutionException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.google.api.core.ApiFuture;
import com.google.cloud.firestore.DocumentReference;
import com.google.cloud.firestore.DocumentSnapshot;
import com.google.cloud.firestore.Query.Direction;
import com.google.cloud.firestore.QueryDocumentSnapshot;
import com.google.cloud.firestore.QuerySnapshot;
import com.google.cloud.firestore.WriteResult;
import com.otcpay.fx.model.InvoicePayment;
import com.otcpay.fx.model.common.Field;
import com.otcpay.fx.model.common.search.PaginatedRequest;
import com.otcpay.fx.model.common.search.Searchable;
import com.otcpay.fx.model.internal.EmailRequest;
import com.otcpay.fx.model.internal.Statistic;
import com.otcpay.fx.service.common.async.AsyncNotifiableService;
import com.otcpay.fx.service.internal.EmailService;
import com.otcpay.fx.service.internal.WebSocketService;
import com.otcpay.fx.util.ErrorUtil;
import com.otcpay.fx.util.TimeUtil;
import com.otcpay.fx.util.UuidUtil;

@Service
public class InvoicePaymentService extends AsyncNotifiableService<InvoicePayment> {
	private static final Logger LOG = LoggerFactory.getLogger(InvoicePaymentService.class);

	public static final String COL_NAME = "InvoicePayments";

	protected static final String STATUS = "status";
	private static final String DEFAULT_TITLE = "[OTCPAY System] New Invoice Payment request Created";
	private static final String SOCKET_MESSAGE = "New Invoice Payment Created - ";

	private int cachedCount = 0;

	@Autowired
	private EmailService emailService;

	@Autowired
	private WebSocketService webSocketService;

	@Override
	public String getCollectionName() {
		return COL_NAME;
	}

	@Override
	public InvoicePayment create(InvoicePayment data) {
		try {
			populateFields(data);
			ApiFuture<WriteResult> result = getFireStore().collection(getCollectionName()).document(data.getId())
					.create(data);

			LOG.info("InvoicePayment [{}] created, update time: {}", data.getId(), result.get().getUpdateTime());
			increaseCountAndSendNotification(data);

			return data;
		} catch (InterruptedException | ExecutionException e) {
			LOG.error(ErrorUtil.getErrorTitle("InvoicePaymentService.create"), e);
		}

		return null;
	}

	@Override
	public boolean update(String id, Map<String, Object> data) {
		data.put(Field.UPDATED_DATE, TimeUtil.currentTime());
		ApiFuture<WriteResult> result = getFireStore().collection(getCollectionName()).document(id).update(data);
		try {
			LOG.info("InvoicePayment [{}] updated, update time: {}", id, result.get().getUpdateTime());
			return true;
		} catch (InterruptedException | ExecutionException e) {
			LOG.error(ErrorUtil.getErrorTitle("InvoicePaymentService.update"), e);
		}

		return false;
	}

	@Override
	public InvoicePayment readById(String id) {
		DocumentReference ref = getFireStore().collection(getCollectionName()).document(id);
		ApiFuture<DocumentSnapshot> future = ref.get();
		try {
			DocumentSnapshot document = future.get();
			if (document.exists()) {
				return document.toObject(InvoicePayment.class);
			}
		} catch (InterruptedException | ExecutionException e) {
			LOG.error(ErrorUtil.getErrorTitle("InvoicePaymentService.readById"), e);
		}

		return null;
	}

	@Override
	public List<Map<String, Object>> readAll(int page, int size, String fields, String keywords, String orderBy,
			String direction) {
		return getCustomData(getCollectionName(),
				PaginatedRequest.builder().page(page).size(size).fields(fields).keywords(keywords).orderBy(orderBy)
						.direction(direction).searchableFields(getSearchableFields()).build());
	}

	@Override
	public boolean delete(String id) {
		try {
			DocumentSnapshot snapshot = getFireStore().collection(getCollectionName()).document(id).get().get();
			if (snapshot.exists()) {
				ApiFuture<WriteResult> result = getFireStore().collection(getCollectionName()).document(id).delete();

				LOG.info("InvoicePayment [{}] deleted, update time: {}", id, result.get().getUpdateTime());
				updateCount(Statistic.COUNTMODE.DECREASE);

				return true;
			}
		} catch (InterruptedException | ExecutionException e) {
			LOG.error(ErrorUtil.getErrorTitle("InvoicePaymentService.delete"), e);
		}

		return false;
	}

	@Override
	public Map<String, Object> readFields(String id, String... fields) {
		ApiFuture<QuerySnapshot> future = getFireStore().collection(getCollectionName()).whereEqualTo(Field.ID, id)
				.select(fields).get();

		QueryDocumentSnapshot snapshot = getSnapshot(future, getCollectionName(), "InvoicePaymentService.readFields");
		if (snapshot != null) {
			return snapshot.getData();
		}

		return null;
	}

	public List<Map<String, Object>> getLatest() {
		ApiFuture<QuerySnapshot> future = getFireStore().collection(getCollectionName())
				.orderBy(Field.UPDATED_DATE, Direction.DESCENDING).limit(10).get();
		List<Map<String, Object>> result = convertToResult(future);
		if (result == null) {
			LOG.error("No latest invoice payment found");

			return new ArrayList<Map<String, Object>>();
		}

		return result;
	}

	@Override
	public int getCount(boolean refresh) {
		if (refresh || cachedCount == 0)
			cachedCount = getCount(getCollectionName());
		return cachedCount;
	}

	@Override
	public List<Searchable> getSearchableFields() {
		return new ArrayList<Searchable>(Arrays.asList(new Searchable(Field.ID, Searchable.Type.STRING),
				new Searchable(Field.USER_ID, Searchable.Type.STRING),
				new Searchable(Field.STATUS, Searchable.Type.STRING),
				new Searchable(Field.TYPE, Searchable.Type.STRING)));
	}

	@Override
	public void populateFields(InvoicePayment data) {
		data.setCreatedDate(TimeUtil.currentTime());
		data.setUpdatedDate(TimeUtil.currentTime());
		data.setStatus(InvoicePayment.Status.Processing);
		data.setId(UuidUtil.generate());
	}

	@Override
	public void populateFields(Map<String, Object> map) {
		map.put(Field.CREATED_DATE, TimeUtil.currentTime());
		map.put(Field.UPDATED_DATE, TimeUtil.currentTime());
		map.put(STATUS, InvoicePayment.Status.Processing);
		map.put(Field.ID, UuidUtil.generate());
	}

	@Override
	public void sendNotification(InvoicePayment data) {
		emailService.sendEmail(EmailRequest.builder().to(EmailService.TO_ORDERS_EMAIL)
				.subject(DEFAULT_TITLE + " - " + TimeUtil.currentTime()).build(), data, getCollectionName());

		webSocketService.sendWebSocketMessage(SOCKET_MESSAGE + data.getId());
	}
}
