package com.otcpay.fx.service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import com.google.api.core.ApiFuture;
import com.google.cloud.firestore.QuerySnapshot;
import com.otcpay.fx.model.rate.CommonRate;
import com.otcpay.fx.model.rate.DailyRate;
import com.otcpay.fx.model.rate.DefaultOtcRate;
import com.otcpay.fx.model.rate.OtcRate;
import com.otcpay.fx.model.rate.Source;
import com.otcpay.fx.model.rate.TTRate;
import com.otcpay.fx.service.google.firebase.FireStoreService;
import com.otcpay.fx.service.internal.SystemService;
import com.otcpay.fx.util.ErrorUtil;
import com.otcpay.fx.util.TimeUtil;

@Service
public class DailyRateService extends FireStoreService {
	private static final Logger LOG = LoggerFactory.getLogger(DailyRateService.class);

	private static final String TTRATE_COL_NAME = "ttrate";
	private static final String TWRATE_COL_NAME = "transferwise";
	private static final String WURATE_COL_NAME = "westernunion";
	private static final String MGRATE_COL_NAME = "moneygram";
	private static final String XERATE_COL_NAME = "xe";
	private static final String XRRATE_COL_NAME = "xrate";

	private static final String COINMILL_COL_NAME = "coinmill";
	private static final String FROM_CURRENCY = "sourceCurrency";
	private static final String TO_CURRENCY = "targetCurrency";
	private static final String TIME = "time";

	@Autowired
	private SystemService systemService;

	/**
	 * Get list of internal default FIAT rate stored in FireStore, with option to
	 * get external rate as reference, which will be used for Hot Page Rate Board,
	 * as well as CMS Global Config page
	 * 
	 * @param showExternal flag to indicate whether to query and include external
	 *                     rate in response
	 * @return list of {@link DailyRate}
	 */
	@SuppressWarnings("unchecked")
	public List<DailyRate> getLatestDefaultDailyRate(boolean showExternal) {
		List<DailyRate> rates = systemService.getSystemCurrency();
		if (showExternal) {
			List<Object> results = Arrays.asList(getLatestTTrate(), getLatestWUrate(), getLatestMGrate(),
					getLatestTWrate(), getLatestXErate(), getLatestXRrate()).parallelStream()
					.collect(Collectors.toList());

			rates.forEach((rate) -> {
				// filter out those match the currency pair
				List<TTRate> ttSources = (List<TTRate>) results.get(0);
				List<TTRate> filteredTTSources = ttSources.stream()
						.filter((s) -> s.getSourceCurrency().equals(rate.getFromCurrency())
								&& s.getTargetCurrency().equals(rate.getToCurrency()))
						.collect(Collectors.toList());
				List<Source> result = transformTTRateToSource(filteredTTSources, DailyRate.Type.SELL);

				int sourceSize = results.size();
				for (int i = 1; i < sourceSize; i++) {
					List<CommonRate> otherSources = (List<CommonRate>) results.get(i);
					List<CommonRate> filteredOtherSources = otherSources.stream()
							.filter((s) -> filterCommonRate(s, rate)).collect(Collectors.toList());
					result.addAll(transformCommonRateToSource(filteredOtherSources));
				}

				result.sort(Comparator.comparing(Source::getOrder, Comparator.naturalOrder())
						.thenComparing(Source::getRate));
				rate.setRates(result);
			});
		}

		return rates;
	}

	/**
	 * Get list of internal default Cryptocurrency Code stored in FireStore, which
	 * will be used to query latest rate in Huobi for Hot Page Index Board
	 * 
	 * @return list of internal default Cryptocurrency Code
	 */
	public List<String> getLatestDefaultCrypto() {
		return Arrays.asList(systemService.getSystemCrypto());
	}

	/**
	 * Get list of internal default OTC rate stored in FireStore, as well as
	 * external OTC rate from CoinMill, which will be used for Hot Page Rate Board
	 * 
	 * @return list of {@link OtcRate}
	 */
	public List<OtcRate> getLatestOtcRate() {
		List<OtcRate> rates = new ArrayList<OtcRate>();

		try {
			QuerySnapshot snapshot = getSnapShotDataOfWholeDay(COINMILL_COL_NAME);

			// map default otc rate to CoinNill rate
			List<DefaultOtcRate> defaultRates = systemService.getSystemDefautlOtcRate();
			snapshot.forEach((doc) -> {
				OtcRate rate = doc.toObject(OtcRate.class);
				insertDefaultRateSetting(defaultRates, rate);
				rates.add(rate);
			});
		} catch (InterruptedException | ExecutionException e) {
			LOG.error(ErrorUtil.getErrorTitle("DailyRateService.getLatestOtcRate"), e);
		}

		rates.sort(Comparator.comparing(OtcRate::getOrder, Comparator.naturalOrder()));
		return rates;
	}

	/**
	 * Get latest daily rate for a particular currency pair and transaction type for
	 * otc mobile platform
	 * 
	 * @param fromCurrency currency to send
	 * @param toCurrency   currency to receive
	 * @param type         type of transcation
	 * @return {@link DailRate}
	 */
	@SuppressWarnings("unchecked")
	public DailyRate getLatestDailyRate(String fromCurrency, String toCurrency, DailyRate.Type type) {
		// get rates from different sources in parallel
		List<Object> results = Arrays
				.asList(getLatestTTRate(fromCurrency, toCurrency, type), getLatestTWRate(fromCurrency, toCurrency),
						getLatestWURate(fromCurrency, toCurrency), getLatestMGRate(fromCurrency, toCurrency),
						getLatestXERate(fromCurrency, toCurrency), getLatestXRRate(fromCurrency, toCurrency))
				.parallelStream().collect(Collectors.toList());

		DailyRate rate = (DailyRate) results.get(0);

		int sourceSize = results.size();
		for (int i = 1; i < sourceSize; i++) {
			rate.getRates().addAll((List<Source>) results.get(i));
		}
		rate.getRates()
				.sort(Comparator.comparing(Source::getOrder, Comparator.naturalOrder()).thenComparing(Source::getRate));
		return rate;
	}

	/**
	 * Return list of external rate from TTRate, which only specific sources will be
	 * included for reference. Normally, this is only used by CMS
	 * 
	 * @return list of {@link TTRate}
	 */
	@Cacheable(value = "TTRate")
	public List<TTRate> getLatestTTrate() {
		List<TTRate> latestRates = new ArrayList<TTRate>();

		try {
			QuerySnapshot snapshot = getSnapShotDataOfWholeDay(TTRATE_COL_NAME);
			snapshot.forEach((doc) -> {
				// Only top most desired rate will used as reference rate for CMS (e.g. HSBC)
				TTRate ttRate = doc.toObject(TTRate.class);
				if (ttRate.getOrder() < 6) {
					latestRates.add(ttRate);
				}
			});
		} catch (InterruptedException | ExecutionException e) {
			LOG.error(ErrorUtil.getErrorTitle("DailyRateService.getLatestTTRate"), e);
		}

		return latestRates;
	}

	/**
	 * Return list of external rate from Western Union, which only specific sources
	 * will be included for reference. Normally, this is only used by CMS
	 * 
	 * @return list of {@link CommonRate}
	 */
	@Cacheable(value = "WURate")
	public List<CommonRate> getLatestWUrate() {
		List<CommonRate> latestRates = new ArrayList<CommonRate>();

		try {
			QuerySnapshot snapshot = getSnapShotDataOfWholeDay(WURATE_COL_NAME);
			snapshot.forEach((doc) -> latestRates.add(doc.toObject(CommonRate.class)));
		} catch (InterruptedException | ExecutionException e) {
			LOG.error(ErrorUtil.getErrorTitle("DailyRateService.getLatestWUrate"), e);
		}

		return latestRates;
	}

	/**
	 * Return list of external rate from MoneyGram, which only specific sources will
	 * be included for reference. Normally, this is only used by CMS
	 * 
	 * @return list of {@link CommonRate}
	 */
	@Cacheable(value = "MGRate")
	public List<CommonRate> getLatestMGrate() {
		List<CommonRate> latestRates = new ArrayList<CommonRate>();

		try {
			QuerySnapshot snapshot = getSnapShotDataOfWholeDay(MGRATE_COL_NAME);
			snapshot.forEach((doc) -> latestRates.add(doc.toObject(CommonRate.class)));
		} catch (InterruptedException | ExecutionException e) {
			LOG.error(ErrorUtil.getErrorTitle("DailyRateService.getLatestMGrate"), e);
		}

		return latestRates;
	}

	/**
	 * Return list of external rate from TransferWise, which only specific sources
	 * will be included for reference. Normally, this is only used by CMS
	 * 
	 * @return list of {@link CommonRate}
	 */
	@Cacheable(value = "TWRate")
	public List<CommonRate> getLatestTWrate() {
		List<CommonRate> latestRates = new ArrayList<CommonRate>();

		try {
			QuerySnapshot snapshot = getSnapShotDataOfWholeDay(TWRATE_COL_NAME);
			snapshot.forEach((doc) -> latestRates.add(doc.toObject(CommonRate.class)));
		} catch (InterruptedException | ExecutionException e) {
			LOG.error(ErrorUtil.getErrorTitle("DailyRateService.getLatestTWrate"), e);
		}

		return latestRates;
	}

	/**
	 * Return list of external rate from XE, which only specific sources will be
	 * included for reference. Normally, this is only used by CMS
	 * 
	 * @return list of {@link CommonRate}
	 */
	@Cacheable(value = "XERate")
	public List<CommonRate> getLatestXErate() {
		List<CommonRate> latestRates = new ArrayList<CommonRate>();

		try {
			QuerySnapshot snapshot = getSnapShotDataOfWholeDay(XERATE_COL_NAME);
			snapshot.forEach((doc) -> latestRates.add(doc.toObject(CommonRate.class)));
		} catch (InterruptedException | ExecutionException e) {
			LOG.error(ErrorUtil.getErrorTitle("DailyRateService.getLatestXErate"), e);
		}

		return latestRates;
	}

	/**
	 * Return list of external rate from X-Rates, which only specific sources will
	 * be included for reference. Normally, this is only used by CMS
	 * 
	 * @return list of {@link CommonRate}
	 */
	@Cacheable(value = "XRRate")
	public List<CommonRate> getLatestXRrate() {
		List<CommonRate> latestRates = new ArrayList<CommonRate>();

		try {
			QuerySnapshot snapshot = getSnapShotDataOfWholeDay(XRRATE_COL_NAME);
			snapshot.forEach((doc) -> latestRates.add(doc.toObject(CommonRate.class)));
		} catch (InterruptedException | ExecutionException e) {
			LOG.error(ErrorUtil.getErrorTitle("DailyRateService.getLatestXRrate"), e);
		}

		return latestRates;
	}

	/**
	 * Get latest FIAT rate with currency pair and transaction type from TTRate
	 * 
	 * @param fromCurrency currency to send
	 * @param toCurrency   currency to receive
	 * @param type         type of transcation
	 * @return List of matching rates
	 */
	public Object getLatestTTRate(String fromCurrency, String toCurrency, DailyRate.Type type) {
		try {
			QuerySnapshot snapshot = constructQuery(TTRATE_COL_NAME, fromCurrency, toCurrency, 0).get();
			if (snapshot.size() == 0) {
				snapshot = constructQuery(TTRATE_COL_NAME, fromCurrency, toCurrency, 1).get();
			}

			return transferTTRateQueryResult(snapshot, fromCurrency, toCurrency, type);
		} catch (InterruptedException | ExecutionException e) {
			LOG.error(ErrorUtil.getErrorTitle("DailyRateService.getLatestTTRate"), e);
		}

		return new DailyRate();
	}

	/**
	 * Get latest FIAT rate with currency pair and transaction type from
	 * TransferWise
	 * 
	 * @param fromCurrency currency to send
	 * @param toCurrency   currency to receive
	 * @param type         type of transcation
	 * @return List of matching rates
	 */
	public Object getLatestTWRate(String fromCurrency, String toCurrency) {
		try {
			QuerySnapshot snapshot = constructQuery(TWRATE_COL_NAME, fromCurrency, toCurrency, 0).get();
			if (snapshot.size() == 0) {
				snapshot = constructQuery(TWRATE_COL_NAME, fromCurrency, toCurrency, 1).get();
			}

			return transferCommonRateQueryResult(snapshot, fromCurrency, toCurrency);
		} catch (InterruptedException | ExecutionException e) {
			LOG.error(ErrorUtil.getErrorTitle("DailyRateService.getLatestTWRate"), e);
		}

		return new ArrayList<Source>();
	}

	/**
	 * Get latest FIAT rate with currency pair and transaction type from
	 * WesternUnion
	 * 
	 * @param fromCurrency currency to send
	 * @param toCurrency   currency to receive
	 * @param type         type of transcation
	 * @return List of matching rates
	 */
	public Object getLatestWURate(String fromCurrency, String toCurrency) {
		try {
			QuerySnapshot snapshot = constructQuery(WURATE_COL_NAME, fromCurrency, toCurrency, 0).get();
			if (snapshot.size() == 0) {
				snapshot = constructQuery(WURATE_COL_NAME, fromCurrency, toCurrency, 1).get();
			}

			return transferCommonRateQueryResult(snapshot, fromCurrency, toCurrency);
		} catch (InterruptedException | ExecutionException e) {
			LOG.error(ErrorUtil.getErrorTitle("DailyRateService.getLatestWURate"), e);
		}

		return new ArrayList<Source>();
	}

	/**
	 * Get latest FIAT rate with currency pair and transaction type from MoenyGram
	 * 
	 * @param fromCurrency currency to send
	 * @param toCurrency   currency to receive
	 * @param type         type of transcation
	 * @return List of matching rates
	 */
	public Object getLatestMGRate(String fromCurrency, String toCurrency) {
		try {
			QuerySnapshot snapshot = constructQuery(MGRATE_COL_NAME, fromCurrency, toCurrency, 0).get();
			if (snapshot.size() == 0) {
				snapshot = constructQuery(MGRATE_COL_NAME, fromCurrency, toCurrency, 1).get();
			}

			return transferCommonRateQueryResult(snapshot, fromCurrency, toCurrency);
		} catch (InterruptedException | ExecutionException e) {
			LOG.error(ErrorUtil.getErrorTitle("DailyRateService.getLatestMGRate"), e);
		}

		return new ArrayList<Source>();
	}

	/**
	 * Get latest FIAT rate with currency pair and transaction type from XE
	 * 
	 * @param fromCurrency currency to send
	 * @param toCurrency   currency to receive
	 * @return List of matching rates
	 */
	@Async
	public Object getLatestXERate(String fromCurrency, String toCurrency) {
		try {
			QuerySnapshot snapshot = constructQuery(XERATE_COL_NAME, fromCurrency, toCurrency, 0).get();
			if (snapshot.size() == 0) {
				snapshot = constructQuery(XERATE_COL_NAME, fromCurrency, toCurrency, 1).get();
			}

			return transferCommonRateQueryResult(snapshot, fromCurrency, toCurrency);
		} catch (InterruptedException | ExecutionException e) {
			LOG.error(ErrorUtil.getErrorTitle("DailyRateService.getLatestXERate"), e);
		}

		return new ArrayList<Source>();
	}

	/**
	 * Get latest FIAT rate with currency pair and transaction type from X-Rate
	 * 
	 * @param fromCurrency currency to send
	 * @param toCurrency   currency to receive
	 * @return List of matching rates
	 */
	@Async
	public Object getLatestXRRate(String fromCurrency, String toCurrency) {
		try {
			QuerySnapshot snapshot = constructQuery(XRRATE_COL_NAME, fromCurrency, toCurrency, 0).get();
			if (snapshot.size() == 0) {
				snapshot = constructQuery(XRRATE_COL_NAME, fromCurrency, toCurrency, 1).get();
			}

			return transferCommonRateQueryResult(snapshot, fromCurrency, toCurrency);
		} catch (InterruptedException | ExecutionException e) {
			LOG.error(ErrorUtil.getErrorTitle("DailyRateService.getLatestXRRate"), e);
		}

		return new ArrayList<Source>();
	}

	private boolean filterCommonRate(CommonRate s, DailyRate rate) {
		try {
			return s.getSourceCurrency().equals(rate.getFromCurrency())
					&& s.getTargetCurrency().equals(rate.getToCurrency());
		} catch (RuntimeException e) {
			LOG.error("Failed to filter WesternUnion source: [{}]", s.toString(), e);
		}

		return false;
	}

	private QuerySnapshot getSnapShotDataOfWholeDay(String collection) throws InterruptedException, ExecutionException {
		// today
		String strDate = TimeUtil.getNDateBeforeAsString(0);
		QuerySnapshot snapshot = getFireStore().collection(collection).whereGreaterThanOrEqualTo(TIME, strDate).get()
				.get();
		if (snapshot.size() == 0) {
			// if no rate in today, get rate from yesterday
			strDate = TimeUtil.getNDateBeforeAsString(1);
			snapshot = getFireStore().collection(collection).whereGreaterThanOrEqualTo(TIME, strDate).get().get();
		}
		return snapshot;
	}

	private ApiFuture<QuerySnapshot> constructQuery(String colName, String fromCurrency, String toCurrency,
			int NumOfDay) {
		String strDate = TimeUtil.getNDateBeforeAsString(NumOfDay);
		ApiFuture<QuerySnapshot> future = getFireStore().collection(colName).whereEqualTo(FROM_CURRENCY, fromCurrency)
				.whereEqualTo(TO_CURRENCY, toCurrency).whereGreaterThanOrEqualTo(TIME, strDate).get();
		return future;
	}

	private void insertDefaultRateSetting(List<DefaultOtcRate> defaultRates, OtcRate rate) {
		DefaultOtcRate dr = null;
		for (int i = 0; i < defaultRates.size(); i++) {
			DefaultOtcRate tempDr = defaultRates.get(i);
			if (tempDr.getSourceCurrency().equals(rate.getSourceCurrency())
					&& tempDr.getTargetCurrency().equals(rate.getTargetCurrency())
					&& tempDr.getType().equals(rate.getType())) {
				dr = tempDr;
				break;
			}
		}

		if (dr != null) {
			rate.setOtcBoardRate(dr.getOtcBoardRate());
			rate.setDefaultRate(dr.isDefaultRate());
		}
	}

	private DailyRate transferTTRateQueryResult(QuerySnapshot snapshot, String fromCurrency, String toCurrency,
			DailyRate.Type type) {
		DailyRate dailyRate = new DailyRate();
		List<TTRate> latestRates = new ArrayList<TTRate>();
		List<String> sourcesString = new ArrayList<String>();

		snapshot.forEach((doc) -> {
			TTRate ttRate = doc.toObject(TTRate.class);
			String source = ttRate.getSource();
			if (!sourcesString.contains(source)) {
				sourcesString.add(source);
				latestRates.add(ttRate);
			}
		});

		List<Source> sources = transformTTRateToSource(latestRates, type);
		dailyRate.setRates(sources);
		dailyRate.setFromCurrency(fromCurrency);
		dailyRate.setToCurrency(toCurrency);
		return dailyRate;
	}

	private List<Source> transformTTRateToSource(List<TTRate> rates, DailyRate.Type type) {
		List<Source> sources = new ArrayList<Source>();
		rates.forEach((rate) -> {
			Source source = new Source();
			source.setName(rate.getSource());
			source.setTime(rate.getTime());
			source.setRate(getRate(rate, type));
			source.setOrder(rate.getOrder());
			sources.add(source);
		});
		return sources.stream().filter((s) -> s.getRate() != 0).collect(Collectors.toList());
	}

	private List<Source> transformCommonRateToSource(List<CommonRate> rates) {
		List<String> sourcesString = new ArrayList<String>();
		List<Source> sources = new ArrayList<Source>();
		rates.forEach((rate) -> {
			String sourceString = rate.getSource();
			if (!sourcesString.contains(sourceString)) {
				sourcesString.add(sourceString);

				Source source = new Source();
				source.setName(rate.getSource());
				source.setTime(rate.getTime());
				source.setRate(rate.getRate());
				source.setOrder(6); // no order is provided in raw data other than those from TTrate
				sources.add(source);
			}
		});
		return sources;
	}

	private List<Source> transferCommonRateQueryResult(QuerySnapshot snapshot, String fromCurrency, String toCurrency) {
		List<CommonRate> latestRates = new ArrayList<CommonRate>();
		List<String> sourcesString = new ArrayList<String>();

		snapshot.forEach((doc) -> {
			CommonRate rate = doc.toObject(CommonRate.class);
			String source = rate.getSource();
			if (!sourcesString.contains(source)) {
				sourcesString.add(source);
				latestRates.add(rate);
			}
		});

		List<Source> sources = new ArrayList<Source>();
		latestRates.forEach((rate) -> {
			Source source = new Source();
			source.setName(rate.getSource());
			source.setTime(rate.getTime());
			source.setRate(rate.getRate());
			source.setOrder(6); // no order is provided in raw data other than those from TTrate
			sources.add(source);
		});
		return sources.stream().filter((s) -> s.getRate() != 0).collect(Collectors.toList());
	}

	private double getRate(TTRate rateObject, DailyRate.Type type) {
		String rate = DailyRate.Type.BUY.equals(type) ? rateObject.getCashBuy() : rateObject.getCashSell();
		try {
			return Double.parseDouble(rate);
		} catch (NumberFormatException e) {
			LOG.error("Failed to parse ttrate {} of type [{}]", rateObject.toString(), type.name());
		}

		return 0;
	}
}
