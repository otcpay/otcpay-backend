package com.otcpay.fx.service.google.firebase;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutionException;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.api.core.ApiFuture;
import com.google.api.core.ApiFutures;
import com.google.cloud.firestore.DocumentReference;
import com.google.cloud.firestore.DocumentSnapshot;
import com.google.cloud.firestore.Firestore;
import com.google.cloud.firestore.Query;
import com.google.cloud.firestore.Query.Direction;
import com.google.cloud.firestore.QueryDocumentSnapshot;
import com.google.cloud.firestore.QuerySnapshot;
import com.google.firebase.cloud.FirestoreClient;
import com.otcpay.fx.model.common.Field;
import com.otcpay.fx.model.common.search.PaginatedRequest;
import com.otcpay.fx.model.common.search.Searchable;
import com.otcpay.fx.model.internal.Statistic;
import com.otcpay.fx.util.ErrorUtil;
import com.otcpay.fx.util.ParamUtil;

public abstract class FireStoreService {
	private static final Logger LOG = LoggerFactory.getLogger(FireStoreService.class);

	protected static Firestore getFireStore() {
		return FirestoreClient.getFirestore();
	}

	/**
	 * Insert multiple data into FireStore in a single transaction. The operation
	 * will always end with all success or all failed with auto-rollback guarded by
	 * FireStore
	 * 
	 * @param colName name of collection
	 * @param data    List of key-value data
	 * @return List of document Id
	 */
	public static List<String> batchInsert(String colName, List<Map<String, Object>> data) {
		try {
			ApiFuture<List<String>> futureTransaction = getFireStore().runTransaction((transaction) -> {
				DocumentReference statRef = getStatDocRef(colName);
				DocumentSnapshot doc = transaction.get(statRef).get();
				List<String> newIds = new ArrayList<String>();
				data.forEach((d) -> {
					String id = (String) d.get(Field.ID);
					// All write operation must come after read operation
					transaction.create(getFireStore().collection(colName).document(id), d);
					newIds.add(id);
				});

				int size = data.size();
				if (doc.exists()) {
					Statistic stat = doc.toObject(Statistic.class);
					stat.setCount(stat.getCount() + size);
					transaction.set(statRef, stat);
				} else {
					Statistic stat = new Statistic(size);
					transaction.set(statRef, stat);
				}
				return newIds;
			});
			return futureTransaction.get();
		} catch (InterruptedException | ExecutionException e) {
			LOG.error(ErrorUtil.getErrorTitle("FireStoreService.batchInsert"), e);
		}

		return null;
	}

	/**
	 * Get counter of number of documents in collection
	 * 
	 * @param colName name of collection
	 * @return
	 */
	protected int getCount(String colName) {
		ApiFuture<DocumentSnapshot> future = getStatDocRef(colName).get();
		try {
			DocumentSnapshot doc = future.get();
			if (doc.exists()) {
				Statistic stat = doc.toObject(Statistic.class);
				return stat.getCount();
			}
		} catch (InterruptedException | ExecutionException e) {
			LOG.error(ErrorUtil.getErrorTitle("FireStoreService.getCount"), e);
		}

		return 0;
	}

	/**
	 * Convert query result into key-value mapping
	 * 
	 * @param futures {@link ApiFuture} of type {@link QuerySnapshot}
	 * @return List of fufilling documents
	 */
	protected List<Map<String, Object>> convertToResult(ApiFuture<QuerySnapshot> future) {
		try {
			QuerySnapshot snapshot = future.get();
			List<QueryDocumentSnapshot> documents = snapshot.getDocuments();
			return documents.stream().map((doc) -> doc.getData()).collect(Collectors.toList());
		} catch (InterruptedException | ExecutionException e) {
			LOG.error(ErrorUtil.getErrorTitle("FireStoreService.convertToResult"), e);
		}

		return null;
	}

	/**
	 * Convert multiple query results into list of key-value mapping
	 * 
	 * @param futures List of {@link ApiFuture} of type {@link QuerySnapshot}
	 * @return List of fufilling documents
	 */
	protected List<Map<String, Object>> convertToResult(List<ApiFuture<QuerySnapshot>> futures) {
		try {
			List<QuerySnapshot> snapshots = ApiFutures.allAsList(futures).get();
			List<QueryDocumentSnapshot> documents = new ArrayList<QueryDocumentSnapshot>();
			snapshots.forEach((snapshot) -> documents.addAll(snapshot.getDocuments()));
			return documents.stream().map((doc) -> doc.getData()).collect(Collectors.toList());
		} catch (InterruptedException | ExecutionException e) {
			LOG.error(ErrorUtil.getErrorTitle("FireStoreService.convertToResult"), e);
		}

		return null;
	}

	/**
	 * Get document(s) with specific filtering, sorting options by keywords matching
	 * from FireStore
	 * 
	 * @param colName name of collection
	 * @param request {@link PaginatedRequest}
	 * @return List of fufilling documents
	 */
	protected List<Map<String, Object>> getCustomData(String colName, PaginatedRequest request) {
		String keywords = request.getKeywords();
		if (request.getSearchableFields() != null && ParamUtil.isAllParamValid(keywords)) {
			List<ApiFuture<QuerySnapshot>> futures = request.getSearchableFields().stream().map((field) -> {
				Query query = getQuery(colName, request);
				if (field.getType().equals(Searchable.Type.ARRAY)) {
					query = query.whereArrayContains(field.getField(), keywords);
				} else {
					query = query.whereEqualTo(field.getField(), keywords);
				}
				return query.get();
			}).collect(Collectors.toList());

			return convertToResult(futures);
		}

		Query query = getQuery(colName, request);
		return convertToResult(query.get());
	}

	/**
	 * Get query snapshot from FireStore Query Future Object
	 * 
	 * @param future     {@link ApiFuture} of type {@link QuerySnapshot}
	 * @param collection name of collection
	 * @param errorTitle title of error, used for uniform logging message when
	 *                   exception caught
	 * @return {@link QueryDocumentSnapshot}
	 */
	protected QueryDocumentSnapshot getSnapshot(ApiFuture<QuerySnapshot> future, String collection, String errorTitle) {
		try {
			QuerySnapshot snapshot = future.get();
			List<QueryDocumentSnapshot> documents = snapshot.getDocuments();
			int size = documents.size();
			if (size == 1) {
				return documents.get(0);
			} else if (size > 1) {
				LOG.error("More than one records found in collection [{}]!", collection);
			}
		} catch (InterruptedException | ExecutionException e) {
			LOG.error(ErrorUtil.getErrorTitle(errorTitle), e);
		}

		return null;
	}

	/**
	 * Update counter of number of documents in a collection with specified mode
	 * 
	 * @param colName collection name
	 * @param mode    mode of operation
	 * @return result of operation
	 */
	protected boolean updateCount(String colName, Statistic.COUNTMODE mode) {
		try {
			ApiFuture<Void> futureTransaction = getFireStore().runTransaction((transaction) -> {
				DocumentReference ref = getStatDocRef(colName);

				DocumentSnapshot doc = ref.get().get();
				if (mode.equals(Statistic.COUNTMODE.INCREASE)) {
					if (doc.exists()) {
						Statistic stat = doc.toObject(Statistic.class);
						stat.setCount(stat.getCount() + 1);
						ref.set(stat);
					} else {
						Statistic stat = new Statistic(1);
						ref.set(stat);
					}
				} else {
					if (doc.exists()) {
						Statistic stat = doc.toObject(Statistic.class);
						stat.setCount(stat.getCount() - 1);
						ref.set(stat);
					} else {
						LOG.info("Statistic document missing, will re-initialize count to zero");
						Statistic stat = new Statistic(0);
						ref.set(stat);
					}
				}
				return null;
			});
			futureTransaction.get();
			return true;
		} catch (InterruptedException | ExecutionException e) {
			LOG.error(ErrorUtil.getErrorTitle("FireStoreService.updateCount"), e);
		}

		return false;
	}

	/**
	 * Get Query object using specific request parameter
	 * 
	 * @param colName name of collection
	 * @param request {@link PaginatedRequest}
	 * @return {@link Query}
	 */
	private Query getQuery(String colName, PaginatedRequest request) {
		String orderBy = request.getOrderBy();
		String direction = request.getDirection();
		Query query = getFireStore().collection(colName).select(request.getFields().split(",")).limit(request.getSize())
				.offset((request.getPage() - 1) * request.getSize());

		// By default order by created date in reverse order
		if (ParamUtil.isAllParamValid(orderBy, direction)) {
			query = query.orderBy(orderBy,
					direction.toLowerCase().equals("asc") ? Direction.ASCENDING : Direction.DESCENDING);
		} else {
			query = query.orderBy("createdDate", Direction.DESCENDING);
		}
		return query;
	}

	private static DocumentReference getStatDocRef(String colName) {
		return getFireStore().collection(colName).document(Statistic.DOC_NAME);
	}

}
