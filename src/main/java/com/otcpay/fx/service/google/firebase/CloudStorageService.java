package com.otcpay.fx.service.google.firebase;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.google.cloud.storage.Blob;
import com.google.cloud.storage.Bucket;
import com.google.firebase.cloud.StorageClient;
import com.otcpay.fx.exception.RestOperationException;
import com.otcpay.fx.model.common.FileResponse;
import com.otcpay.fx.util.UuidUtil;

@Service
public class CloudStorageService {
	private static final Logger LOG = LoggerFactory.getLogger(CloudStorageService.class);

	public enum FileType {
		json, csv
	}

	public Bucket getBucket() {
		return StorageClient.getInstance().bucket();
	}

	public List<FileResponse> listFiles() {
		List<FileResponse> responses = new ArrayList<FileResponse>();

		Iterator<Blob> blobs = getBucket().list().iterateAll().iterator();
		while (blobs.hasNext()) {
			Blob blob = blobs.next();
			String selfLink = blob.getSelfLink();
			String mediaLink = blob.getMediaLink();
			String uploadName = blob.getName();
			long size = blob.getSize();
			long createdTime = blob.getCreateTime();

			FileResponse response = FileResponse.builder().selfLink(selfLink).mediaLink(mediaLink).name(uploadName)
					.size(size).createdTime(createdTime).build();
			responses.add(response);
		}

		return responses;
	}

	public FileResponse uploadFile(MultipartFile file, String folder) {
		LOG.info("Uploading file");

		String fileName = String.format("%s_%s", UuidUtil.generate(), file.getOriginalFilename());
		if (folder != null && !folder.isEmpty())
			fileName = String.format("%s/%s_%s", folder, UuidUtil.generate(), file.getOriginalFilename());

		try {
			Blob blob = getBucket().create(fileName, file.getBytes());
			String selfLink = blob.getSelfLink();
			String mediaLink = blob.getMediaLink();
			String uploadName = blob.getName();
			long size = blob.getSize();
			long createdTime = blob.getCreateTime();

			FileResponse response = FileResponse.builder().selfLink(selfLink).mediaLink(mediaLink).name(uploadName)
					.size(size).createdTime(createdTime).build();

			LOG.info("File [{}] uploaded to {}", fileName, selfLink);
			return response;
		} catch (IOException e) {
			String message = "Failed to upload file";
			LOG.error(message, e);
			throw RestOperationException.createError(message, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	public byte[] downloadFile(String name, String folder) {
		LOG.info("Downloading file [{}]", name);
		Blob blob = getBucket().get(fullName(name, folder));
		return blob == null ? null : blob.getContent();
	}

	public boolean deleteFile(String name, String folder) {
		LOG.info("Deleteing file [{}]", name);
		return getBucket().get(fullName(name, folder)).delete();
	}

	private String fullName(String name, String folder) {
		return String.format("%s/%s", folder, name);

	}
}
