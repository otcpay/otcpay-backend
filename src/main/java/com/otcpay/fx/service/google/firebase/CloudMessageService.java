package com.otcpay.fx.service.google.firebase;

import java.util.List;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import com.google.firebase.messaging.FirebaseMessaging;
import com.google.firebase.messaging.FirebaseMessagingException;
import com.google.firebase.messaging.Message;
import com.google.firebase.messaging.Notification;
import com.google.firebase.messaging.TopicManagementResponse;
import com.otcpay.fx.model.message.NotificationRequest;
import com.otcpay.fx.model.message.SubscriptionRequest;

@Service
public class CloudMessageService {
	private static final Logger LOG = LoggerFactory.getLogger(CloudMessageService.class);

	private static final String BODY = "body";
	private static final String CONTENT = "content";

	public static FirebaseMessaging getMessageInstance() {
		return FirebaseMessaging.getInstance();
	}

	public TopicManagementResponse subscribeToTopic(SubscriptionRequest subscriptionRequest) {
		try {
			return getMessageInstance().subscribeToTopic(getTokens(subscriptionRequest),
					subscriptionRequest.getTopicName());
		} catch (FirebaseMessagingException e) {
			LOG.error("Firebase subscribe to topic fail", e);
		}
		return null;
	}

	public TopicManagementResponse unsubscribeFromTopic(SubscriptionRequest subscriptionRequest) {
		try {
			return getMessageInstance().unsubscribeFromTopic(getTokens(subscriptionRequest),
					subscriptionRequest.getTopicName());
		} catch (FirebaseMessagingException e) {
			LOG.error("Firebase unsubscribe from topic fail", e);
		}
		return null;
	}

	public String sendNotificationToDevice(NotificationRequest notificationRequest) {
		LOG.info("Sending notification to device: {}", notificationRequest.getTarget());
		Message message = Message.builder().setToken(notificationRequest.getTarget())
				.setNotification(Notification.builder().setBody(notificationRequest.getBody())
						.setTitle(notificationRequest.getTitle()).build())
				.putData(CONTENT, notificationRequest.getTitle()).putData(BODY, notificationRequest.getBody()).build();

		String response = null;
		try {
			response = getMessageInstance().send(message);
		} catch (FirebaseMessagingException e) {
			LOG.error("Fail to send firebase notification", e);
		}

		return response;
	}

	public String sendNotificationToTopic(NotificationRequest notificationRequest) {
		Message message = Message.builder().setTopic(notificationRequest.getTarget())
				.setNotification(Notification.builder().setBody(notificationRequest.getBody())
						.setTitle(notificationRequest.getTitle()).build())
				.putData(CONTENT, notificationRequest.getTitle()).putData(BODY, notificationRequest.getBody()).build();

		String response = null;
		try {
			response = getMessageInstance().send(message);
		} catch (FirebaseMessagingException e) {
			LOG.error("Fail to send firebase notification", e);
		}

		return response;
	}

	private List<String> getTokens(SubscriptionRequest subscriptionRequest) {
		return subscriptionRequest.getPayloads().stream().map((payload) -> payload.getTokens())
				.collect(Collectors.toList());
	}
}
