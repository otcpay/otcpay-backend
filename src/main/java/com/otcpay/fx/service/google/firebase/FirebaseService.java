package com.otcpay.fx.service.google.firebase;

import java.io.FileNotFoundException;
import java.io.IOException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.io.ClassPathResource;
import org.springframework.stereotype.Service;

import com.google.auth.oauth2.GoogleCredentials;
import com.google.firebase.FirebaseApp;
import com.google.firebase.FirebaseOptions;
import com.otcpay.fx.util.SystemUtil;

@Service
public class FirebaseService {
	private static final Logger LOG = LoggerFactory.getLogger(FirebaseService.class);

	@SuppressWarnings("deprecation")
	public FirebaseService() {
		try {
			ClassPathResource resource = new ClassPathResource(SystemUtil.getEnv(SystemUtil.FIREBASE_CREDENTIAL));
			GoogleCredentials credentials = GoogleCredentials.fromStream(resource.getInputStream());
			FirebaseOptions options = new FirebaseOptions.Builder().setCredentials(credentials)
					.setStorageBucket(SystemUtil.getEnv(SystemUtil.STORAGE_URL))
					.setDatabaseUrl(SystemUtil.getEnv(SystemUtil.FIREBASE_URL)).build();

			if (FirebaseApp.getApps().size() == 0) {
				FirebaseApp.initializeApp(options);

				LOG.info("Firebase connection initalized");
			}
		} catch (FileNotFoundException e) {
			LOG.error("Firebase credential not found", e);
		} catch (IOException e) {
			LOG.error("Unknown exception reading credential", e);
		}
	}
}
