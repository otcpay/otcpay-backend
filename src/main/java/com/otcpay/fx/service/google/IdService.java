package com.otcpay.fx.service.google;

import java.io.IOException;
import java.security.GeneralSecurityException;
import java.util.Collections;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.google.api.client.googleapis.auth.oauth2.GoogleIdToken;
import com.google.api.client.googleapis.auth.oauth2.GoogleIdToken.Payload;
import com.google.api.client.googleapis.auth.oauth2.GoogleIdTokenVerifier;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.json.jackson2.JacksonFactory;

@Service
public class IdService {
	private static final Logger LOG = LoggerFactory.getLogger(IdService.class);

	@Value("${google.application.clientId}")
	private String CLIENT_ID;

	public boolean validateIDToken(String idTokenString) {
		GoogleIdTokenVerifier verifier = new GoogleIdTokenVerifier.Builder(new NetHttpTransport(), new JacksonFactory())
				.setAudience(Collections.singletonList(CLIENT_ID)).build();

		LOG.info("Token: {}", idTokenString);

		try {
			GoogleIdToken idToken = verifier.verify(idTokenString);
			if (idToken != null) {
				Payload payload = idToken.getPayload();
				return payload.getEmailVerified().booleanValue();
			}
		} catch (GeneralSecurityException | IOException e) {
			LOG.error("Failed to verify ID Token", e);
		}

		return false;
	}
}
