package com.otcpay.fx.service.google;

import com.otcpay.fx.model.secret.jwt.JwtResponse;

public interface IdValidator {
	public JwtResponse execute();
}
