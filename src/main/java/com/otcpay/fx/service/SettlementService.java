package com.otcpay.fx.service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutionException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import com.google.api.core.ApiFuture;
import com.google.cloud.firestore.DocumentReference;
import com.google.cloud.firestore.DocumentSnapshot;
import com.google.cloud.firestore.QueryDocumentSnapshot;
import com.google.cloud.firestore.QuerySnapshot;
import com.google.cloud.firestore.WriteResult;
import com.otcpay.fx.model.Settlement;
import com.otcpay.fx.model.common.Field;
import com.otcpay.fx.model.common.search.PaginatedRequest;
import com.otcpay.fx.model.common.search.Searchable;
import com.otcpay.fx.model.internal.Statistic;
import com.otcpay.fx.service.common.async.AsyncNotifiableService;
import com.otcpay.fx.util.ErrorUtil;
import com.otcpay.fx.util.TimeUtil;
import com.otcpay.fx.util.UuidUtil;

@Service
public class SettlementService extends AsyncNotifiableService<Settlement> {
	private static final Logger LOG = LoggerFactory.getLogger(SettlementService.class);

	public static final String COL_NAME = "Settlements";
	private static final String INVOICE_ID = "invoiceId";

	private int cachedCount = 0;

	@Override
	public String getCollectionName() {
		return COL_NAME;
	}

	@Override
	public Settlement create(Settlement data) {
		try {
			populateFields(data);
			ApiFuture<WriteResult> result = getFireStore().collection(getCollectionName()).document(data.getId())
					.create(data);

			LOG.info("Settlement [{}] created, update time: {}", data.getId(), result.get().getUpdateTime());
			updateCount(Statistic.COUNTMODE.INCREASE);

			return data;
		} catch (InterruptedException | ExecutionException e) {
			LOG.error(ErrorUtil.getErrorTitle("SettlementService.create"), e);
		}

		return null;
	}

	@Override
	public boolean update(String id, Map<String, Object> data) {
		data.put(Field.UPDATED_DATE, TimeUtil.currentTime());
		ApiFuture<WriteResult> result = getFireStore().collection(getCollectionName()).document(id).update(data);
		try {
			LOG.info("Settlement [{}] updated, update time: {}", id, result.get().getUpdateTime());
			return true;
		} catch (InterruptedException | ExecutionException e) {
			LOG.error(ErrorUtil.getErrorTitle("SettlementService.update"), e);
		}

		return false;
	}

	@Override
	public Settlement readById(String id) {
		DocumentReference ref = getFireStore().collection(getCollectionName()).document(id);
		ApiFuture<DocumentSnapshot> future = ref.get();
		try {
			DocumentSnapshot document = future.get();
			if (document.exists()) {
				return document.toObject(Settlement.class);
			}
		} catch (InterruptedException | ExecutionException e) {
			LOG.error(ErrorUtil.getErrorTitle("SettlementService.readById"), e);
		}

		return null;
	}

	@Override
	public List<Map<String, Object>> readAll(int page, int size, String fields, String keywords, String orderBy,
			String direction) {
		return getCustomData(getCollectionName(),
				PaginatedRequest.builder().page(page).size(size).fields(fields).keywords(keywords).orderBy(orderBy)
						.direction(direction).searchableFields(getSearchableFields()).build());
	}

	@Override
	public boolean delete(String id) {
		try {
			DocumentSnapshot snapshot = getFireStore().collection(getCollectionName()).document(id).get().get();
			if (snapshot.exists()) {
				ApiFuture<WriteResult> result = getFireStore().collection(getCollectionName()).document(id).delete();
				LOG.info("Settlement [{}] deleted, update time: {}", id, result.get().getUpdateTime());
				updateCount(Statistic.COUNTMODE.DECREASE);

				return true;
			}
		} catch (InterruptedException | ExecutionException e) {
			LOG.error(ErrorUtil.getErrorTitle("SettlementService.delete"), e);
		}

		return false;
	}

	@Override
	public Map<String, Object> readFields(String id, String... fields) {
		ApiFuture<QuerySnapshot> future = getFireStore().collection(getCollectionName()).whereEqualTo(Field.ID, id)
				.select(fields).get();

		QueryDocumentSnapshot snapshot = getSnapshot(future, getCollectionName(), "SettlementService.readFields");
		if (snapshot != null) {
			return snapshot.getData();
		}

		return null;
	}

	@Override
	public int getCount(boolean refresh) {
		if (refresh || cachedCount == 0)
			cachedCount = getCount(getCollectionName());
		return cachedCount;
	}

	@Override
	public List<Searchable> getSearchableFields() {
		return new ArrayList<Searchable>(Arrays.asList(new Searchable(Field.ID, Searchable.Type.STRING),
				new Searchable(Field.USER_ID, Searchable.Type.STRING),
				new Searchable(INVOICE_ID, Searchable.Type.STRING)));
	}

	@Override
	public void populateFields(Settlement data) {
		data.setCreatedDate(TimeUtil.currentTime());
		data.setUpdatedDate(TimeUtil.currentTime());
		data.setId(UuidUtil.generate());
	}

	@Override
	public void populateFields(Map<String, Object> map) {
		map.put(Field.CREATED_DATE, TimeUtil.currentTime());
		map.put(Field.UPDATED_DATE, TimeUtil.currentTime());
		map.put(Field.ID, UuidUtil.generate());
	}

}
