package com.otcpay.fx.service.common.async;

import org.springframework.scheduling.annotation.Async;

import com.otcpay.fx.model.internal.Statistic;
import com.otcpay.fx.service.common.CommonService;
import com.otcpay.fx.service.google.firebase.FireStoreService;

public abstract class AsyncNotifiableService<T> extends FireStoreService implements CommonService<T> {
	@Async
	public void increaseCountAndSendNotification(T data) {
		updateCount(Statistic.COUNTMODE.INCREASE);

		sendNotification(data);
	}

	@Async
	public void updateCount(Statistic.COUNTMODE mode) {
		updateCount(getCollectionName(), mode);
	}

	/**
	 * Send notification when object is successfully created. By default, it has no
	 * action unless it is overridden
	 * 
	 * @param data Object in <T> type
	 */
	public void sendNotification(T data) {
		return;
	};
}
