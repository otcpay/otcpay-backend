package com.otcpay.fx.service.common;

/**
 * Used for all servces that has connection to FireStore
 * 
 * @author Anthony
 *
 */
public interface DBService {
	/**
	 * To specific which collection the service should points to in FireStore
	 * 
	 * @return Name of the collection
	 */
	public String getCollectionName();
}
