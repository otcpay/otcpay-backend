package com.otcpay.fx.service.common;

import java.util.List;
import java.util.Map;

import com.otcpay.fx.model.common.search.Searchable;

/*
 * Common Service which includes of CRUD operations
 */
public interface CommonService<T> extends DBService {
	/**
	 * Save new document to FireStore
	 * 
	 * @param data Object to be saved
	 * @return Object in <T> type
	 */
	public T create(T data);

	/**
	 * Update document in FireStore by document Id
	 * 
	 * @param id   document Id in FireStore
	 * @param data key-value pair to update
	 * @return result of the operation
	 */
	public boolean update(String id, Map<String, Object> data);

	/**
	 * Get document from FireStore by document Id
	 * 
	 * @param id document Id in FireStore
	 * @return Object in <T> type
	 */
	public T readById(String id);

	/**
	 * Get document(s) with specific filtering, sorting options by keywords matching
	 * from FireStore
	 * 
	 * @param page      page of data
	 * @param size      number of documents to be returned
	 * @param fields    field(s) interested in the query, separated in comma
	 * @param keywords  keyword used in query
	 * @param orderBy   field to order
	 * @param direction ordering direction, either asc or desc
	 * @return List of fufilling document
	 */
	public List<Map<String, Object>> readAll(int page, int size, String fields, String keywords, String orderBy,
			String direction);

	/**
	 * Delete document by document Id in FireStore
	 * 
	 * @param id document Id in FireStore
	 * @return result of the operation
	 */
	public boolean delete(String id);

	/**
	 * Get document with specific fields by document Id in FireStore
	 * 
	 * @param id     document Id in FireStore
	 * @param fields field(s) in the document
	 * @return map of fields selected from a document
	 */
	public Map<String, Object> readFields(String id, String... fields);

	/**
	 * Get number of documents of a specific collection in FireStore
	 * 
	 * @param refresh flag to indicate if number of document(s) need to refreshed
	 *                for latest snapshot or cached number of document(s) is
	 *                acceptable
	 * @return number of documents
	 * 
	 */
	public int getCount(boolean refresh);

	/**
	 * List of field(s) that can be searched by keywords in
	 * {@link #readAll(int, int, String, String, String, String)}
	 * 
	 * @return List of {@link Searchable}
	 */
	public List<Searchable> getSearchableFields();

	/**
	 * Auto fill in some fields in the object when {@link #create(Object)}
	 * 
	 * @param data Object in <T> type
	 */
	public void populateFields(T data);

	/**
	 * Auto fill in some fields in the object during batch insert
	 * 
	 * @param map Object in key-value pair(s)
	 */
	public void populateFields(Map<String, Object> map);
}
