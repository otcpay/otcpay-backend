package com.otcpay.fx.service.external;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import com.huobi.client.MarketClient;
import com.huobi.client.req.market.MarketDetailRequest;
import com.huobi.constant.HuobiOptions;
import com.huobi.constant.Options;
import com.huobi.model.market.MarketDetail;
import com.otcpay.fx.exception.RestOperationException;
import com.otcpay.fx.util.SystemUtil;

@Service
public class HuobiService {
	private static final Logger LOG = LoggerFactory.getLogger(HuobiService.class);

	public static final String DEFAULT_COIN = "USDT";

	private MarketClient marketClient;

	public HuobiService() {
		Options option = HuobiOptions.builder().apiKey(SystemUtil.getEnv(SystemUtil.HUOBI_ACCESS_KEY))
				.secretKey(SystemUtil.getEnv(SystemUtil.HUOBI_SECRET_KEY)).build();
		marketClient = MarketClient.create(option);
	}

	public MarketDetail getRate(String fromCurrency, String toCurrency) {
		try {
			return marketClient.getMarketDetail(MarketDetailRequest.builder()
					.symbol(fromCurrency.toLowerCase() + toCurrency.toLowerCase()).build());
		} catch (Exception e) {
			String message = "Failed to get cryptocurrency from Houbi";
			LOG.error(message, e);
			throw RestOperationException.createError(message);
		}

	}
}
