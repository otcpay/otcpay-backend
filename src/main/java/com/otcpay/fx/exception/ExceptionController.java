package com.otcpay.fx.exception;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.support.DefaultMessageSourceResolvable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException;
import org.springframework.web.multipart.MaxUploadSizeExceededException;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import io.jsonwebtoken.ExpiredJwtException;

/**
 * 
 * Extened implementation of {@link ResponseEntityExceptionHandler}, so that
 * custom exception as well as generic excpetion from framework and runtime can
 * be centralized for management and logging.
 * 
 * @author Anthony
 *
 */
@ControllerAdvice
public class ExceptionController extends ResponseEntityExceptionHandler {
	private static final Logger LOG = LoggerFactory.getLogger(ExceptionController.class);

	@ExceptionHandler(value = Exception.class)
	public ResponseEntity<ExceptionResponse> exception(Exception exception) {
		LOG.error("Unknown Exception occured", exception);
		return new ResponseEntity<ExceptionResponse>(
				new ExceptionResponse("Exception occured during runtime. For error detail, please check log."),
				HttpStatus.INTERNAL_SERVER_ERROR);
	}

	@ExceptionHandler(value = RestOperationException.class)
	public ResponseEntity<ExceptionResponse> restException(RestOperationException exception) {
		String message = "Exception occured during REST Operation. For error detail, please check log.";
		if (exception.getMessage() != null && !exception.getMessage().isEmpty())
			message = exception.getMessage();
		return new ResponseEntity<ExceptionResponse>(new ExceptionResponse(message), exception.getStatus());
	}

	@ExceptionHandler(value = ExpiredJwtException.class)
	public ResponseEntity<ExceptionResponse> tokenException(ExpiredJwtException exception) {
		return new ResponseEntity<ExceptionResponse>(new ExceptionResponse("Token expired"), HttpStatus.FORBIDDEN);
	}

	@ExceptionHandler(value = MethodArgumentTypeMismatchException.class)
	public ResponseEntity<ExceptionResponse> typeMismatch(MethodArgumentTypeMismatchException exception) {
		return new ResponseEntity<ExceptionResponse>(new ExceptionResponse(exception.getMessage()),
				HttpStatus.BAD_REQUEST);
	}

	@ExceptionHandler(MaxUploadSizeExceededException.class)
	public ResponseEntity<ExceptionResponse> handleMaxUploadSizeException(MaxUploadSizeExceededException exc) {
		return new ResponseEntity<ExceptionResponse>(new ExceptionResponse("File too large!"),
				HttpStatus.EXPECTATION_FAILED);
	}

	@Override
	public ResponseEntity<Object> handleHttpMessageNotReadable(HttpMessageNotReadableException exception,
			HttpHeaders headers, HttpStatus status, WebRequest request) {
		return new ResponseEntity<Object>(new ExceptionResponse(exception.getMessage()), HttpStatus.BAD_REQUEST);
	}

	@Override
	protected ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException exception,
			HttpHeaders headers, HttpStatus status, WebRequest request) {
		String errorMessage = exception.getBindingResult().getFieldErrors().stream()
				.map(DefaultMessageSourceResolvable::getDefaultMessage).findFirst().orElse(exception.getMessage());
		return new ResponseEntity<Object>(new ExceptionResponse(errorMessage), HttpStatus.BAD_REQUEST);
	}

}