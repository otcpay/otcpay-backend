package com.otcpay.fx.exception;

import org.springframework.http.HttpStatus;

import lombok.Getter;

/**
 * Static convenience methods to raise exception with default message, status
 * code or all customized
 * 
 * @author Anthony
 *
 */
@Getter
public class RestOperationException extends RuntimeException {
	private static final long serialVersionUID = 1L;

	private HttpStatus status;

	private String message;

	private RestOperationException(String message) {
		this.status = HttpStatus.BAD_REQUEST;
		this.message = message;
	}

	private RestOperationException(String message, HttpStatus status) {
		this.status = status;
		this.message = message;
	}

	public static RestOperationException createError(String message) {
		return new RestOperationException(message);
	}

	public static RestOperationException createError(String message, HttpStatus status) {
		return new RestOperationException(message, status);
	}

	public static RestOperationException createInvalidTokenError() {
		return new RestOperationException(ExceptionResponse.INVALID_TOKEN_MSG, HttpStatus.FORBIDDEN);
	}

	public static RestOperationException createPayloadEmptyError() {
		return new RestOperationException(ExceptionResponse.PAYLOAD_EMPTY_MSG);
	}

	public static RestOperationException createRequsetFieldNotExistError() {
		return new RestOperationException(ExceptionResponse.REQUEST_FIELD_NOT_EXIST_MSG);
	}

	public static RestOperationException createResourceNotFoundError() {
		return new RestOperationException(ExceptionResponse.RESOURCE_NOT_FOUND_MSG, HttpStatus.NOT_FOUND);
	}

	public static RestOperationException createRequestCannotModifyError() {
		return new RestOperationException(ExceptionResponse.REQUEST_FIELD_CANNOT_MODIFY_MSG, HttpStatus.FORBIDDEN);
	}

	public static RestOperationException createNotSeparatedByCommaError() {
		return new RestOperationException(ExceptionResponse.NOT_SEPARATED_BY_COMMA_MSG);
	}

	public static RestOperationException createMustHaveExternalIdError() {
		return new RestOperationException(ExceptionResponse.MUST_HAVE_EXTERNAL_ID_MSG);
	}
}
