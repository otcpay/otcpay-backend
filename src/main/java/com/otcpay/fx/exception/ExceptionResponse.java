package com.otcpay.fx.exception;

import com.otcpay.fx.model.common.CommonResponse;

import lombok.EqualsAndHashCode;
import lombok.Getter;

/**
 * Object to hold message and request result, which will be returned to the
 * client when exception caught
 * 
 * @author Anthony
 *
 */
@EqualsAndHashCode(callSuper = true)
public class ExceptionResponse extends CommonResponse {
	public static String INVALID_TOKEN_MSG = "Invalid Token";

	public static String PAYLOAD_EMPTY_MSG = "Payload cannot be empty";

	public static String REQUEST_FIELD_NOT_EXIST_MSG = "Request fields not exist";

	public static String RESOURCE_NOT_FOUND_MSG = "Resource not found";

	public static String REQUEST_FIELD_CANNOT_MODIFY_MSG = "Request fields are forbidden to modify";

	public static String NOT_SEPARATED_BY_COMMA_MSG = "Custom field(s) must be a single field or a list separated by comma";

	public static String MUST_HAVE_EXTERNAL_ID_MSG = "Must provide fields and external id";

	@Getter
	private String message;

	public ExceptionResponse(String message) {
		super(false);
		this.message = message;
	}
}
