package com.otcpay.fx.annotation.rest;

import static java.lang.annotation.ElementType.TYPE;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.otcpay.fx.rest.Version;

/**
 * 
 * Convenient annotation to make a controller / method to be auto-configured
 * with Spring Boot's REST framework, with versioning control
 * 
 * @author Anthony
 * 
 */
@Retention(RUNTIME)
@Target(TYPE)
@RestController
@RequestMapping(Version.V1)
@CrossOrigin(origins = "*", allowedHeaders = "*")
public @interface RestControllerV1 {
}
