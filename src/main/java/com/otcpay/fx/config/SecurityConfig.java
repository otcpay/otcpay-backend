package com.otcpay.fx.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.Customizer;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.web.authentication.SavedRequestAwareAuthenticationSuccessHandler;

import de.codecentric.boot.admin.server.config.AdminServerProperties;

/**
 * 
 * Extended implementation of {@link WebSecurityConfigurerAdapter}, so that
 * authentication, session management and authorization can be managed by Spring
 * Security accordingly
 * 
 * <p>
 * All requests from actuator will be whitelisted for all checking By default,
 * all requests under sub-domain /api will be redirected to Spring Boot Admin
 * login page. Only those specified for REST API and assets will be excluded.
 * 
 * Session used by Spring Boot Admin as well Swagger UI under sub-domain
 * /api/admin will be managed using Cookies with basic authentication
 * 
 * @author Anthony
 *
 */
@Configuration
public class SecurityConfig extends WebSecurityConfigurerAdapter {
	@Autowired
	private AdminServerProperties adminServer;

	@Bean
	public BCryptPasswordEncoder encoder() {
		return new BCryptPasswordEncoder();
	}

	@Override
	public void configure(WebSecurity web) throws Exception {
		web.ignoring().antMatchers("/actuator/**");
		web.ignoring().antMatchers(HttpMethod.OPTIONS, "/**", "/actuator/**");
	}

	@Override
	protected void configure(HttpSecurity http) throws Exception {
		SavedRequestAwareAuthenticationSuccessHandler successHandler = new SavedRequestAwareAuthenticationSuccessHandler();
		successHandler.setTargetUrlParameter("redirectTo");
		successHandler.setDefaultTargetUrl(this.adminServer.path("/"));

		http.cors().disable().csrf().disable().headers().frameOptions().disable().and()
				.authorizeRequests((authorizeRequests) -> authorizeRequests
						.antMatchers(this.adminServer.path("/assets/**")).permitAll()
						.antMatchers("/actuator/**", "/authenticate", "/validate", "/v1/**", "/system/**",
								"/authorization/**")
						.permitAll().antMatchers(this.adminServer.path("/login")).permitAll().anyRequest()
						.authenticated())
				.formLogin((formLogin) -> formLogin.loginPage(this.adminServer.path("/login"))
						.successHandler(successHandler))
				.logout((logout) -> logout.logoutUrl(this.adminServer.path("/logout")).deleteCookies("JSESSIONID")
						.invalidateHttpSession(true))
				.httpBasic(Customizer.withDefaults());
	}

	@Override
	protected void configure(AuthenticationManagerBuilder auth) throws Exception {
		auth.inMemoryAuthentication().withUser("admin").password(encoder().encode("admin")).roles("USER");
	}
}
