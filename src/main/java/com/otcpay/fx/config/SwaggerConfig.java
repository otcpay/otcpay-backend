package com.otcpay.fx.config;

import java.util.Collections;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.otcpay.fx.rest.Version;

import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

/**
 * 
 * Basic configuration for Swagger UI
 * 
 * @author Anthony
 *
 */
@EnableSwagger2
@Configuration
public class SwaggerConfig {
	@Bean
	public Docket api() {
		return new Docket(DocumentationType.SWAGGER_2).select()
				.apis(RequestHandlerSelectors.basePackage("com.otcpay.fx")).paths(PathSelectors.any()).build()
				.apiInfo(apiInfo());
	}

	private ApiInfo apiInfo() {
		return new ApiInfo("OCTPAY-FX REST API", "This is the backend API of OCTPAY FX platform", Version.V1,
				"Terms of service", new Contact("Anthony NG", "https://www.otcpay.asia", "octpaydev@gmail.com"),
				"License of API", "https://www.otcpay.asia", Collections.emptyList());
	}
}
