package com.otcpay.fx.config.handler;

import java.lang.reflect.Method;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.aop.interceptor.AsyncUncaughtExceptionHandler;

/**
 * 
 * Simple implementation of {@link AsyncUncaughtExceptionHandler}, so that
 * exception thrown in async operationcan be caught properly and logged in
 * logging system without being ignored
 * 
 * @author Anthony
 *
 */
public class CustomAsyncExceptionHandler implements AsyncUncaughtExceptionHandler {
	private static final Logger LOG = LoggerFactory.getLogger(CustomAsyncExceptionHandler.class);

	@Override
	public void handleUncaughtException(Throwable throwable, Method method, Object... obj) {
		LOG.error("Uncaught exception in async method [{}]: {}", method.getName(), throwable);
	}

}