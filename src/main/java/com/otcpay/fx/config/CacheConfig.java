package com.otcpay.fx.config;

import java.util.concurrent.TimeUnit;

import org.springframework.cache.CacheManager;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.cache.caffeine.CaffeineCacheManager;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.github.benmanes.caffeine.cache.Caffeine;
import com.otcpay.fx.util.SystemUtil;

/**
 * 
 * Simple implementation of {@link CacheManager}, so that all frequently
 * accessed resource can be cached in RAM without querying cloud resource.
 * 
 * <p>
 * Expiration time and maximnum size of cache can is configurable by ENV VAR
 * 
 * @author Anthony
 *
 */
@Configuration
@EnableCaching(proxyTargetClass = true)
public class CacheConfig {
	@Bean
	public Caffeine<Object, Object> caffeineConfig() {
		return Caffeine.newBuilder()
				.expireAfterWrite(SystemUtil.getEnvAsInt(SystemUtil.CACHE_EXPIRE_DURATION), TimeUnit.MINUTES)
				.maximumSize(SystemUtil.getEnvAsInt(SystemUtil.CACHE_MAXINUM_SIZE));
	}

	@Bean
	public CacheManager cacheManager(Caffeine<Object, Object> caffeine) {
		CaffeineCacheManager caffeineCacheManager = new CaffeineCacheManager();
		caffeineCacheManager.setCaffeine(caffeine);
		return caffeineCacheManager;
	}

}