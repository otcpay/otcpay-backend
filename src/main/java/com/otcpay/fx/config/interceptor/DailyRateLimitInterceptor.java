package com.otcpay.fx.config.interceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerInterceptor;

import com.otcpay.fx.service.internal.BucketService;

import io.github.bucket4j.Bucket;
import io.github.bucket4j.ConsumptionProbe;

/**
 * 
 * Simple implementation of {@link HandlerInterceptor}, so that resource
 * consuming service will never be occupied by one client. Limit is imposed per
 * ip, remaining value and cooldown period is added to response header
 * 
 * <p>
 * This is used for rate limiting fiat-rate and crypto-rate query
 * 
 * @author Anthony
 * 
 */
@Component
public class DailyRateLimitInterceptor implements HandlerInterceptor {
	private static final Logger LOG = LoggerFactory.getLogger(DailyRateLimitInterceptor.class);

	@Autowired
	private BucketService bucketService;

	@Override
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler)
			throws Exception {
		String ip = request.getRemoteAddr();
		LOG.info("Remote IP: {}", ip);

		long waitForRefill = 60;

		Bucket ipBucket = bucketService.ipBucket(ip);
		if (ipBucket != null) {
			ConsumptionProbe probe = ipBucket.tryConsumeAndReturnRemaining(1);
			if (probe.isConsumed()) {
				response.addHeader("X-Rate-Limit-Remaining", String.valueOf(probe.getRemainingTokens()));
				return true;
			}

			waitForRefill = probe.getNanosToWaitForRefill() / 1_000_000_000;
		}
		response.addHeader("X-Rate-Limit-Retry-After-Seconds", String.valueOf(waitForRefill));
		response.sendError(HttpStatus.TOO_MANY_REQUESTS.value(), "Too much request");
		return false;
	}
}
