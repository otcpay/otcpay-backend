package com.otcpay.fx.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import com.otcpay.fx.config.interceptor.DailyRateLimitInterceptor;

/**
 * 
 * Simple implementation of {@link WebMvcConfigurer}, so that resource consuming
 * can be intercepted for rate limit checking
 * 
 * @author Anthony
 *
 */
@Configuration
public class WebMvcConfig implements WebMvcConfigurer {

	@Autowired
	private DailyRateLimitInterceptor interceptor;

	@Override
	public void addInterceptors(InterceptorRegistry registry) {
		registry.addInterceptor(interceptor).addPathPatterns("/v1/fiat-rates");
		registry.addInterceptor(interceptor).addPathPatterns("/v1/crypto-rates");
	}
}