package com.otcpay.fx.rest.internal;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.ResponseStatus;

import com.otcpay.fx.annotation.rest.RestControllerV1;
import com.otcpay.fx.exception.RestOperationException;
import com.otcpay.fx.model.message.MessageResponse;
import com.otcpay.fx.model.message.Notification;
import com.otcpay.fx.model.message.NotificationRequest;
import com.otcpay.fx.model.message.StatusNotification;
import com.otcpay.fx.rest.Version;
import com.otcpay.fx.service.NotificationService;
import com.otcpay.fx.service.google.firebase.CloudMessageService;
import com.otcpay.fx.util.secret.impl.JwtTokenUtil;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;

@RestControllerV1
@Api(value = Version.V1, tags = "Notifications in OCTPAY Fx System")
public class NotificationController {
	@Autowired
	private CloudMessageService cloudMessageService;

	@Autowired
	private NotificationService notificationService;

	@Autowired
	private JwtTokenUtil jwtTokenUtil;

	@PostMapping(value = "/notification/token", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiOperation(httpMethod = "POST", value = "Send notification to device", response = MessageResponse.class, nickname = "sendNotificationToDevice", notes = "Send notification to specifc devcie by token")
	@ApiImplicitParam(name = "Authorization", value = "Jwt Token", dataType = "string", paramType = "header")
	public MessageResponse sendNotificationToDevice(@RequestHeader(name = "Authorization") String token,
			@RequestBody NotificationRequest notificationRequest) {
		if (!jwtTokenUtil.isTokenExpired(token)) {
			notificationService.pushSingleNotification(notificationRequest);

			String messageId = cloudMessageService.sendNotificationToDevice(notificationRequest);
			if (messageId == null) {
				throw RestOperationException.createError(
						String.format("Failed to send notification to device [%s]", notificationRequest.getTarget()));
			}
			return new MessageResponse(messageId);
		}

		throw RestOperationException.createInvalidTokenError();
	}

	@PostMapping(value = "/notification/user", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiOperation(httpMethod = "POST", value = "Send status notification to user", response = MessageResponse.class, nickname = "sendStatusNotification", notes = "Send notification to specifc user by userId")
	@ApiImplicitParam(name = "Authorization", value = "Jwt Token", dataType = "string", paramType = "header")
	public MessageResponse sendStatusNotification(@RequestHeader(name = "Authorization") String token,
			@RequestBody StatusNotification notificationRequest) {
		if (!jwtTokenUtil.isTokenExpired(token)) {
			NotificationRequest request = notificationService.pushStatusNotificationByUserId(notificationRequest);

			if (request != null) {
				String messageId = cloudMessageService.sendNotificationToDevice(request);
				if (messageId == null) {
					throw RestOperationException.createError(
							String.format("Failed to send notification to user [%s]", notificationRequest.getUserId()));
				}
				return new MessageResponse(messageId);
			}
		}

		throw RestOperationException.createInvalidTokenError();
	}

	@PostMapping(value = "/notification/topic", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiOperation(httpMethod = "POST", value = "Send notification to topic", response = MessageResponse.class, nickname = "sendNotificationToTopic", notes = "Send notification to specific topic subscribed by group of devices")
	@ApiImplicitParam(name = "Authorization", value = "Jwt Token", dataType = "string", paramType = "header")
	public MessageResponse sendNotificationToTopic(@RequestHeader(name = "Authorization") String token,
			@RequestBody NotificationRequest notificationRequest) {
		if (!jwtTokenUtil.isTokenExpired(token)) {
			notificationService.pushMultipleNotification(notificationRequest);

			String messageId = cloudMessageService.sendNotificationToTopic(notificationRequest);
			if (messageId == null) {
				throw RestOperationException.createError(
						String.format("Failed to send notification to topic [%s]", notificationRequest.getTarget()));
			}
			return new MessageResponse(messageId);
		}

		throw RestOperationException.createInvalidTokenError();
	}

	@PostMapping(value = "/notifications/read/{id}")
	@ApiOperation(httpMethod = "POST", value = "Read all notifications", nickname = "readAllNotification", notes = "Real all unread notifications")
	@ApiImplicitParam(name = "Authorization", value = "Jwt Token", dataType = "string", paramType = "header")
	@ResponseStatus(HttpStatus.OK)
	public void readAllNotification(@RequestHeader(name = "Authorization") String token,
			@PathVariable(value = "id", required = true) String id) {
		if (!jwtTokenUtil.isTokenExpired(token)) {
			notificationService.readAllNotification(id);
		} else {
			throw RestOperationException.createInvalidTokenError();
		}
	}

	@GetMapping(value = "/notifications/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiOperation(httpMethod = "GET", value = "Get notifications", response = Notification.class, responseContainer = "List", nickname = "getNotifications", notes = "Get notifications by specific user id")
	@ApiImplicitParam(name = "Authorization", value = "Jwt Token", dataType = "string", paramType = "header")
	public List<Notification> getNotifications(@RequestHeader(name = "Authorization") String token,
			@PathVariable(value = "id", required = true) String id) {
		if (!jwtTokenUtil.isTokenExpired(token)) {
			return notificationService.pullNotification(id);
		}

		throw RestOperationException.createInvalidTokenError();
	}

	@DeleteMapping(value = "/notifications/all/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiOperation(httpMethod = "DELETE", value = "Clear notifications", nickname = "clearNotifications", notes = "Clear all notifications of specific user")
	@ApiImplicitParam(name = "Authorization", value = "Jwt Token", dataType = "string", paramType = "header")
	public void clearNotification(@RequestHeader(name = "Authorization") String token,
			@PathVariable(value = "id", required = true) String id) {
		if (!jwtTokenUtil.isTokenExpired(token)) {
			notificationService.clearNotification(id);
		} else {
			throw RestOperationException.createInvalidTokenError();
		}
	}
}
