package com.otcpay.fx.rest.internal.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.otcpay.fx.exception.RestOperationException;
import com.otcpay.fx.model.User;
import com.otcpay.fx.model.secret.jwt.JwtRequest;
import com.otcpay.fx.model.secret.jwt.JwtResponse;
import com.otcpay.fx.model.secret.jwt.ValidateResponse;
import com.otcpay.fx.service.UserService;
import com.otcpay.fx.service.google.IdService;
import com.otcpay.fx.service.google.IdValidator;
import com.otcpay.fx.service.internal.BucketService;
import com.otcpay.fx.util.ParamUtil;
import com.otcpay.fx.util.secret.TokenUtil;
import com.otcpay.fx.util.secret.TokenUtil.Provider;
import com.otcpay.fx.util.secret.impl.JwtTokenUtil;

import io.swagger.annotations.ApiParam;
import springfox.documentation.annotations.ApiIgnore;

@RestController
@CrossOrigin(origins = "*", allowedHeaders = "*")
@ApiIgnore
public class JwtAuthenticationController {
	@Autowired
	private JwtTokenUtil jwtTokenUtil;

	@Autowired
	private UserService userService;

	@Autowired
	private IdService googleService;

	@Autowired
	private BucketService bucketSevice;

	@Autowired
	private BCryptPasswordEncoder encoder;

	@RequestMapping(value = "/authenticate", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public JwtResponse createAuthenticationToken(
			@ApiParam(value = "provider") @RequestParam(value = "provider", required = false) Provider provider,
			@RequestBody JwtRequest authenticationRequest) throws Exception {
		if (ParamUtil.isAllParamValid(authenticationRequest.getPassword(), authenticationRequest.getUsername())) {
			if (bucketSevice.usernameBucket(authenticationRequest.getUsername()).tryConsume(1)) {
				JwtResponse response = vaidateCallback(provider, authenticationRequest, new IdValidator() {
					@Override
					public JwtResponse execute() {
						return generateTokenIfUserFound(authenticationRequest.getUsername(),
								authenticationRequest.getPassword());
					}
				});

				if (response == null) {
					// normal email / mobile authentication
					return generateTokenIfUserFound(authenticationRequest.getUsername(),
							authenticationRequest.getPassword());
				} else {
					// third party authentication with Id Token
					return response;
				}
			}

			throw RestOperationException.createError("Too many authentication request", HttpStatus.TOO_MANY_REQUESTS);
		} else if (ParamUtil.isAllParamValid(authenticationRequest.getToken())
				&& authenticationRequest.getUser() != null) {
			// create user with Id token
			JwtResponse response = vaidateCallback(provider, authenticationRequest, new IdValidator() {
				@Override
				public JwtResponse execute() {
					User newUser = userService.create(authenticationRequest.getUser());
					if (newUser == null) {
						throw RestOperationException.createError("Failed to create user",
								HttpStatus.INTERNAL_SERVER_ERROR);
					}

					return generateTokenIfUserFound(authenticationRequest.getUser().getAccount(),
							authenticationRequest.getUser().getGoogleId());
				}
			});

			if (response != null) {
				return response;
			}

			throw RestOperationException.createError("Cannot generate token", HttpStatus.INTERNAL_SERVER_ERROR);
		}

		throw RestOperationException
				.createError("Please provide both username and password, or Id Token with user detail");
	}

	@RequestMapping(value = "/validate", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public ValidateResponse validateToken(@RequestBody JwtResponse jwtResponse) {
		if (!jwtTokenUtil.isTokenExpired(TokenUtil.Type.BEARER.toString() + " " + jwtResponse.getJwttoken())) {
			String token = jwtResponse.getJwttoken();
			String role = jwtTokenUtil.getRoleFromToken(token);
			String userId = jwtTokenUtil.getUserIdFromToken(token);
			String username = jwtTokenUtil.getUsernameFromToken(token);

			return ValidateResponse.builder().userId(userId).username(username).role(User.Type.valueOf(role)).build();
		}

		throw RestOperationException.createInvalidTokenError();
	}

	private JwtResponse vaidateCallback(Provider provider, JwtRequest authenticationRequest, IdValidator validator) {
		if (provider != null && provider.equals(Provider.GOOGLE)) {
			boolean valid = googleService.validateIDToken(authenticationRequest.getToken());
			if (valid) {
				return validator.execute();
			} else {
				throw RestOperationException.createError("User is not valid", HttpStatus.UNAUTHORIZED);
			}
		}

		return null;
	}

	private JwtResponse generateTokenIfUserFound(String username, String password) {
		User user = userService.findUser(username);
		if (user == null) {
			throw RestOperationException.createError("User not found", HttpStatus.NOT_FOUND);
		}

		if (encoder.matches(password, user.getPassword())) {
			final String token = jwtTokenUtil.generateToken(username, user.getType().name(), user.getId());
			return JwtResponse.builder().jwttoken(token).role(user.getType()).userId(user.getId()).build();
		}

		throw RestOperationException.createError("Credential not matches", HttpStatus.UNAUTHORIZED);
	}
}
