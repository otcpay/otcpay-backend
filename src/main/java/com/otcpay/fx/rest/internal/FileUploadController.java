package com.otcpay.fx.rest.internal;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.multipart.MultipartFile;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.MappingIterator;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.csv.CsvMapper;
import com.fasterxml.jackson.dataformat.csv.CsvSchema;
import com.otcpay.fx.annotation.rest.RestControllerV1;
import com.otcpay.fx.exception.RestOperationException;
import com.otcpay.fx.model.common.BatchResponse;
import com.otcpay.fx.model.common.CommonResponse;
import com.otcpay.fx.model.common.FileResponse;
import com.otcpay.fx.rest.Version;
import com.otcpay.fx.service.UserService;
import com.otcpay.fx.service.common.async.AsyncNotifiableService;
import com.otcpay.fx.service.google.firebase.CloudStorageService;
import com.otcpay.fx.service.google.firebase.FireStoreService;
import com.otcpay.fx.service.internal.SystemService;
import com.otcpay.fx.util.secret.impl.JwtTokenUtil;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

@RestControllerV1
@Api(value = Version.V1, tags = "File upload in OCTPAY Fx System")
public class FileUploadController {
	private static final Logger LOG = LoggerFactory.getLogger(FileUploadController.class);

	private final CsvMapper csvMapper = new CsvMapper();

	@Autowired
	private CloudStorageService storageService;

	@Autowired
	private UserService userService;

	@Autowired
	private SystemService systemService;

	@Autowired
	private JwtTokenUtil jwtTokenUtil;

	@RequestMapping(value = "/file", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
	@ApiOperation(httpMethod = "POST", value = "Upload file", response = FileResponse.class, nickname = "uploadFile", notes = "Upload file to cloud storage", consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
	@ApiImplicitParam(name = "Authorization", value = "Jwt Token", dataType = "string", paramType = "header")
	public FileResponse uploadFile(@RequestHeader(name = "Authorization") String token,
			@RequestPart("type") String type, @RequestPart("file") MultipartFile file) {
		if (!jwtTokenUtil.isTokenExpired(token)) {
			if (file == null || file.getSize() == 0) {
				throw RestOperationException.createError("Cannot pass empty file");
			}

			return storageService.uploadFile(file, type);
		}

		throw RestOperationException.createInvalidTokenError();
	}

	@RequestMapping(value = "/files", method = RequestMethod.GET)
	@ApiOperation(httpMethod = "GET", value = "List file", nickname = "listFile", notes = "List file from cloud storage")
	@ApiImplicitParam(name = "Authorization", value = "Jwt Token", dataType = "string", paramType = "header")
	public List<FileResponse> listFile(@RequestHeader(name = "Authorization") String token) {
		if (!jwtTokenUtil.isTokenExpired(token)) {
			return storageService.listFiles();
		}

		throw RestOperationException.createInvalidTokenError();
	}

	@RequestMapping(value = "/file/{filename}", method = RequestMethod.GET)
	@ApiOperation(httpMethod = "GET", value = "Download file", nickname = "downloadFile", notes = "Download file from cloud storage")
	@ApiImplicitParam(name = "Authorization", value = "Jwt Token", dataType = "string", paramType = "header")
	public ResponseEntity<Resource> downloadFile(@RequestHeader(name = "Authorization") String token,
			@ApiParam(value = "folder", required = false) @RequestParam(value = "folder", required = false) String folder,
			@ApiParam(value = "filename", required = true) @PathVariable(value = "filename", required = true) String filename)
			throws IOException {
		if (!jwtTokenUtil.isTokenExpired(token)) {
			byte[] data = storageService.downloadFile(filename, folder);
			if (data == null) {
				throw RestOperationException.createError("File not found", HttpStatus.NOT_FOUND);
			}

			ByteArrayResource resource = new ByteArrayResource(data);

			HttpHeaders headers = new HttpHeaders();
			headers.add(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=" + filename);
			return ResponseEntity.ok().headers(headers).contentLength(data.length)
					.contentType(MediaType.APPLICATION_OCTET_STREAM).body(resource);
		}

		throw RestOperationException.createInvalidTokenError();
	}

	@RequestMapping(value = "/file/{filename}", method = RequestMethod.DELETE, produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiOperation(httpMethod = "DELETE", value = "Delete file", nickname = "deleteFile", notes = "Delete file from cloud storage")
	@ApiImplicitParam(name = "Authorization", value = "Jwt Token", dataType = "string", paramType = "header")
	public CommonResponse deleteFile(@RequestHeader(name = "Authorization") String token,
			@ApiParam(value = "filename", required = true) @PathVariable(value = "filename", required = true) String filename,
			@ApiParam(value = "folder", required = false) @RequestParam(value = "folder", required = false) String folder)
			throws IOException {
		if (!jwtTokenUtil.isTokenExpired(token)) {
			return new CommonResponse(storageService.deleteFile(filename, folder));
		}

		throw RestOperationException.createInvalidTokenError();
	}

	@RequestMapping(value = "/{resource}/batch/{fileType}", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
	@ApiOperation(httpMethod = "POST", value = "Batch import", response = CommonResponse.class, nickname = "batchImport", notes = "Batch import data", consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
	@ApiImplicitParam(name = "Authorization", value = "Jwt Token", dataType = "string", paramType = "header")
	public BatchResponse batchUpload(@RequestHeader(name = "Authorization") String token,
			@ApiParam(value = "fileType", required = true) @PathVariable(value = "fileType", required = true) CloudStorageService.FileType fileType,
			@ApiParam(value = "resource", required = true) @PathVariable(value = "resource", required = true) String resource,
			@RequestPart("file") MultipartFile file) throws IOException {
		if (!jwtTokenUtil.isTokenExpired(token)) {
			if (file == null || file.getSize() == 0) {
				throw RestOperationException.createError("Cannot pass empty file");
			}

			List<Map<String, Object>> results = new ArrayList<Map<String, Object>>();
			if (resource.equals(UserService.COL_NAME)) {
				results = convertToList(userService, fileType, file.getBytes());
			} else {
				throw RestOperationException.createError("Unknown resource", HttpStatus.EXPECTATION_FAILED);
			}

			BatchResponse response = BatchResponse
					.withErrors(Arrays.asList("Failed to insert batch data, please check log for detail"));
			List<String> ids = FireStoreService.batchInsert(resource, results);
			if (ids != null && ids.size() > 0) {
				response = BatchResponse.withIds(ids);
			}

			return response;
		}

		throw RestOperationException.createInvalidTokenError();
	}

	protected <T> List<Map<String, Object>> convertToList(AsyncNotifiableService<T> service,
			CloudStorageService.FileType fileType, byte[] bytes) throws IOException {
		List<Map<String, Object>> data = new ArrayList<Map<String, Object>>();
		ObjectMapper mapper = systemService.getObjectMapper();
		if (fileType.equals(CloudStorageService.FileType.json)) {
			try {
				data = mapper.readValue(bytes, new TypeReference<List<Map<String, Object>>>() {
				});
			} catch (IOException e) {
				String message = "Failed to parse json";
				LOG.error(message, e);
				throw RestOperationException.createError(message, HttpStatus.INTERNAL_SERVER_ERROR);
			}
		}

		if (fileType.equals(CloudStorageService.FileType.csv)) {
			CsvSchema csv = CsvSchema.emptySchema().withHeader();
			try {
				MappingIterator<Map<String, Object>> mappingIterator = csvMapper.reader()
						.forType(new TypeReference<Map<String, Object>>() {
						}).with(csv).readValues(bytes);
				data = mappingIterator.readAll();
			} catch (IOException e) {
				String message = "Failed to parse csv";
				LOG.error(message, e);
				throw RestOperationException.createError(message, HttpStatus.INTERNAL_SERVER_ERROR);
			}
		}

		data.forEach((d) -> service.populateFields(d));
		return mapper.convertValue(data, new TypeReference<List<Map<String, Object>>>() {
		});
	}

}
