package com.otcpay.fx.rest.internal;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.otcpay.fx.exception.RestOperationException;
import com.otcpay.fx.model.common.CommonResponse;
import com.otcpay.fx.model.internal.Banner;
import com.otcpay.fx.model.internal.DetailStatistic;
import com.otcpay.fx.model.internal.EmailRequest;
import com.otcpay.fx.model.internal.FirebaseStatus;
import com.otcpay.fx.model.internal.LastUpdatedTime;
import com.otcpay.fx.model.rate.DefaultFiatRate;
import com.otcpay.fx.model.rate.DefaultOtcRate;
import com.otcpay.fx.service.internal.EmailService;
import com.otcpay.fx.service.internal.SystemService;
import com.otcpay.fx.util.secret.impl.ApiKeyUtil;
import com.otcpay.fx.util.secret.impl.JwtTokenUtil;

import io.swagger.annotations.ApiParam;
import springfox.documentation.annotations.ApiIgnore;

@RestController
@RequestMapping("/system")
@CrossOrigin(origins = "*", allowedHeaders = "*")
@ApiIgnore
public class SystemController {
	@Autowired
	private SystemService systemService;

	@Autowired
	private EmailService emailService;

	@Autowired
	private JwtTokenUtil jwtTokenUtil;

	@Autowired
	private ApiKeyUtil apiKeyUtil;

	@RequestMapping(value = "/status", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseStatus(HttpStatus.OK)
	public void status() {
	}

	@RequestMapping(value = "/email", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseStatus(HttpStatus.OK)
	public void sendEmail(@RequestHeader(name = "Authorization") String token, @RequestBody EmailRequest request) {
		if (jwtTokenUtil.isAdminToken(token)) {
			emailService.sendEmail(request);
		}
	}

	@RequestMapping(value = "/info", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public Map<String, DetailStatistic> info(@RequestHeader(name = "Authorization") String token) {
		if (jwtTokenUtil.isAdminToken(token)) {
			return systemService.getSystemStatistic();
		}

		throw RestOperationException.createInvalidTokenError();
	}

	@RequestMapping(value = "/detail", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public Map<String, Map<String, Object>> detail(@RequestHeader(name = "Authorization") String token) {
		if (jwtTokenUtil.isAdminToken(token)) {
			return systemService.getSystemStatisticDetail();
		}

		throw RestOperationException.createInvalidTokenError();
	}

	@RequestMapping(value = "/incident", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public FirebaseStatus[] coreStatus(@RequestHeader(name = "Authorization") String token) {
		if (jwtTokenUtil.isAdminToken(token)) {
			return systemService.getCoreServiceStatus();
		}

		throw RestOperationException.createInvalidTokenError();
	}

	@RequestMapping(value = "/mapping", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public Map<String, Object> mapping(@RequestHeader(name = "Authorization") String token) {
		if (apiKeyUtil.isTokenValid(token)) {
			return systemService.getSystemMapping();
		}

		throw RestOperationException.createInvalidTokenError();
	}

	@RequestMapping(value = "/config/time", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseStatus(HttpStatus.OK)
	public LastUpdatedTime getStatisticTime() {
		return systemService.getStatisticLastUpdatedTime();
	}

	@RequestMapping(value = "/config/time", method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseStatus(HttpStatus.OK)
	public CommonResponse updateStatisticTime(@RequestHeader(name = "Authorization") String token,
			@RequestBody LastUpdatedTime newTime) {
		if (jwtTokenUtil.isAdminToken(token)) {
			return new CommonResponse(systemService.updateStatisticLastUpdatedTime(newTime.getLastUpdatedTime()));
		}

		throw RestOperationException.createInvalidTokenError();
	}

	@RequestMapping(value = "/config/currency", method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseStatus(HttpStatus.OK)
	public CommonResponse updateCurrencyConfig(@RequestHeader(name = "Authorization") String token,
			@RequestBody List<DefaultFiatRate> rates) {
		if (jwtTokenUtil.isAdminToken(token)) {
			return new CommonResponse(systemService.updateCurrencyConfig(rates));
		}

		throw RestOperationException.createInvalidTokenError();
	}

	@RequestMapping(value = "/config/currency", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseStatus(HttpStatus.OK)
	public CommonResponse addCurrencyConfig(@RequestHeader(name = "Authorization") String token,
			@RequestBody DefaultFiatRate rate) {
		if (jwtTokenUtil.isAdminToken(token)) {
			return new CommonResponse(systemService.addCurrencyConfig(rate));
		}

		throw RestOperationException.createInvalidTokenError();
	}

	@RequestMapping(value = "/config/banner", method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseStatus(HttpStatus.OK)
	public CommonResponse updateBannerConfig(@RequestHeader(name = "Authorization") String token,
			@ApiParam(value = "target", required = true) @RequestParam(value = "target", required = true) String target,
			@RequestBody List<Banner> banners) {
		if (jwtTokenUtil.isAdminToken(token)) {
			return new CommonResponse(systemService.updateBannerConfig(banners, target));
		}

		throw RestOperationException.createInvalidTokenError();
	}

	@RequestMapping(value = "/config/banner", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseStatus(HttpStatus.OK)
	public CommonResponse addBannerConfig(@RequestHeader(name = "Authorization") String token,
			@ApiParam(value = "target", required = true) @RequestParam(value = "target", required = true) String target,
			@RequestBody Banner banner) {
		if (jwtTokenUtil.isAdminToken(token)) {
			return new CommonResponse(systemService.addBannerConfig(banner, target));
		}

		throw RestOperationException.createInvalidTokenError();
	}

	@RequestMapping(value = "/config/otc", method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseStatus(HttpStatus.OK)
	public CommonResponse updateOtcConfig(@RequestHeader(name = "Authorization") String token,
			@RequestBody List<DefaultOtcRate> rates) {
		if (jwtTokenUtil.isAdminToken(token)) {
			return new CommonResponse(systemService.updateOtcConfig(rates));
		}

		throw RestOperationException.createInvalidTokenError();
	}
}
