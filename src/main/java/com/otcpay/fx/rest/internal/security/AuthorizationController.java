package com.otcpay.fx.rest.internal.security;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.otcpay.fx.exception.RestOperationException;
import com.otcpay.fx.model.common.CommonResponse;
import com.otcpay.fx.model.internal.Authorization.AuthorizationGroup;
import com.otcpay.fx.model.internal.Authorization.AuthroizationMapping;
import com.otcpay.fx.service.internal.AuthorizationService;
import com.otcpay.fx.util.secret.impl.JwtTokenUtil;

import springfox.documentation.annotations.ApiIgnore;

@RestController
@RequestMapping("/authorization")
@CrossOrigin(origins = "*", allowedHeaders = "*")
@ApiIgnore
public class AuthorizationController {
	@Autowired
	private AuthorizationService authorizationService;

	@Autowired
	private JwtTokenUtil jwtTokenUtil;

	@RequestMapping(value = "/all", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public Map<String, AuthroizationMapping> getAllAuthorizationMapping(
			@RequestHeader(name = "Authorization") String token) {
		if (!jwtTokenUtil.isTokenExpired(token)) {
			return authorizationService.getAllAuthorizationMapping();
		}

		throw RestOperationException.createInvalidTokenError();
	}

	@RequestMapping(value = "/groups/{group}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public AuthroizationMapping getAuthorizationMapping(
			@PathVariable(value = "group", required = true) AuthorizationGroup group,
			@RequestHeader(name = "Authorization") String token) {
		if (!jwtTokenUtil.isTokenExpired(token)) {
			return authorizationService.getAuthorizationMapping(group);
		}

		throw RestOperationException.createInvalidTokenError();
	}

	@RequestMapping(value = "/groups/{group}/user/{id}", method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_VALUE)
	public CommonResponse updateGroupAccess(@PathVariable(value = "group", required = true) AuthorizationGroup group,
			@PathVariable(value = "id", required = true) String id,
			@RequestHeader(name = "Authorization") String token) {
		if (jwtTokenUtil.isAdminToken(token)) {
			return new CommonResponse(authorizationService.updateGroupAccess(group, id));
		}

		throw RestOperationException.createInvalidTokenError();
	}
}
