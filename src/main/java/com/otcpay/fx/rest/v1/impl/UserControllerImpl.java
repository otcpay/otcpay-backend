package com.otcpay.fx.rest.v1.impl;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;

import com.otcpay.fx.annotation.rest.RestControllerV1;
import com.otcpay.fx.exception.RestOperationException;
import com.otcpay.fx.model.User;
import com.otcpay.fx.model.common.BatchResponse;
import com.otcpay.fx.model.common.CommonResponse;
import com.otcpay.fx.model.common.search.PaginatedResponse;
import com.otcpay.fx.model.secret.jwt.JwtResponse;
import com.otcpay.fx.rest.common.CommonController;
import com.otcpay.fx.rest.v1.UserController;
import com.otcpay.fx.service.UserService;
import com.otcpay.fx.util.ParamUtil;
import com.otcpay.fx.util.secret.impl.ApiKeyUtil;
import com.otcpay.fx.util.secret.impl.JwtTokenUtil;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import springfox.documentation.annotations.ApiIgnore;

@RestControllerV1
@Api(value = "/v1", tags = "Users in OCTPAY Fx System")
public class UserControllerImpl implements CommonController<User>, UserController {
	@Autowired
	private JwtTokenUtil jwtTokenUtil;

	@Autowired
	private ApiKeyUtil apiKeyUtil;

	@Autowired
	private UserService userService;

	@RequestMapping(value = "/users/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiOperation(httpMethod = "GET", value = "Get user by id", response = User.class, nickname = "getUserById", notes = "Get specific User by id")
	@ApiImplicitParam(name = "Authorization", value = "Jwt Token", dataType = "string", paramType = "header")
	@Override
	public User getData(@ApiParam(value = "id", required = true) @PathVariable(value = "id", required = true) String id,
			@RequestHeader(name = "Authorization") String token) {
		if (!jwtTokenUtil.isTokenExpired(token)) {
			User user = userService.readById(id);
			if (user != null) {
				return user;
			}

			throw RestOperationException.createResourceNotFoundError();
		}

		throw RestOperationException.createInvalidTokenError();
	}

	@RequestMapping(value = "/users", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiOperation(httpMethod = "GET", value = "Get all users", response = PaginatedResponse.class, nickname = "getUsers", notes = "Get all users available")
	@ApiImplicitParam(name = "Authorization", value = "Jwt Token", dataType = "string", paramType = "header")
	@Override
	public PaginatedResponse getPaginatedData(@RequestHeader(name = "Authorization") String token,
			@ApiParam(value = "refresh", defaultValue = "false") @RequestParam(value = "refresh", defaultValue = "false") boolean refresh,
			@ApiParam(value = "page", required = true, defaultValue = "1") @RequestParam(value = "page", required = true, defaultValue = "1") int page,
			@ApiParam(value = "size", required = true, defaultValue = "20") @RequestParam(value = "size", required = true, defaultValue = "20") int size,
			@ApiParam(value = "keywords") @RequestParam(value = "keywords", required = false) String keywords,
			@ApiParam(value = "fields", required = true) @RequestParam(value = "fields", required = true) String fields,
			@ApiParam(value = "orderBy") @RequestParam(value = "orderBy", required = false) String orderBy,
			@ApiParam(value = "direction") @RequestParam(value = "direction", required = false) String direction) {
		if (!ParamUtil.isValidCommaSeparated(fields)) {
			throw RestOperationException.createNotSeparatedByCommaError();
		}

		if (!ParamUtil.isAllFieldsValid(User.class.getDeclaredFields(), Arrays.asList(fields.split(",")))) {
			throw RestOperationException.createRequsetFieldNotExistError();
		}

		if (!jwtTokenUtil.isTokenExpired(token)) {
			List<Map<String, Object>> data = userService.readAll(page, size, fields, keywords, orderBy, direction);
			if (data != null) {
				return PaginatedResponse.constructResponse(page, size, keywords, refresh, data, userService);
			}

			throw RestOperationException.createResourceNotFoundError();
		}

		throw RestOperationException.createInvalidTokenError();
	}

	@RequestMapping(value = "/users/{id}", method = RequestMethod.PUT, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiOperation(httpMethod = "PUT", value = "Update user by id", response = CommonResponse.class, nickname = "updateUserById", notes = "Update specific User by id")
	@ApiImplicitParam(name = "Authorization", value = "Jwt Token", dataType = "string", paramType = "header")
	@Override
	public CommonResponse updateData(
			@ApiParam(value = "id", required = true) @PathVariable(value = "id", required = true) String id,
			@RequestBody Map<String, Object> user, @RequestHeader(name = "Authorization") String token) {
		if (user == null || user.keySet().size() == 0) {
			throw RestOperationException.createPayloadEmptyError();
		}

		if (!ParamUtil.isAllFieldsValid(User.class.getDeclaredFields(), user.keySet())) {
			throw RestOperationException.createRequsetFieldNotExistError();
		}

		if (ParamUtil.includeForbiddenFields(user.keySet())) {
			throw RestOperationException.createRequestCannotModifyError();
		}

		if (!jwtTokenUtil.isTokenExpired(token)) {
			return new CommonResponse(userService.update(id, user));
		}

		throw RestOperationException.createInvalidTokenError();
	}

	@RequestMapping(value = "/users/resetPassword", method = RequestMethod.PUT, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiOperation(httpMethod = "PUT", value = "Reset user password", response = CommonResponse.class, nickname = "resetUserPasswordById", notes = "Reset password of specific User by id")
	@ApiImplicitParam(name = "Authorization", value = "Jwt Token", dataType = "string", paramType = "header")
	@Override
	public CommonResponse resetPassword(@RequestBody Map<String, Object> payload,
			@RequestHeader(name = "Authorization") String token) {
		if (payload == null || payload.keySet().size() == 0) {
			throw RestOperationException.createPayloadEmptyError();
		}

		if (!ParamUtil.isAllFieldsValid(User.class.getDeclaredFields(), payload.keySet())) {
			throw RestOperationException.createRequsetFieldNotExistError();
		}

		if (apiKeyUtil.isTokenValid(token) || !jwtTokenUtil.isTokenExpired(token)) {
			payload.put(UserService.PASSWORD, userService.encryptPassword((String) payload.get(UserService.PASSWORD)));
			return new CommonResponse(userService.resetPassword(payload));
		}

		throw RestOperationException.createInvalidTokenError();
	}

	@RequestMapping(value = "/users/register", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiOperation(httpMethod = "POST", value = "Register User", response = JwtResponse.class, nickname = "registerUser", notes = "Register user as placeholder")
	@ApiImplicitParam(name = "Authorization", value = "Jwt Token or API Key", dataType = "string", paramType = "header")
	@Override
	public JwtResponse register(@RequestBody User user, @RequestHeader(name = "Authorization") String token) {
		if (apiKeyUtil.isTokenValid(token) || !jwtTokenUtil.isTokenExpired(token)) {
			user.setType(User.Type.CUSTOMER);
			JwtResponse response = userService.register(user);
			if (response != null) {
				return response;
			}

			throw RestOperationException.createError("Failed to register user");
		}

		throw RestOperationException.createInvalidTokenError();
	}

	@RequestMapping(value = "/user/account", method = RequestMethod.GET)
	@ApiOperation(httpMethod = "GET", value = "Check account", response = CommonResponse.class, nickname = "checkAccount", notes = "Check account exist or not")
	@ApiImplicitParam(name = "Authorization", value = "Jwt Token or API Key", dataType = "string", paramType = "header")
	@ResponseStatus(HttpStatus.OK)
	@Override
	public CommonResponse checkAccount(
			@ApiParam(value = "username", required = true) @RequestParam(value = "username", required = true) String username,
			@RequestHeader(name = "Authorization") String token) {
		if (apiKeyUtil.isTokenValid(token) || !jwtTokenUtil.isTokenExpired(token)) {
			User user = userService.findUser(username);
			return new CommonResponse(user != null);
		}

		throw RestOperationException.createInvalidTokenError();
	}

	@RequestMapping(value = "/users", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiOperation(httpMethod = "POST", value = "Create User", response = User.class, nickname = "createUser", notes = "Create user by payload")
	@ApiImplicitParam(name = "Authorization", value = "Jwt Token or API Key", dataType = "string", paramType = "header")
	@Override
	public User createData(@Valid @RequestBody User user, @RequestHeader(name = "Authorization") String token) {
		if (apiKeyUtil.isTokenValid(token) || !jwtTokenUtil.isTokenExpired(token)) {
			return userService.create(user);
		}

		throw RestOperationException.createInvalidTokenError();
	}

	@RequestMapping(value = "/users/{id}", method = RequestMethod.DELETE, produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiOperation(httpMethod = "DELETE", value = "Delete User by id", response = CommonResponse.class, nickname = "deleteUserById", notes = "Delete specific User by id")
	@ApiImplicitParam(name = "Authorization", value = "Jwt Token", dataType = "string", paramType = "header")
	@Override
	public CommonResponse deleteData(
			@ApiParam(value = "id", required = true) @PathVariable(value = "id", required = true) String id,
			@RequestHeader(name = "Authorization") String token) {
		if (!jwtTokenUtil.isTokenExpired(token)) {
			return new CommonResponse(userService.delete(id));
		}

		throw RestOperationException.createInvalidTokenError();
	}

	@RequestMapping(value = "/user", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiOperation(httpMethod = "GET", value = "Get user by id with specific fields", response = Map.class, nickname = "getUserByFields", notes = "Get specific user by id with specific fields")
	@ApiImplicitParam(name = "Authorization", value = "Jwt Token", dataType = "string", paramType = "header")
	@Override
	public Map<String, Object> getDataByFields(
			@ApiParam(value = "id", required = true) @RequestParam(value = "id", required = true) String id,
			@ApiParam(value = "fields", required = true) @RequestParam(value = "fields", required = true) String fields,
			@RequestHeader(name = "Authorization") String token) {
		if (!ParamUtil.isValidCommaSeparated(fields)) {
			throw RestOperationException.createNotSeparatedByCommaError();
		}

		if (!jwtTokenUtil.isTokenExpired(token)) {
			Map<String, Object> data = userService.readFields(id, fields.split(","));
			if (data != null) {
				return data;
			}

			throw RestOperationException.createResourceNotFoundError();
		}

		throw RestOperationException.createInvalidTokenError();
	}

	@RequestMapping(value = "/users", method = RequestMethod.PUT, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiOperation(httpMethod = "PUT", value = "update multiple user", response = CommonResponse.class, nickname = "updateMultipleUsers", notes = "Update multiple users by given ids")
	@ApiImplicitParam(name = "Authorization", value = "Jwt Token", dataType = "string", paramType = "header")
	@Override
	public CommonResponse updateMultipleData(
			@ApiParam(value = "ids") @RequestParam(value = "ids", required = true) String ids,
			@RequestBody Map<String, Object> data, @RequestHeader(name = "Authorization") String token) {
		if (data == null || data.keySet().size() == 0) {
			throw RestOperationException.createPayloadEmptyError();
		}

		if (!ParamUtil.isAllFieldsValid(User.class.getDeclaredFields(), data.keySet())) {
			throw RestOperationException.createRequsetFieldNotExistError();
		}

		if (ParamUtil.includeForbiddenFields(data.keySet())) {
			throw RestOperationException.createRequestCannotModifyError();
		}

		if (!jwtTokenUtil.isTokenExpired(token)) {
			List<String> errors = new ArrayList<String>();
			List<String> idList = Arrays.asList(ids.split(","));
			int length = idList.size();
			for (int i = 0; i < length; i++) {
				String id = idList.get(i);
				if (!userService.update(idList.get(i), data)) {
					errors.add(String.format("Failed to update [%s]", id));
				}
			}
			return BatchResponse.withErrors(errors);
		}

		throw RestOperationException.createInvalidTokenError();
	}

	@RequestMapping(value = "/users", method = RequestMethod.DELETE, produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiOperation(httpMethod = "DELETE", value = "delete multiple user", response = CommonResponse.class, nickname = "deleteMultipleUsers", notes = "Delete multiple users by given ids")
	@ApiImplicitParam(name = "Authorization", value = "Jwt Token", dataType = "string", paramType = "header")
	@Override
	public CommonResponse deleteMultipleData(
			@ApiParam(value = "ids") @RequestParam(value = "ids", required = true) String ids,
			@RequestHeader(name = "Authorization") String token) {
		if (!jwtTokenUtil.isTokenExpired(token)) {
			List<String> errors = new ArrayList<String>();
			List<String> idList = Arrays.asList(ids.split(","));
			int length = idList.size();
			for (int i = 0; i < length; i++) {
				String id = idList.get(i);
				if (!userService.delete(idList.get(i))) {
					errors.add(String.format("Failed to delete [%s]", id));
				}
			}
			return BatchResponse.withErrors(errors);
		}

		throw RestOperationException.createInvalidTokenError();
	}

	@ApiIgnore
	@RequestMapping(value = "/users/cms/{id}", method = RequestMethod.PUT, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiImplicitParam(name = "Authorization", value = "Jwt Token or API Key", dataType = "string", paramType = "header")
	@Override
	public CommonResponse cmsUpdate(@PathVariable(value = "id", required = true) String id,
			@RequestBody Map<String, Object> data, @RequestHeader(name = "Authorization") String token) {
		if (!jwtTokenUtil.isTokenExpired(token)) {
			return new CommonResponse(userService.update(id, data));
		}

		throw RestOperationException.createInvalidTokenError();
	}

}
