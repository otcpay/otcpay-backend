package com.otcpay.fx.rest.v1.impl;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.huobi.model.market.MarketDetail;
import com.otcpay.fx.annotation.rest.RestControllerV1;
import com.otcpay.fx.exception.RestOperationException;
import com.otcpay.fx.model.rate.DailyRate;
import com.otcpay.fx.model.rate.OtcRate;
import com.otcpay.fx.service.DailyRateService;
import com.otcpay.fx.service.external.HuobiService;
import com.otcpay.fx.util.secret.impl.ApiKeyUtil;
import com.otcpay.fx.util.secret.impl.JwtTokenUtil;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

@RestControllerV1
@Api(value = "/v1", tags = "DailyRate in OCTPAY Fx System")
public class DailyRateControllerImpl {
	@Autowired
	private JwtTokenUtil jwtTokenUtil;

	@Autowired
	private ApiKeyUtil apiKeyUtil;

	@Autowired
	private DailyRateService dailyRateService;

	@Autowired
	private HuobiService huobiService;

	@RequestMapping(value = "/fiat-rates", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiOperation(httpMethod = "GET", value = "Get latest daily FIAT rate", response = DailyRate.class, nickname = "getLatestFiatRate", notes = "Get latest daily rate by type and currency")
	@ApiImplicitParam(name = "Authorization", value = "Jwt Token", dataType = "string", paramType = "header")
	public DailyRate getLatestFiatRate(
			@ApiParam(value = "fromCurrency", required = true) @RequestParam(value = "fromCurrency", required = true) String fromCurrency,
			@ApiParam(value = "toCurrency", required = true) @RequestParam(value = "toCurrency", required = true) String toCurrency,
			@ApiParam(value = "type", required = true) @RequestParam(value = "type", required = true) DailyRate.Type type,
			@RequestHeader(name = "Authorization") String token) {
		if (!jwtTokenUtil.isTokenExpired(token)) {
			return dailyRateService.getLatestDailyRate(fromCurrency, toCurrency, type);
		}

		throw RestOperationException.createInvalidTokenError();
	}

	@RequestMapping(value = "/fiat-rates/default", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiOperation(httpMethod = "GET", value = "Get default latest daily FIAT rate", response = DailyRate.class, responseContainer = "List", nickname = "getDefaultLatestFiatRate", notes = "Get default latest daily rate by type and currency")
	@ApiImplicitParam(name = "Authorization", value = "Jwt Token", dataType = "string", paramType = "header")
	public List<DailyRate> getDefaultLatestFiatRate(
			@ApiParam(value = "showExternal", required = false) @RequestParam(value = "showExternal", required = false) boolean showExternal,
			@RequestHeader(name = "Authorization") String token) {
		if (apiKeyUtil.isTokenValid(token) || !jwtTokenUtil.isTokenExpired(token)) {
			return dailyRateService.getLatestDefaultDailyRate(showExternal);
		}

		throw RestOperationException.createInvalidTokenError();
	}

	@RequestMapping(value = "/otc-rates", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiOperation(httpMethod = "GET", value = "Get latest daily OTC rate", response = OtcRate.class, responseContainer = "List", nickname = "getLatestOtcRate", notes = "Get default latest daily OTC rate")
	@ApiImplicitParam(name = "Authorization", value = "Jwt Token", dataType = "string", paramType = "header")
	public List<OtcRate> getLatestOtcRate(@RequestHeader(name = "Authorization") String token) {
		if (apiKeyUtil.isTokenValid(token) || !jwtTokenUtil.isTokenExpired(token)) {
			return dailyRateService.getLatestOtcRate();
		}

		throw RestOperationException.createInvalidTokenError();
	}

	@RequestMapping(value = "/crypto-rates/default", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiOperation(httpMethod = "GET", value = "Get default latest daily Crypto rate", response = MarketDetail.class, responseContainer = "List", nickname = "getDefaultLatestCryptoRate", notes = "Get default latest daily rate of cryptocurrency")
	@ApiImplicitParam(name = "Authorization", value = "Jwt Token", dataType = "string", paramType = "header")
	public List<MarketDetail> getDefaultLatestCryptoRate(@RequestHeader(name = "Authorization") String token) {
		if (apiKeyUtil.isTokenValid(token) || !jwtTokenUtil.isTokenExpired(token)) {
			List<String> cryptos = dailyRateService.getLatestDefaultCrypto();
			return cryptos.parallelStream().map((c) -> huobiService.getRate(c, HuobiService.DEFAULT_COIN))
					.collect(Collectors.toList());
		}

		throw RestOperationException.createInvalidTokenError();
	}

	@RequestMapping(value = "/crypto-rates", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiOperation(httpMethod = "GET", value = "Get latest daily Crypto rate", response = MarketDetail.class, nickname = "getLatestCryptoRate", notes = "Get latest daily rate of cryptocurrency")
	@ApiImplicitParam(name = "Authorization", value = "Jwt Token", dataType = "string", paramType = "header")
	public MarketDetail getLatestCryptoRate(
			@ApiParam(value = "fromCurrency", required = true) @RequestParam(value = "fromCurrency", required = true) String fromCurrency,
			@ApiParam(value = "toCurrency", required = true) @RequestParam(value = "toCurrency", required = true) String toCurrency,
			@RequestHeader(name = "Authorization") String token) {
		if (!jwtTokenUtil.isTokenExpired(token)) {
			return huobiService.getRate(fromCurrency, toCurrency);
		}

		throw RestOperationException.createInvalidTokenError();
	}

}
