package com.otcpay.fx.rest.v1.impl;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.otcpay.fx.annotation.rest.RestControllerV1;
import com.otcpay.fx.exception.RestOperationException;
import com.otcpay.fx.model.InvoicePayment;
import com.otcpay.fx.model.common.BatchResponse;
import com.otcpay.fx.model.common.CommonResponse;
import com.otcpay.fx.model.common.search.PaginatedResponse;
import com.otcpay.fx.rest.common.CommonController;
import com.otcpay.fx.rest.v1.InvoicePaymentController;
import com.otcpay.fx.service.InvoicePaymentService;
import com.otcpay.fx.util.ParamUtil;
import com.otcpay.fx.util.secret.impl.ApiKeyUtil;
import com.otcpay.fx.util.secret.impl.JwtTokenUtil;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import springfox.documentation.annotations.ApiIgnore;

@RestControllerV1
@Api(value = "/v1", tags = "InvoicePayments in OCTPAY Fx System")
public class InvoicePaymentControllerImpl implements CommonController<InvoicePayment>, InvoicePaymentController {
	@Autowired
	private JwtTokenUtil jwtTokenUtil;

	@Autowired
	private ApiKeyUtil apiKeyUtil;

	@Autowired
	private InvoicePaymentService invoicePaymentService;

	@RequestMapping(value = "/invoicePayments/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiOperation(httpMethod = "GET", value = "Get invoicePayment by id", response = InvoicePayment.class, nickname = "getInvoicePaymentById", notes = "Get specific InvoicePayment by id")
	@ApiImplicitParam(name = "Authorization", value = "Jwt Token", dataType = "string", paramType = "header")
	@Override
	public InvoicePayment getData(
			@ApiParam(value = "id", required = true) @PathVariable(value = "id", required = true) String id,
			@RequestHeader(name = "Authorization") String token) {
		if (!jwtTokenUtil.isTokenExpired(token)) {
			InvoicePayment invoicePayment = invoicePaymentService.readById(id);
			if (invoicePayment != null) {
				return invoicePayment;
			}

			throw RestOperationException.createResourceNotFoundError();
		}

		throw RestOperationException.createInvalidTokenError();
	}

	@RequestMapping(value = "/invoicePayments", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiOperation(httpMethod = "GET", value = "Get all invoicePayments", response = PaginatedResponse.class, nickname = "getInvoicePayments", notes = "Get all invoicePayments available")
	@ApiImplicitParam(name = "Authorization", value = "Jwt Token", dataType = "string", paramType = "header")
	@Override
	public PaginatedResponse getPaginatedData(@RequestHeader(name = "Authorization") String token,
			@ApiParam(value = "refresh", defaultValue = "false") @RequestParam(value = "refresh", defaultValue = "false") boolean refresh,
			@ApiParam(value = "page", required = true, defaultValue = "1") @RequestParam(value = "page", required = true, defaultValue = "1") int page,
			@ApiParam(value = "size", required = true, defaultValue = "20") @RequestParam(value = "size", required = true, defaultValue = "20") int size,
			@ApiParam(value = "keywords") @RequestParam(value = "keywords", required = false) String keywords,
			@ApiParam(value = "fields", required = true) @RequestParam(value = "fields", required = true) String fields,
			@ApiParam(value = "orderBy") @RequestParam(value = "orderBy", required = false) String orderBy,
			@ApiParam(value = "direction") @RequestParam(value = "direction", required = false) String direction) {
		if (!ParamUtil.isValidCommaSeparated(fields)) {
			throw RestOperationException.createNotSeparatedByCommaError();
		}

		if (!ParamUtil.isAllFieldsValid(InvoicePayment.class.getDeclaredFields(), Arrays.asList(fields.split(",")))) {
			throw RestOperationException.createRequsetFieldNotExistError();
		}

		if (!jwtTokenUtil.isTokenExpired(token)) {
			List<Map<String, Object>> data = invoicePaymentService.readAll(page, size, fields, keywords, orderBy,
					direction);
			if (data != null) {
				return PaginatedResponse.constructResponse(page, size, keywords, refresh, data, invoicePaymentService);
			}

			throw RestOperationException.createResourceNotFoundError();
		}

		throw RestOperationException.createInvalidTokenError();
	}

	@RequestMapping(value = "/invoicePayments/{id}", method = RequestMethod.PUT, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiOperation(httpMethod = "PUT", value = "Update invoicePayment by id", response = CommonResponse.class, nickname = "updateInvoicePaymentById", notes = "Update specific InvoicePayment by id")
	@ApiImplicitParam(name = "Authorization", value = "Jwt Token", dataType = "string", paramType = "header")
	@Override
	public CommonResponse updateData(
			@ApiParam(value = "id", required = true) @PathVariable(value = "id", required = true) String id,
			@RequestBody Map<String, Object> invoicePayment, @RequestHeader(name = "Authorization") String token) {
		if (invoicePayment == null || invoicePayment.keySet().size() == 0) {
			throw RestOperationException.createPayloadEmptyError();
		}

		if (!ParamUtil.isAllFieldsValid(InvoicePayment.class.getDeclaredFields(), invoicePayment.keySet())) {
			throw RestOperationException.createRequsetFieldNotExistError();
		}

		if (ParamUtil.includeForbiddenFields(invoicePayment.keySet())) {
			throw RestOperationException.createRequestCannotModifyError();
		}

		if (!jwtTokenUtil.isTokenExpired(token)) {
			return new CommonResponse(invoicePaymentService.update(id, invoicePayment));
		}

		throw RestOperationException.createInvalidTokenError();
	}

	@RequestMapping(value = "/invoicePayments", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiOperation(httpMethod = "POST", value = "Create InvoicePayment", response = InvoicePayment.class, nickname = "createInvoicePayment", notes = "Create invoicePayment by payload")
	@ApiImplicitParam(name = "Authorization", value = "Jwt Token or API Key", dataType = "string", paramType = "header")
	@Override
	public InvoicePayment createData(@Valid @RequestBody InvoicePayment invoicePayment,
			@RequestHeader(name = "Authorization") String token) {
		if (!jwtTokenUtil.isTokenExpired(token)) {
			return invoicePaymentService.create(invoicePayment);
		}

		throw RestOperationException.createInvalidTokenError();
	}

	@RequestMapping(value = "/invoicePayments/{id}", method = RequestMethod.DELETE, produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiOperation(httpMethod = "DELETE", value = "Delete InvoicePayment by id", response = CommonResponse.class, nickname = "deleteInvoicePaymentById", notes = "Delete specific InvoicePayment by id")
	@ApiImplicitParam(name = "Authorization", value = "Jwt Token", dataType = "string", paramType = "header")
	@Override
	public CommonResponse deleteData(
			@ApiParam(value = "id", required = true) @PathVariable(value = "id", required = true) String id,
			@RequestHeader(name = "Authorization") String token) {
		if (!jwtTokenUtil.isTokenExpired(token)) {
			return new CommonResponse(invoicePaymentService.delete(id));
		}

		throw RestOperationException.createInvalidTokenError();
	}

	@RequestMapping(value = "/invoicePayment", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiOperation(httpMethod = "GET", value = "Get invoicePayment by id with specific fields", response = Map.class, nickname = "getInvoicePaymentByFields", notes = "Get specific invoicePayment by id with specific fields")
	@ApiImplicitParam(name = "Authorization", value = "Jwt Token", dataType = "string", paramType = "header")
	@Override
	public Map<String, Object> getDataByFields(
			@ApiParam(value = "id", required = true) @RequestParam(value = "id", required = true) String id,
			@ApiParam(value = "fields", required = true) @RequestParam(value = "fields", required = true) String fields,
			@RequestHeader(name = "Authorization") String token) {
		if (!ParamUtil.isValidCommaSeparated(fields)) {
			throw RestOperationException.createNotSeparatedByCommaError();
		}

		if (!jwtTokenUtil.isTokenExpired(token)) {
			Map<String, Object> data = invoicePaymentService.readFields(id, fields.split(","));
			if (data != null) {
				return data;
			}

			throw RestOperationException.createResourceNotFoundError();
		}

		throw RestOperationException.createInvalidTokenError();
	}

	@RequestMapping(value = "/invoicePayments", method = RequestMethod.PUT, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiOperation(httpMethod = "PUT", value = "update multiple invoicePayment", response = CommonResponse.class, nickname = "updateMultipleInvoicePayments", notes = "Update multiple invoicePayments by given ids")
	@ApiImplicitParam(name = "Authorization", value = "Jwt Token", dataType = "string", paramType = "header")
	@Override
	public CommonResponse updateMultipleData(
			@ApiParam(value = "ids") @RequestParam(value = "ids", required = true) String ids,
			@RequestBody Map<String, Object> data, @RequestHeader(name = "Authorization") String token) {
		if (data == null || data.keySet().size() == 0) {
			throw RestOperationException.createPayloadEmptyError();
		}

		if (!ParamUtil.isAllFieldsValid(InvoicePayment.class.getDeclaredFields(), data.keySet())) {
			throw RestOperationException.createRequsetFieldNotExistError();
		}

		if (ParamUtil.includeForbiddenFields(data.keySet())) {
			throw RestOperationException.createRequestCannotModifyError();
		}

		if (!jwtTokenUtil.isTokenExpired(token)) {
			List<String> errors = new ArrayList<String>();
			List<String> idList = Arrays.asList(ids.split(","));
			int length = idList.size();
			for (int i = 0; i < length; i++) {
				String id = idList.get(i);
				if (!invoicePaymentService.update(idList.get(i), data)) {
					errors.add(String.format("Failed to update [%s]", id));
				}
			}
			return BatchResponse.withErrors(errors);
		}

		throw RestOperationException.createInvalidTokenError();
	}

	@RequestMapping(value = "/invoicePayments", method = RequestMethod.DELETE, produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiOperation(httpMethod = "DELETE", value = "delete multiple invoicePayment", response = CommonResponse.class, nickname = "deleteMultipleInvoicePayments", notes = "Delete multiple invoicePayments by given ids")
	@ApiImplicitParam(name = "Authorization", value = "Jwt Token", dataType = "string", paramType = "header")
	@Override
	public CommonResponse deleteMultipleData(
			@ApiParam(value = "ids") @RequestParam(value = "ids", required = true) String ids,
			@RequestHeader(name = "Authorization") String token) {
		if (!jwtTokenUtil.isTokenExpired(token)) {
			List<String> errors = new ArrayList<String>();
			List<String> idList = Arrays.asList(ids.split(","));
			int length = idList.size();
			for (int i = 0; i < length; i++) {
				String id = idList.get(i);
				if (!invoicePaymentService.delete(idList.get(i))) {
					errors.add(String.format("Failed to delete [%s]", id));
				}
			}
			return BatchResponse.withErrors(errors);
		}

		throw RestOperationException.createInvalidTokenError();
	}

	@RequestMapping(value = "/latest/invoicePayments", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiOperation(httpMethod = "GET", value = "Get latest invoicePayments", response = Map.class, responseContainer = "List", nickname = "getLatestInvoicePayments", notes = "Get latest invoicePayments available")
	@ApiImplicitParam(name = "Authorization", value = "Jwt Token", dataType = "string", paramType = "header")
	@Override
	public List<Map<String, Object>> getLatestAd(@RequestHeader(name = "Authorization") String token) {
		if (apiKeyUtil.isTokenValid(token) || !jwtTokenUtil.isTokenExpired(token)) {
			return invoicePaymentService.getLatest();
		}

		throw RestOperationException.createInvalidTokenError();
	}

	@ApiIgnore
	@RequestMapping(value = "/invoicePayments/cms/{id}", method = RequestMethod.PUT, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiImplicitParam(name = "Authorization", value = "Jwt Token or API Key", dataType = "string", paramType = "header")
	@Override
	public CommonResponse cmsUpdate(@PathVariable(value = "id", required = true) String id,
			@RequestBody Map<String, Object> data, @RequestHeader(name = "Authorization") String token) {
		if (!jwtTokenUtil.isTokenExpired(token)) {
			return new CommonResponse(invoicePaymentService.update(id, data));
		}

		throw RestOperationException.createInvalidTokenError();
	}

}
