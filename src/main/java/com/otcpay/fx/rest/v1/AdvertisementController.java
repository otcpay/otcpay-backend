package com.otcpay.fx.rest.v1;

import java.util.List;
import java.util.Map;

/**
 * Specific controller used by Advertisement REST Controller
 * 
 * @author Anthony
 *
 */
public interface AdvertisementController {
	/**
	 * Get latest Advertisement from FireStore for OTC Page
	 * 
	 * @param token  JWT token
	 * @param cursor (last updated of document) of the query, used for pagination
	 * @return latest 20 documents from FireStore order by last updated date, order
	 *         next 20 documents starting from the cursor ordering using last
	 *         updated date
	 */
	public List<Map<String, Object>> getLatestAd(String token, String cursor);
}
