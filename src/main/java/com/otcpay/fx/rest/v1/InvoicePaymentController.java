package com.otcpay.fx.rest.v1;

import java.util.List;
import java.util.Map;

/**
 * Specific controller used by Invoice Payment REST Controller
 * 
 * @author Anthony
 *
 */
public interface InvoicePaymentController {
	/**
	 * Get latest Invoice Payment from FireStore for Hot Page, Quotation section
	 * 
	 * @param token JWT token
	 * @return latest 20 documents from FireStore order by last updated date
	 */
	public List<Map<String, Object>> getLatestAd(String token);
}
