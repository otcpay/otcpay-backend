package com.otcpay.fx.rest.v1;

import java.util.Map;

import com.otcpay.fx.model.User;
import com.otcpay.fx.model.common.CommonResponse;
import com.otcpay.fx.model.secret.jwt.JwtResponse;

/**
 * Specific controller used by User REST Controller
 * 
 * @author Anthony
 *
 */
public interface UserController {
	/**
	 * Reset password for user
	 * 
	 * @param payload identity and password in key-value pair
	 * @param token   JWT token
	 * @return result of the operation. If error(s) occurred, error message will be
	 *         included
	 */
	public CommonResponse resetPassword(Map<String, Object> payload, String token);

	/**
	 * Register user
	 * 
	 * @param user  {@link User object}
	 * @param token JWT token
	 * @return {@link JwtResponse}
	 */
	public JwtResponse register(User user, String token);

	/**
	 * Check if account has already being registered before
	 * 
	 * @param username account of the user
	 * @param token    JWT token
	 * @return result of the operation. If error(s) occurred, error message will be
	 *         included
	 */
	public CommonResponse checkAccount(String username, String token);
}
