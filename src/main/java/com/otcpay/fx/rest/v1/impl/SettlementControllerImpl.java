package com.otcpay.fx.rest.v1.impl;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.otcpay.fx.annotation.rest.RestControllerV1;
import com.otcpay.fx.exception.RestOperationException;
import com.otcpay.fx.model.Settlement;
import com.otcpay.fx.model.common.BatchResponse;
import com.otcpay.fx.model.common.CommonResponse;
import com.otcpay.fx.model.common.search.PaginatedResponse;
import com.otcpay.fx.rest.common.CommonController;
import com.otcpay.fx.service.SettlementService;
import com.otcpay.fx.util.ParamUtil;
import com.otcpay.fx.util.secret.impl.JwtTokenUtil;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import springfox.documentation.annotations.ApiIgnore;

@RestControllerV1
@Api(value = "/v1", tags = "Settlements in OCTPAY Fx System")
public class SettlementControllerImpl implements CommonController<Settlement> {
	@Autowired
	private JwtTokenUtil jwtTokenUtil;

	@Autowired
	private SettlementService settlementService;

	@RequestMapping(value = "/settlements/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiOperation(httpMethod = "GET", value = "Get settlement by id", response = Settlement.class, nickname = "getSettlementById", notes = "Get specific Settlement by id")
	@ApiImplicitParam(name = "Authorization", value = "Jwt Token", dataType = "string", paramType = "header")
	@Override
	public Settlement getData(
			@ApiParam(value = "id", required = true) @PathVariable(value = "id", required = true) String id,
			@RequestHeader(name = "Authorization") String token) {
		if (!jwtTokenUtil.isTokenExpired(token)) {
			Settlement settlement = settlementService.readById(id);
			if (settlement != null) {
				return settlement;
			}

			throw RestOperationException.createResourceNotFoundError();
		}

		throw RestOperationException.createInvalidTokenError();
	}

	@RequestMapping(value = "/settlements", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiOperation(httpMethod = "GET", value = "Get all settlements", response = PaginatedResponse.class, nickname = "getSettlements", notes = "Get all settlements available")
	@ApiImplicitParam(name = "Authorization", value = "Jwt Token", dataType = "string", paramType = "header")
	@Override
	public PaginatedResponse getPaginatedData(@RequestHeader(name = "Authorization") String token,
			@ApiParam(value = "refresh", defaultValue = "false") @RequestParam(value = "refresh", defaultValue = "false") boolean refresh,
			@ApiParam(value = "page", required = true, defaultValue = "1") @RequestParam(value = "page", required = true, defaultValue = "1") int page,
			@ApiParam(value = "size", required = true, defaultValue = "20") @RequestParam(value = "size", required = true, defaultValue = "20") int size,
			@ApiParam(value = "keywords") @RequestParam(value = "keywords", required = false) String keywords,
			@ApiParam(value = "fields", required = true) @RequestParam(value = "fields", required = true) String fields,
			@ApiParam(value = "orderBy") @RequestParam(value = "orderBy", required = false) String orderBy,
			@ApiParam(value = "direction") @RequestParam(value = "direction", required = false) String direction) {
		if (!ParamUtil.isValidCommaSeparated(fields)) {
			throw RestOperationException.createNotSeparatedByCommaError();
		}

		if (!ParamUtil.isAllFieldsValid(Settlement.class.getDeclaredFields(), Arrays.asList(fields.split(",")))) {
			throw RestOperationException.createRequsetFieldNotExistError();
		}

		if (!jwtTokenUtil.isTokenExpired(token)) {
			List<Map<String, Object>> data = settlementService.readAll(page, size, fields, keywords, orderBy,
					direction);
			if (data != null) {
				return PaginatedResponse.constructResponse(page, size, keywords, refresh, data, settlementService);
			}

			throw RestOperationException.createResourceNotFoundError();
		}

		throw RestOperationException.createInvalidTokenError();
	}

	@RequestMapping(value = "/settlements/{id}", method = RequestMethod.PUT, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiOperation(httpMethod = "PUT", value = "Update settlement by id", response = CommonResponse.class, nickname = "updateSettlementById", notes = "Update specific Settlement by id")
	@ApiImplicitParam(name = "Authorization", value = "Jwt Token", dataType = "string", paramType = "header")
	@Override
	public CommonResponse updateData(
			@ApiParam(value = "id", required = true) @PathVariable(value = "id", required = true) String id,
			@RequestBody Map<String, Object> settlement, @RequestHeader(name = "Authorization") String token) {
		if (settlement == null || settlement.keySet().size() == 0) {
			throw RestOperationException.createPayloadEmptyError();
		}

		if (!ParamUtil.isAllFieldsValid(Settlement.class.getDeclaredFields(), settlement.keySet())) {
			throw RestOperationException.createRequsetFieldNotExistError();
		}

		if (ParamUtil.includeForbiddenFields(settlement.keySet())) {
			throw RestOperationException.createRequestCannotModifyError();
		}

		if (!jwtTokenUtil.isTokenExpired(token)) {
			return new CommonResponse(settlementService.update(id, settlement));
		}

		throw RestOperationException.createInvalidTokenError();
	}

	@RequestMapping(value = "/settlements", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiOperation(httpMethod = "POST", value = "Create Settlement", response = Settlement.class, nickname = "createSettlement", notes = "Create settlement by payload")
	@ApiImplicitParam(name = "Authorization", value = "Jwt Token or API Key", dataType = "string", paramType = "header")
	@Override
	public Settlement createData(@Valid @RequestBody Settlement settlement,
			@RequestHeader(name = "Authorization") String token) {
		if (!jwtTokenUtil.isTokenExpired(token)) {
			return settlementService.create(settlement);
		}

		throw RestOperationException.createInvalidTokenError();
	}

	@RequestMapping(value = "/settlements/{id}", method = RequestMethod.DELETE, produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiOperation(httpMethod = "DELETE", value = "Delete Settlement by id", response = CommonResponse.class, nickname = "deleteSettlementById", notes = "Delete specific Settlement by id")
	@ApiImplicitParam(name = "Authorization", value = "Jwt Token", dataType = "string", paramType = "header")
	@Override
	public CommonResponse deleteData(
			@ApiParam(value = "id", required = true) @PathVariable(value = "id", required = true) String id,
			@RequestHeader(name = "Authorization") String token) {
		if (!jwtTokenUtil.isTokenExpired(token)) {
			return new CommonResponse(settlementService.delete(id));
		}

		throw RestOperationException.createInvalidTokenError();
	}

	@RequestMapping(value = "/settlement", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiOperation(httpMethod = "GET", value = "Get settlement by id with specific fields", response = Map.class, nickname = "getSettlementByFields", notes = "Get specific settlement by id with specific fields")
	@ApiImplicitParam(name = "Authorization", value = "Jwt Token", dataType = "string", paramType = "header")
	@Override
	public Map<String, Object> getDataByFields(
			@ApiParam(value = "id", required = true) @RequestParam(value = "id", required = true) String id,
			@ApiParam(value = "fields", required = true) @RequestParam(value = "fields", required = true) String fields,
			@RequestHeader(name = "Authorization") String token) {
		if (!ParamUtil.isValidCommaSeparated(fields)) {
			throw RestOperationException.createNotSeparatedByCommaError();
		}

		if (!jwtTokenUtil.isTokenExpired(token)) {
			Map<String, Object> data = settlementService.readFields(id, fields.split(","));
			if (data != null) {
				return data;
			}

			throw RestOperationException.createResourceNotFoundError();
		}

		throw RestOperationException.createInvalidTokenError();
	}

	@RequestMapping(value = "/settlements", method = RequestMethod.PUT, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiOperation(httpMethod = "PUT", value = "update multiple settlement", response = CommonResponse.class, nickname = "updateMultipleSettlements", notes = "Update multiple settlements by given ids")
	@ApiImplicitParam(name = "Authorization", value = "Jwt Token", dataType = "string", paramType = "header")
	@Override
	public CommonResponse updateMultipleData(
			@ApiParam(value = "ids") @RequestParam(value = "ids", required = true) String ids,
			@RequestBody Map<String, Object> data, @RequestHeader(name = "Authorization") String token) {
		if (data == null || data.keySet().size() == 0) {
			throw RestOperationException.createPayloadEmptyError();
		}

		if (!ParamUtil.isAllFieldsValid(Settlement.class.getDeclaredFields(), data.keySet())) {
			throw RestOperationException.createRequsetFieldNotExistError();
		}

		if (ParamUtil.includeForbiddenFields(data.keySet())) {
			throw RestOperationException.createRequestCannotModifyError();
		}

		if (!jwtTokenUtil.isTokenExpired(token)) {
			List<String> errors = new ArrayList<String>();
			List<String> idList = Arrays.asList(ids.split(","));
			int length = idList.size();
			for (int i = 0; i < length; i++) {
				String id = idList.get(i);
				if (!settlementService.update(idList.get(i), data)) {
					errors.add(String.format("Failed to update [%s]", id));
				}
			}
			return BatchResponse.withErrors(errors);
		}

		throw RestOperationException.createInvalidTokenError();
	}

	@RequestMapping(value = "/settlements", method = RequestMethod.DELETE, produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiOperation(httpMethod = "DELETE", value = "delete multiple settlement", response = CommonResponse.class, nickname = "deleteMultipleSettlements", notes = "Delete multiple settlements by given ids")
	@ApiImplicitParam(name = "Authorization", value = "Jwt Token", dataType = "string", paramType = "header")
	@Override
	public CommonResponse deleteMultipleData(
			@ApiParam(value = "ids") @RequestParam(value = "ids", required = true) String ids,
			@RequestHeader(name = "Authorization") String token) {
		if (!jwtTokenUtil.isTokenExpired(token)) {
			List<String> errors = new ArrayList<String>();
			List<String> idList = Arrays.asList(ids.split(","));
			int length = idList.size();
			for (int i = 0; i < length; i++) {
				String id = idList.get(i);
				if (!settlementService.delete(idList.get(i))) {
					errors.add(String.format("Failed to delete [%s]", id));
				}
			}
			return BatchResponse.withErrors(errors);
		}

		throw RestOperationException.createInvalidTokenError();
	}

	@ApiIgnore
	@RequestMapping(value = "/settlements/cms/{id}", method = RequestMethod.PUT, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiImplicitParam(name = "Authorization", value = "Jwt Token or API Key", dataType = "string", paramType = "header")
	@Override
	public CommonResponse cmsUpdate(@PathVariable(value = "id", required = true) String id,
			@RequestBody Map<String, Object> data, @RequestHeader(name = "Authorization") String token) {
		if (!jwtTokenUtil.isTokenExpired(token)) {
			return new CommonResponse(settlementService.update(id, data));
		}

		throw RestOperationException.createInvalidTokenError();
	}
}
