package com.otcpay.fx.rest.common;

import java.util.Map;

import com.otcpay.fx.model.common.CommonResponse;
import com.otcpay.fx.model.common.search.PaginatedResponse;

/**
 * 
 * Common controller used for resource stored in FireStore
 * 
 * @author Anthony
 *
 * @param <T>
 */
public interface CommonController<T> {
	/**
	 * Get document from FireStore by document Id and convert to required <T< type
	 * 
	 * @param id    document Id in FireStore
	 * @param token JWT token
	 * @return Object in <T> type
	 */
	public T getData(String id, String token);

	/**
	 * Update document in FireStore by document Id. If key(s) found in document,
	 * existing value will be overwrote; If key(s) not found in document, it will be
	 * added to the document
	 * 
	 * @param id    document Id in FireStore
	 * @param data  key-value pair to update
	 * @param token JWT token
	 * @return {@link CommonResponse} result of the operation. If error(s) occurred,
	 *         error message will be included
	 */
	public CommonResponse updateData(String id, Map<String, Object> data, String token);

	/**
	 * Save new document to FireStore
	 * 
	 * @param data  Object to be saved
	 * @param token JWT token
	 * @return Object in <T> type
	 */
	public T createData(T data, String token);

	/**
	 * Delete document by document Id in FireStore
	 * 
	 * @param id    document Id in FireStore
	 * @param token JWT token
	 * @return result of the operation. If error(s) occurred, error message will be
	 *         included
	 */
	public CommonResponse deleteData(String id, String token);

	/**
	 * Get document with specific fields by document Id in FireStore, this could be
	 * particular useful to save bandwidth for faster response
	 * 
	 * @param id     document Id in FireStore
	 * @param fields field(s) in the document
	 * @param token  JWT token
	 * @return Data in key-value pair(s)
	 */
	public Map<String, Object> getDataByFields(String id, String fields, String token);

	/**
	 * Get document(s) with specific filtering, sorting options by keywords matching
	 * from FireStore
	 * 
	 * @param token     JWT token
	 * @param refresh   flag to indicate if number of document(s) need to refreshed
	 *                  for latest snapshot or cached number of document(s) is
	 *                  acceptable
	 * @param page      page of data
	 * @param size      number of documents to be returned
	 * @param keywords  keyword used in query
	 * @param fields    field(s) interested in the query, separated in comma
	 * @param orderBy   field to order
	 * @param direction ordering direction, either asc or desc
	 * @return {@link PaginatedResponse}
	 */
	public PaginatedResponse getPaginatedData(String token, boolean refresh, int page, int size, String keywords,
			String fields, String orderBy, String direction);

	/**
	 * Update multiple documents with document Id(s) in FireStore
	 * 
	 * @param ids   document Id(s) separated in comma
	 * @param data  key-value pair to update
	 * @param token JWT token
	 * @return result of the operation. If error(s) occurred, error message will be
	 *         included
	 */
	public CommonResponse updateMultipleData(String ids, Map<String, Object> data, String token);

	/**
	 * Delete multiple documents with document Id(s) in FireStore
	 * 
	 * @param ids   document Id(s) separated in comma
	 * @param data  key-value pair to update
	 * @param token JWT token
	 * @return result of the operation. If error(s) occurred, error message will be
	 *         included
	 */
	public CommonResponse deleteMultipleData(String ids, String token);

	/**
	 * Update document by document Id in FireStore, only used by CMS
	 * 
	 * @param id    document Id(s) separated in comma
	 * @param data  key-value pair to update
	 * @param token JWT token
	 * @return result of the operation. If error(s) occurred, error message will be
	 *         included
	 */
	public CommonResponse cmsUpdate(String id, Map<String, Object> data, String token);
}
