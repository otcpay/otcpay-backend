package com.otcpay.fx.util;

import java.io.UnsupportedEncodingException;
import java.lang.reflect.Field;
import java.net.URLDecoder;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import org.springframework.http.HttpStatus;

import com.otcpay.fx.exception.RestOperationException;

public class ParamUtil {
	private static final List<String> FORBIDDEN_FIELDS = Arrays.asList("id", "createdDate", "updatedDate", "userId",
			"categoryId", "itemId", "pawnId", "password");

	public static boolean isEitherParamValid(String... params) {
		for (String param : params) {
			if (param != null && !param.isEmpty()) {
				return true;
			}
		}

		return false;
	}

	public static boolean isAllParamValid(String... params) {
		if (params.length == 0)
			return false;

		boolean isValid = true;
		for (String param : params) {
			if (param == null || param.isEmpty()) {
				isValid = false;
				break;
			}
		}
		return isValid;
	}

	public static boolean isValidCommaSeparated(String param) {
		String pattern = "^(?:[a-zA-Z0-9]+(?:,[a-zA-Z0-9]+)*)?$";
		Pattern r = Pattern.compile(pattern);
		Matcher m = r.matcher(param);
		return m.find();
	}

	public static boolean isAllDigit(String mobile) {
		String regex = "[0-9]+";
		return mobile.matches(regex);
	}

	public static boolean isAllFieldsValid(Field[] fields, Collection<String> keys) {
		List<String> fieldList = Arrays.asList(fields).stream().map(field -> {
			return field.getName();
		}).collect(Collectors.toList());
		for (String key : keys) {
			if (!fieldList.contains(key)) {
				return false;
			}
		}

		return true;
	}

	public static boolean includeForbiddenFields(Set<String> keys) {
		for (String key : keys) {
			if (FORBIDDEN_FIELDS.contains(key)) {
				return true;
			}
		}

		return false;
	}

	public static String decode(String encodeParam) {
		try {
			return URLDecoder.decode(encodeParam, "UTF-8");
		} catch (UnsupportedEncodingException e) {
			throw RestOperationException.createError("Failed to decode param", HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

}
