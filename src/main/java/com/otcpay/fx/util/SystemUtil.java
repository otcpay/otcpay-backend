package com.otcpay.fx.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.otcpay.fx.exception.RestOperationException;

public class SystemUtil {
	private static final Logger LOG = LoggerFactory.getLogger(SystemUtil.class);

	public static final String ENABLE_TOKEN = "ENABLE_TOKEN";
	public static final String ENABLE_API_KEY = "ENABLE_API_KEY";
	public static final String API_KEY = "API_KEY";
	public static final String FIREBASE_CREDENTIAL = "FIREBASE_CREDENTIAL";
	public static final String STORAGE_URL = "STORAGE_URL";
	public static final String FIREBASE_URL = "FIREBASE_URL";
	public static final String HUOBI_ACCESS_KEY = "HUOBI_ACCESS_KEY";
	public static final String HUOBI_SECRET_KEY = "HUOBI_SECRET_KEY";
	public static final String CACHE_EXPIRE_DURATION = "CACHE_EXPIRE_DURATION";
	public static final String CACHE_MAXINUM_SIZE = "CACHE_MAXINUM_SIZE";
	public static final String CMS_ENDPOINT = "CMS_ENDPOINT";

	public static String getEnv(String key) {
		if (key == null)
			throw RestOperationException.createError("ENV Key cannot be null", HttpStatus.INTERNAL_SERVER_ERROR);

		String value = System.getenv(key);
		if (value == null)
			return "";

		return value;
	}

	public static boolean getEnvAsBool(String key) {
		String value = getEnv(key);
		return Boolean.parseBoolean(value);
	}

	public static int getEnvAsInt(String key) {
		String value = getEnv(key);
		try {
			return Integer.parseInt(value);
		} catch (NumberFormatException e) {
			LOG.info("Failed to convert Env Var of key [%s] into integer", key, e);
		}

		return 1;
	}

	public static <T> T getEnv(String key, Class<T> cls) {
		String value = getEnv(key);
		ObjectMapper mapper = new ObjectMapper();
		try {
			return mapper.readValue(value, cls);
		} catch (JsonProcessingException e) {
			throw RestOperationException.createError(String
					.format("Failed to convert Env Var of key [%s] into object of type - [%s]", key, cls.getName()),
					HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
}
