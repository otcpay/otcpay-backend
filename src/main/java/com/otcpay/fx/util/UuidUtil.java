package com.otcpay.fx.util;

import java.util.UUID;

public class UuidUtil {
	public static String generate() {
		return UUID.randomUUID().toString();
	}

	public static String getParts(String uuid, int part) {
		return uuid.split("-")[part].toUpperCase();
	}
}
