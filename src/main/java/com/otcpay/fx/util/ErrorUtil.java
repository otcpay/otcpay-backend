package com.otcpay.fx.util;

public class ErrorUtil {
	public static String getErrorTitle(String subject) {
		return "[" + subject + "] Operation failed";
	}
}
