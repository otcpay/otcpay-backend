package com.otcpay.fx.util;

import java.text.SimpleDateFormat;
import java.time.Duration;
import java.time.Instant;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

public class TimeUtil {
	public enum Type {
		ADD, MINUS
	}

	private static final SimpleDateFormat sdfDate = new SimpleDateFormat("yyyy-MM-dd");

	public static String currentTime() {
		return String.valueOf(System.currentTimeMillis());
	}

	public static String yyyymmdd() {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
		Date resultdate = new Date(System.currentTimeMillis());
		return sdf.format(resultdate);
	}

	public static String currentTimeWithOffset(Type type, long offset) {
		if (type.equals(Type.ADD)) {
			return String.valueOf(System.currentTimeMillis() + offset);
		} else if (type.equals(Type.MINUS)) {
			return String.valueOf(System.currentTimeMillis() - offset);
		}

		return currentTime();
	}

	public static String getNDateBeforeAsString(int numOfDay) {
		Date now = Date.from(Instant.now().minus(Duration.ofDays(numOfDay)));
		return getDateString(now);
	}

	public static String getDateString(Date time) {
		return sdfDate.format(time);
	}

	public static String getNMonthEpochTime(int numOfMonth) {
		Calendar cal = Calendar.getInstance(TimeZone.getTimeZone("UTC"));
		setTimeToInitial(cal);
		cal.add(Calendar.MONTH, numOfMonth);
		return String.valueOf(cal.getTimeInMillis());
	}

	public static String getNDayEpochTime(int numOfDay) {
		Calendar cal = Calendar.getInstance(TimeZone.getTimeZone("UTC"));
		setTimeToInitial(cal);
		cal.add(Calendar.DATE, numOfDay);
		return String.valueOf(cal.getTimeInMillis());
	}

	public static String getFirstDayOfMonth() {
		Calendar cal = Calendar.getInstance();
		setTimeToInitial(cal);
		cal.set(Calendar.DAY_OF_MONTH, 1);
		return String.valueOf(cal.getTimeInMillis());
	}

	public static String getFirstDayOfWeek() {
		Calendar cal = Calendar.getInstance();
		setTimeToInitial(cal);
		cal.set(Calendar.DAY_OF_WEEK, cal.getFirstDayOfWeek());
		return String.valueOf(cal.getTimeInMillis());
	}

	private static void setTimeToInitial(Calendar cal) {
		cal.set(Calendar.HOUR_OF_DAY, 0);
		cal.clear(Calendar.MINUTE);
		cal.clear(Calendar.SECOND);
		cal.clear(Calendar.MILLISECOND);
	}
}
