package com.otcpay.fx.util.secret.impl;

import org.springframework.stereotype.Component;

import com.otcpay.fx.exception.RestOperationException;
import com.otcpay.fx.util.SystemUtil;
import com.otcpay.fx.util.secret.TokenUtil;

@Component
public class ApiKeyUtil extends TokenUtil {
	public boolean isTokenValid(String token) {
		if (SystemUtil.getEnvAsBool(SystemUtil.ENABLE_API_KEY)) {
			String[] parts = getToken(token);
			if (parts.length == 2 && parts[0].equals(Type.API_KEY.toString())) {
				String key = parts[1];
				if (token == null || token.isEmpty()) {
					throw RestOperationException.createError("Empty token provided");
				}
				String apiKey = SystemUtil.getEnv(SystemUtil.API_KEY);
				return apiKey.equals(key);
			}

			return false;
		}
		return true;
	}
}
