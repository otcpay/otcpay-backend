package com.otcpay.fx.util.secret.impl;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.function.Function;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.otcpay.fx.exception.RestOperationException;
import com.otcpay.fx.model.User;
import com.otcpay.fx.util.SystemUtil;
import com.otcpay.fx.util.secret.TokenUtil;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

@Component
public class JwtTokenUtil extends TokenUtil {
	private static final long JWT_TOKEN_VALIDITY = 8 * 60 * 60;

	public static final String USERID = "userId";
	public static final String ROLE = "role";

	@Value("${spring.application.publicKey}")
	private String secret;

	@Value("${spring.application.name}")
	private String issuer;

	public String getUsernameFromToken(String token) {
		return getClaimFromToken(token, Claims::getSubject);
	}

	public Date getExpirationDateFromToken(String token) {
		return getClaimFromToken(token, Claims::getExpiration);
	}

	public String getRoleFromToken(String token) {
		return getClaimFromToken(token, claims -> (String) claims.get(ROLE));
	}

	public String getUserIdFromToken(String token) {
		return getClaimFromToken(token, claims -> (String) claims.get(USERID));
	}

	public <T> T getClaimFromToken(String token, Function<Claims, T> claimsResolver) {
		final Claims claims = getAllClaimsFromToken(token);
		return claimsResolver.apply(claims);
	}

	private Claims getAllClaimsFromToken(String token) {
		return Jwts.parser().setSigningKey(secret).parseClaimsJws(token).getBody();
	}

	public boolean isTokenExpired(String authToken) {
		if (SystemUtil.getEnvAsBool(SystemUtil.ENABLE_TOKEN)) {
			String[] parts = getToken(authToken);
			if (parts.length == 2 && parts[0].equals(Type.BEARER.toString())) {
				String token = parts[1];
				if (token == null || token.isEmpty()) {
					throw RestOperationException.createError("Empty token provided");
				}

				final Date expiration = getExpirationDateFromToken(token);
				return expiration.before(new Date());
			}
			return true;
		}
		return false;
	}

	public boolean isAdminToken(String token) {
		if (SystemUtil.getEnvAsBool(SystemUtil.ENABLE_TOKEN) && !isTokenExpired(token)) {
			String roleClaim = getRoleFromToken(token);
			User.Type role = User.Type.valueOf(roleClaim);
			return role.equals(User.Type.ADMIN) || role.equals(User.Type.OWNER);
		} else if (!SystemUtil.getEnvAsBool(SystemUtil.ENABLE_TOKEN)) {
			return true;
		}

		return false;
	}

	public String generateToken(String username, String role, String userId) {
		Map<String, Object> claims = new HashMap<>();
		claims.put(ROLE, role);
		claims.put(USERID, userId);
		return doGenerateToken(claims, username, role);
	}

	private String doGenerateToken(Map<String, Object> claims, String subject, String role) {
		return Jwts.builder().setClaims(claims).setSubject(subject).setIssuedAt(new Date(System.currentTimeMillis()))
				.setExpiration(new Date(System.currentTimeMillis() + JWT_TOKEN_VALIDITY * 1000)).setIssuer(issuer)
				.setAudience(role).signWith(SignatureAlgorithm.HS256, secret).compact();

	}
}
