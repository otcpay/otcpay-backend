package com.otcpay.fx.util.secret;

import com.otcpay.fx.exception.RestOperationException;

public abstract class TokenUtil {
	public enum Type {
		BEARER("Bearer"), API_KEY("API-KEY");

		private final String name;

		private Type(String name) {
			this.name = name;
		}

		public boolean equalsName(String otherName) {
			return name.equals(otherName);
		}

		public String toString() {
			return this.name;
		}
	}

	public enum Provider {
		GOOGLE, FACEBOOK
	}

	public String[] getToken(String token) {
		if (token == null || token.isEmpty()) {
			throw RestOperationException.createError("Empty token provided");
		}

		String[] parts = token.split(" ");
		return parts;
	}
}
