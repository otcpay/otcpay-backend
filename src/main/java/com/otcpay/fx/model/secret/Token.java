package com.otcpay.fx.model.secret;

import com.otcpay.fx.util.secret.TokenUtil;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Token {
	private TokenUtil.Type type;
	private String token;
}
