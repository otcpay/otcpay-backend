package com.otcpay.fx.model.secret.jwt;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.otcpay.fx.model.User;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class JwtResponse {
	private String jwttoken;
	private String userId;
	private User.Type role;
}
