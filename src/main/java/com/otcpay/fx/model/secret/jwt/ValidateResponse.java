package com.otcpay.fx.model.secret.jwt;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.otcpay.fx.model.User;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class ValidateResponse {
	private String userId;
	private String username;
	private User.Type role;
}
