package com.otcpay.fx.model.common;

public class Field {
	public static final String ID = "id";
	public static final String USER_ID = "userId";
	public static final String CREATED_DATE = "createdDate";
	public static final String UPDATED_DATE = "updatedDate";
	public static final String TYPE = "type";
	public static final String STATUS = "status";
	public static final String CRYPTOCURRENCY = "crytocurrency";
}
