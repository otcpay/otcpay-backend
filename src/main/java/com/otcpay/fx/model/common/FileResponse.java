package com.otcpay.fx.model.common;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class FileResponse {
	private String selfLink;
	private String mediaLink;
	private String name;
	private long size;
	private long createdTime;
}
