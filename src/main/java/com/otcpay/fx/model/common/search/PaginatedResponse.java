package com.otcpay.fx.model.common.search;

import java.util.List;
import java.util.Map;

import com.otcpay.fx.service.common.async.AsyncNotifiableService;
import com.otcpay.fx.util.ParamUtil;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class PaginatedResponse {
	private int page;
	private int size;
	private int totalCount;

	private List<Map<String, Object>> data;

	public static PaginatedResponse constructResponse(int page, int size, String keywords, boolean refresh,
			List<Map<String, Object>> data, @SuppressWarnings("rawtypes") AsyncNotifiableService service) {
		PaginatedResponse response = new PaginatedResponse();
		response.setPage(page);
		response.setSize(size);
		response.setTotalCount(ParamUtil.isAllParamValid(keywords) ? data.size() : service.getCount(refresh));
		response.setData(data);
		return response;
	}
}
