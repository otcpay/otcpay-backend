package com.otcpay.fx.model.common;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

@EqualsAndHashCode(callSuper = true)
public class BatchResponse extends CommonResponse {
	@Getter
	@Setter
	@JsonInclude(Include.NON_NULL)
	private List<String> ids;

	@Getter
	@Setter
	@JsonInclude(Include.NON_NULL)
	private List<String> errors;

	public static BatchResponse withIds(List<String> ids) {
		return new BatchResponse(ids, null, true);
	}

	public static BatchResponse withErrors(List<String> errors) {
		return new BatchResponse(null, errors, errors == null || errors.size() == 0);
	}

	public BatchResponse(List<String> ids, List<String> errors, boolean result) {
		super(result);

		this.ids = ids;
	}
}
