package com.otcpay.fx.model.common.search;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Searchable {
	public enum Type {
		ARRAY, STRING
	}

	private String field;
	private Type type;
}
