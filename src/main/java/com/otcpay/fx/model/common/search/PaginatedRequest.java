package com.otcpay.fx.model.common.search;

import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class PaginatedRequest {
	private int page;
	private int size;
	private List<Searchable> searchableFields;
	private String fields;
	private String keywords;
	private String orderBy;
	private String direction;
}
