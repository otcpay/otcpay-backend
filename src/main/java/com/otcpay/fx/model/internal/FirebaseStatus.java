package com.otcpay.fx.model.internal;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@JsonIgnoreProperties(ignoreUnknown = true)
public class FirebaseStatus {
	private String begin;
	private String created;
	private String end;
	@JsonProperty("external_desc")
	private String externalDesc;
	private String modified;
	private int number;
	@JsonProperty("service_key")
	private String serviceKey;
	@JsonProperty("service_name")
	private String serviceName;
	private String severity;
	private String uri;
}
