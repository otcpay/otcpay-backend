package com.otcpay.fx.model.internal.Authorization;

public enum AuthorizationLevel {
	All, Editable, Viewable
}
