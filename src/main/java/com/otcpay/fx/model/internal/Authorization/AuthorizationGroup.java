package com.otcpay.fx.model.internal.Authorization;

public enum AuthorizationGroup {
	OWNER, DEALER, ADMIN
}
