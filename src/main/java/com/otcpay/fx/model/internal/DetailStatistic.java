package com.otcpay.fx.model.internal;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class DetailStatistic {
	private int total;
	private long today;
	private long lastWeek;
	private long lastTwoWeek;
	private long lastFourWeek;
	private long lastMonth;
	private long lastTwoMonth;
	private long lastThreeMonth;
	private long lastHalfYear;
	private long absToday;
	private long yesterday;
	private long twoDaysBefore;
	private long threeDaysBefore;
	private long fourDaysBefore;
	private long fiveDaysBefore;
	private long sixDaysBefore;
	private long absWeek;
	private long absMonth;
}
