package com.otcpay.fx.model.internal;

import lombok.Data;

@Data
public class LastUpdatedTime {
	private String lastUpdatedTime;
}
