package com.otcpay.fx.model.internal;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Statistic {
	public static final String DOC_NAME = "Statistic";
	public static final String SUB_DOC_NAME = "Detail";

	public enum COUNTMODE {
		INCREASE, DECREASE
	};

	private int count;
}
