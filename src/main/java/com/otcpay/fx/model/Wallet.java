package com.otcpay.fx.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.otcpay.fx.model.Advertisement.Currency;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Wallet {
	public enum SubType {
		ERC20, TRC20, Omni, NONE
	}

	private Currency type;
	private SubType subType;
	private String name;
	private String address;

	public Currency getType() {
		return type;
	}

	public void setType(Currency type) {
		this.type = type;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public SubType getSubType() {
		return subType;
	}

	public void setSubType(SubType subType) {
		this.subType = subType;
	}

}
