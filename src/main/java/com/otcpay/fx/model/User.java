package com.otcpay.fx.model;

import java.util.List;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class User {
	public enum Type {
		CUSTOMER, OWNER, DEALER, ADMIN
	}

	private String id;
	@NotBlank(message = "Username is mandatory")
	private String username;
	@NotNull(message = "Role is mandatory")
	private Type type;
	private String createdDate;
	private String updatedDate;
	@NotBlank(message = "Account is mandatory")
	private String account;
	private String whatsappId;
	private String wechatId;
	private String telegramId;
	private String googleId;
	@NotBlank(message = "Password is mandatory")
	private String password;
	private String profilePic;
	@NotBlank(message = "Region is mandatory")
	private String region;
	private boolean verified;
	private Proof idProof;
	private Proof addressProof;
	private List<BankProof> banks;
	private List<Wallet> wallets;
	private int defaultBanks;
	private int defaultWallets;
	private String notificationToken;
	private String approvedBy;
}
