package com.otcpay.fx.model;

import javax.validation.constraints.NotBlank;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class Advertisement {
	public enum Type {
		BUY, SELL
	}

	public enum Currency {
		USDT, BTC, ETH, XRP, USDC, BUSD
	}

	public enum Status {
		Listed, Expired
	}

	private String id;
	@NotBlank(message = "User Id is mandatory")
	private String userId;
	@NotBlank(message = "Quantity is mandatory")
	private String quantity;
	private String profilePic;
	private Type type;
	private Status status;
	private String name;
	private String region;
	private String contact;
	private Currency crytocurrency;
	@NotBlank(message = "Upper Limit is mandatory")
	private String upperLimit;
	@NotBlank(message = "Lower Limit is mandatory")
	private String lowerLimit;
	@NotBlank(message = "Settle Currency is mandatory")
	private String settleCurrency;
	@NotBlank(message = "Offer is mandatory")
	private String offer;
	private String createdDate;
	private String updatedDate;
	private boolean verified;
}
