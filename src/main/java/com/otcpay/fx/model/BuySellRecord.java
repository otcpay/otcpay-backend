package com.otcpay.fx.model;

import javax.validation.constraints.NotBlank;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.otcpay.fx.model.Advertisement.Currency;
import com.otcpay.fx.model.Advertisement.Type;

import lombok.Data;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class BuySellRecord {
	public enum Status {
		Processing, Matched, Cancelled
	}

	private String id;
	private boolean settled;
	@NotBlank(message = "Amount is mandatory")
	private String amount;
	@NotBlank(message = "Offer rate is mandatory")
	private String offerRate;
	@NotBlank(message = "Total cost is mandatory")
	private String totalCost;
	@NotBlank(message = "User Id is mandatory")
	private String userId;
	private String adId;
	@NotBlank(message = "Settle Currency is mandatory")
	private String settleCurrency;
	private String name;
	private String contact;
	private String settleRate;
	private String createdDate;
	private String updatedDate;

	private Type type;
	private Status status;
	private Currency crytocurrency;
}
