package com.otcpay.fx.model;

import javax.validation.constraints.NotBlank;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.otcpay.fx.model.Settlement.Type;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class InvoicePayment {
	public enum ContactMethod {
		whatsapp, wechat, telegram, email
	}

	public enum SettlementMethod {
		USDT, Cash
	}

	public enum Status {
		Processing, Sent, Expired
	}

	private String id;
	@NotBlank(message = "User Id is mandatory")
	private String userId;
	private String name;
	private String contact;
	private Type type;
	@NotBlank(message = "Receive Amount is mandatory")
	private String receivedAmount;
	@NotBlank(message = "Send From is mandatory")
	private String sendFrom;
	@NotBlank(message = "Send To is mandatory")
	private String sendTo;
	@NotBlank(message = "Receive Currency is mandatory")
	private String receivedCurrency;
	@NotBlank(message = "Send Currency is mandatory")
	private String sendCurrency;
	@NotBlank(message = "Amount Needed is mandatory")
	private String amountNeeded;
	private String amountSaved;
	@NotBlank(message = "Reference rate is mandatory")
	private String referenceRate;
	@NotBlank(message = "Receive Name is mandatory")
	private String receiveName;
	@NotBlank(message = "Receive Bank is mandatory")
	private String receiveBank;
	@NotBlank(message = "Receive Bank Number is mandatory")
	private String receiveBankNumber;
	private String receiveBankAddress;
	private String invoiceImage;
	private String bankImage;
	private ContactMethod contactMethod;
	private SettlementMethod settlementMethod;
	private Status status;
	private String createdDate;
	private String updatedDate;
}
