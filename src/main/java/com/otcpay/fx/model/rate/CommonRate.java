package com.otcpay.fx.model.rate;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class CommonRate {
	private Object fee;
	private double rate;
	private String source;
	private String targetCurrency;
	private String targetCountry;
	private String sourceCurrency;
	private String sourceCountry;
	private String time;

	@Override
	public String toString() {
		return String.format("[source=%s, sourceCurrency=%s, targetCurrency=%s]", source, sourceCurrency,
				targetCurrency);
	}
}
