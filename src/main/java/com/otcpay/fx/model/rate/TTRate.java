package com.otcpay.fx.model.rate;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class TTRate {
	private String cashBuy;
	private String cashSell;
	private String time;
	private String ttSell;
	private String ttBuy;
	private String targetCurrency;
	private String targetCountry;
	private String sourceCurrency;
	private String sourceCountry;
	private String source;
	private int order;

	@Override
	public String toString() {
		return String.format("[source=%s, sourceCurrency=%s, targetCurrency=%s]", source, sourceCurrency,
				targetCurrency);
	}
}
