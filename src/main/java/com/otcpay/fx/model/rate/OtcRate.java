package com.otcpay.fx.model.rate;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@JsonInclude(Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class OtcRate {
	public enum Type {
		BUY, SELL
	}

	private double rate;
	private String source;
	private String sourceCurrency;
	private String targetCurrency;
	private String time;
	private int order;
	private double otcBoardRate;
	private boolean defaultRate;
	private Type type;
}
