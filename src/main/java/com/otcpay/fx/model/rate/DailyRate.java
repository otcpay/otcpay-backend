package com.otcpay.fx.model.rate;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@JsonInclude(Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class DailyRate {
	public enum Type {
		BUY, SELL
	}

	private String fromCurrency;
	private String toCurrency;
	private double otcBoardRate;
	private boolean defaultRate;

	private List<Source> rates;
}
