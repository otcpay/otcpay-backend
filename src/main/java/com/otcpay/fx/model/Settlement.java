package com.otcpay.fx.model;

import javax.validation.constraints.NotBlank;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class Settlement {
	public enum Type {
		B2C, B2B, OTC, DIRECT
	}

	public enum SubType {
		Cash, USDT, BUY, SELL
	}

	private String id;
	@NotBlank(message = "User Id is mandatory")
	private String userId;
	@NotBlank(message = "External Id is mandatory")
	private String externalId;
	private Type type;
	private boolean settled;
	@NotBlank(message = "Final Rate is mandatory")
	private String finalRate;
	@NotBlank(message = "Final Amount is mandatory")
	private String finalAmount;
	@NotBlank(message = "Cost Rate is mandatory")
	private String costRate;
	@NotBlank(message = "Receive Currency is mandatory")
	private String receivedCurrency;
	@NotBlank(message = "Send Currency is mandatory")
	private String sendCurrency;
	private SubType settlementType;
	private String settledTime;
	private String createdDate;
	private String updatedDate;
}
