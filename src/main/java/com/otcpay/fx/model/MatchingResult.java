package com.otcpay.fx.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class MatchingResult extends BuySellRecord {
	private String adUserName;
	private String adContact;
	private String adUserId;

	public String getAdUserName() {
		return adUserName;
	}

	public void setAdUserName(String adUserName) {
		this.adUserName = adUserName;
	}

	public String getAdContact() {
		return adContact;
	}

	public void setAdContact(String adContact) {
		this.adContact = adContact;
	}

	public String getAdUserId() {
		return adUserId;
	}

	public void setAdUserId(String adUserId) {
		this.adUserId = adUserId;
	}

}
