package com.otcpay.fx.util;

import org.junit.Assert;
import org.junit.Rule;
import org.junit.Test;
import org.junit.contrib.java.lang.system.EnvironmentVariables;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.test.util.ReflectionTestUtils;

import com.otcpay.fx.model.User;
import com.otcpay.fx.util.secret.TokenUtil;
import com.otcpay.fx.util.secret.impl.JwtTokenUtil;

@RunWith(MockitoJUnitRunner.class)
public class JwtTokenUtilTest {
	@Rule
	public final EnvironmentVariables environmentVariables = new EnvironmentVariables();

	@InjectMocks
	private JwtTokenUtil util = new JwtTokenUtil();

	@Test
	public void testGenerateToken() {
		ReflectionTestUtils.setField(util, "secret", "superSecret");

		String email = "test@com";
		String userId = "test-id";
		String token = TokenUtil.Type.BEARER.toString() + " "
				+ util.generateToken(email, User.Type.ADMIN.name(), userId);
		Assert.assertTrue(util.isAdminToken(token));
		Assert.assertFalse(util.isTokenExpired(token));

		String[] authToken = util.getToken(token);
		Assert.assertTrue(authToken[0].equals(TokenUtil.Type.BEARER.toString()));

		Assert.assertTrue(User.Type.valueOf(util.getRoleFromToken(authToken[1])).equals(User.Type.ADMIN));
		Assert.assertTrue(util.getUsernameFromToken(authToken[1]).equals(email));
		Assert.assertTrue(util.getUserIdFromToken(authToken[1]).equals(userId));
	}

	@Test
	public void testIsTokenExpired() {
		environmentVariables.set(SystemUtil.ENABLE_TOKEN, "true");

		ReflectionTestUtils.setField(util, "secret", "superSecret");

		String email = "test@com";
		String token = TokenUtil.Type.BEARER.toString() + " "
				+ util.generateToken(email, User.Type.ADMIN.name(), "test-id");

		Assert.assertFalse(util.isTokenExpired(token));
		Assert.assertTrue(util.isTokenExpired(TokenUtil.Type.API_KEY.toString() + " "
				+ util.generateToken(email, User.Type.ADMIN.name(), "test-id")));
		Assert.assertTrue(util.isTokenExpired(util.generateToken(email, User.Type.ADMIN.name(), "test-id")));
	}

	@Test
	public void testIsAdminToken() {
		environmentVariables.set(SystemUtil.ENABLE_TOKEN, "true");

		ReflectionTestUtils.setField(util, "secret", "superSecret");

		String email = "test@com";
		Assert.assertFalse(util.isAdminToken(util.generateToken(email, User.Type.ADMIN.name(), "test-id")));
	}
}
