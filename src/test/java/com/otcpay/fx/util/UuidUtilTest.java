package com.otcpay.fx.util;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.junit.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class UuidUtilTest {
	@Test
	public void testGetPart() {
		Assert.assertEquals("80D5", UuidUtil.getParts("0018f0e7-80d5-45c8-a536-3133daaa6d77", 1));
		Assert.assertEquals("0018F0E7", UuidUtil.getParts("0018f0e7-8095-45c8-a536-3133daaa6d77", 0));
	}

	@Test
	public void testGetPart2() {
		String uuid = UuidUtil.generate();
		Assert.assertEquals(4, UuidUtil.getParts(uuid, 1).length());
		Assert.assertEquals(8, UuidUtil.getParts(uuid, 0).length());
	}
}
