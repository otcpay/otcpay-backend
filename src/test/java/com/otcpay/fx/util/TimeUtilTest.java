package com.otcpay.fx.util;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.junit.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class TimeUtilTest {

	@Test
	public void testOffsetTime() {
		long offset = 500;

		String currentTime = TimeUtil.currentTime();
		String offsetedTime = TimeUtil.currentTimeWithOffset(TimeUtil.Type.ADD, offset);
		long diff = Long.valueOf(offsetedTime) - Long.valueOf(currentTime);
		Assert.assertTrue(diff >= offset || diff > 400);

		currentTime = TimeUtil.currentTime();
		offsetedTime = TimeUtil.currentTimeWithOffset(TimeUtil.Type.MINUS, offset);
		diff = Long.valueOf(offsetedTime) - Long.valueOf(currentTime);
		Assert.assertTrue(diff <= offset || diff > 400);
	}

	@Test
	public void testYYYYMMDD() {
		String yyyymmdd = TimeUtil.yyyymmdd();
		Assert.assertEquals(8, yyyymmdd.length());
	}
}
