package com.otcpay.fx.util;

import java.util.HashSet;
import java.util.Set;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.junit.MockitoJUnitRunner;

import com.otcpay.fx.model.User;

@RunWith(MockitoJUnitRunner.class)
public class ParamUtilTest {

	@Test
	public void testParamValidationSuccess() {
		Assert.assertTrue(ParamUtil.isEitherParamValid("", "abc"));
		Assert.assertTrue(ParamUtil.isEitherParamValid("def", "abc"));
		Assert.assertTrue(ParamUtil.isAllParamValid("123", "456"));
		Assert.assertTrue(ParamUtil.isEitherParamValid("!!!"));
	}

	@Test
	public void testParamValidationFail() {
		Assert.assertFalse(ParamUtil.isAllParamValid("", "abc"));
		Assert.assertFalse(ParamUtil.isEitherParamValid("", ""));
	}

	@Test
	public void testCommaValidateFunction() {
		Assert.assertTrue(ParamUtil.isValidCommaSeparated("id,test"));
		Assert.assertTrue(ParamUtil.isValidCommaSeparated("id"));

		Assert.assertFalse(ParamUtil.isValidCommaSeparated("id.ddd"));
		Assert.assertFalse(ParamUtil.isValidCommaSeparated(",id,ddd"));
		Assert.assertFalse(ParamUtil.isValidCommaSeparated("id,ddd,"));
	}

	@Test
	public void testFieldValidationSuccess() {
		Set<String> fields = new HashSet<String>() {
			private static final long serialVersionUID = 1L;
			{
				add("id");
				add("createdDate");
				add("updatedDate");
			}
		};
		Assert.assertTrue(ParamUtil.isAllFieldsValid(User.class.getDeclaredFields(), fields));
	}

	@Test
	public void testFieldValidationFail() {
		Set<String> fields = new HashSet<String>() {
			private static final long serialVersionUID = 1L;
			{
				add("id");
				add("createdDate");
				add("unknown");
			}
		};
		Assert.assertFalse(ParamUtil.isAllFieldsValid(User.class.getDeclaredFields(), fields));
	}

	@Test
	public void testForbiddenFieldValidation() {
		Set<String> fields = new HashSet<String>() {
			private static final long serialVersionUID = 1L;
			{
				add("id");
				add("createdDate");
				add("unknown");
			}
		};
		Assert.assertTrue(ParamUtil.includeForbiddenFields(fields));

		fields = new HashSet<String>() {
			private static final long serialVersionUID = 1L;
			{
				add("name");
				add("title");
				add("body");
			}
		};
		Assert.assertFalse(ParamUtil.includeForbiddenFields(fields));
	}
}
