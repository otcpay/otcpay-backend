package com.otcpay.fx.util;

import org.junit.Assert;
import org.junit.Rule;
import org.junit.Test;
import org.junit.contrib.java.lang.system.EnvironmentVariables;
import org.junit.runner.RunWith;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.http.HttpStatus;

import com.otcpay.fx.exception.RestOperationException;

@RunWith(MockitoJUnitRunner.class)
public class SystemUtilTest {
	@Rule
	public final EnvironmentVariables environmentVariables = new EnvironmentVariables();

	@Test
	public void testGetEnvVar() {
		environmentVariables.set(SystemUtil.ENABLE_API_KEY, "true");
		Assert.assertEquals("true", SystemUtil.getEnv(SystemUtil.ENABLE_API_KEY));
		Assert.assertEquals(true, SystemUtil.getEnvAsBool(SystemUtil.ENABLE_API_KEY));

		environmentVariables.set("count", "1");
		Assert.assertEquals("1", SystemUtil.getEnv("count"));
		Assert.assertEquals(1, SystemUtil.getEnvAsInt("count"));
	}

	@Test
	public void testNullKey() {
		try {
			SystemUtil.getEnv(null);
		} catch (RestOperationException e) {
			Assert.assertEquals(e.getStatus(), HttpStatus.INTERNAL_SERVER_ERROR);
			Assert.assertEquals(e.getMessage(), "ENV Key cannot be null");
		}
	}

	@Test
	public void testStringToInt() {
		environmentVariables.set("count", "abc");

		try {
			SystemUtil.getEnvAsInt("count");
		} catch (RestOperationException e) {
			Assert.assertEquals(e.getStatus(), HttpStatus.INTERNAL_SERVER_ERROR);
			Assert.assertTrue(e.getMessage().contains("Failed to convert Env Var of key"));
		}
	}

	@Test
	public void testStringToArray() {
		environmentVariables.set("count", "abc");

		try {
			SystemUtil.getEnv("count", String[].class);
		} catch (RestOperationException e) {
			Assert.assertEquals(e.getStatus(), HttpStatus.INTERNAL_SERVER_ERROR);
			Assert.assertTrue(e.getMessage().contains("Failed to convert Env Var of key"));
		}
	}
}
