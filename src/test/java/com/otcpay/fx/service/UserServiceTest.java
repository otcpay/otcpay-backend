package com.otcpay.fx.service;

import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;

import java.util.HashMap;
import java.util.Map;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import com.otcpay.fx.model.User;
import com.otcpay.fx.model.common.Field;

@RunWith(MockitoJUnitRunner.class)
public class UserServiceTest {
	@InjectMocks
	private UserService service = new UserService();

	@Mock
	private BCryptPasswordEncoder encoder;

	@Test
	public void testPopulateFieldsWithMapDataSuccess() {
		String pw = "123456";
		when(encoder.encode(anyString())).thenReturn(new BCryptPasswordEncoder().encode(pw));

		Map<String, Object> maps = new HashMap<String, Object>();
		maps.put(UserService.TYPE, User.Type.ADMIN.name());
		maps.put(UserService.PASSWORD, pw);
		service.populateFields(maps);

		Assert.assertFalse(((String) maps.get(UserService.PASSWORD)).contains(pw));
		Assert.assertTrue(((String) maps.get(Field.ID)).length() > 0);
		Assert.assertTrue(((String) maps.get(Field.CREATED_DATE)).length() > 0);
		Assert.assertTrue(((String) maps.get(Field.UPDATED_DATE)).length() > 0);
		Assert.assertEquals(-1, (int) maps.get(UserService.DEFAULT_BANK));
		Assert.assertEquals(-1, (int) maps.get(UserService.DEFAULT_WALLET));
	}

	@Test
	public void testPopulateFieldsWithUserObjectSuccess() {
		String pw = "123456";
		when(encoder.encode(anyString())).thenReturn(new BCryptPasswordEncoder().encode(pw));

		User user = new User();
		user.setType(User.Type.ADMIN);
		user.setPassword(pw);
		service.populateFields(user);

		Assert.assertFalse(user.getPassword().contains(pw));
		Assert.assertTrue(user.getId().length() > 0);
		Assert.assertTrue(user.getCreatedDate().length() > 0);
		Assert.assertTrue(user.getUpdatedDate().length() > 0);
		Assert.assertEquals(-1, user.getDefaultBanks());
		Assert.assertEquals(-1, user.getDefaultWallets());
	}

}
