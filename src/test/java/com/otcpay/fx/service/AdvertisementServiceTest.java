package com.otcpay.fx.service;

import java.util.HashMap;
import java.util.Map;

import org.junit.Assert;
import org.junit.Test;
import org.mockito.InjectMocks;

import com.otcpay.fx.model.Advertisement;
import com.otcpay.fx.model.common.Field;

public class AdvertisementServiceTest {
	@InjectMocks
	private AdvertisementService service = new AdvertisementService();

	@Test
	public void testPopulateFieldsWithMapDataSuccess() {
		Map<String, Object> maps = new HashMap<String, Object>();
		service.populateFields(maps);

		Assert.assertTrue(((String) maps.get(Field.ID)).length() > 0);
		Assert.assertTrue(((String) maps.get(Field.CREATED_DATE)).length() > 0);
		Assert.assertTrue(((String) maps.get(Field.UPDATED_DATE)).length() > 0);
		Assert.assertEquals(maps.get(AdvertisementService.STATUS), Advertisement.Status.Listed);
	}

	@Test
	public void testPopulateFieldsWithUserObjectSuccess() {
		Advertisement advertisement = new Advertisement();
		service.populateFields(advertisement);

		Assert.assertTrue(advertisement.getId().length() > 0);
		Assert.assertTrue(advertisement.getCreatedDate().length() > 0);
		Assert.assertTrue(advertisement.getUpdatedDate().length() > 0);
		Assert.assertEquals(advertisement.getStatus(), Advertisement.Status.Listed);
	}
}
