package com.otcpay.fx.service;

import java.util.HashMap;
import java.util.Map;

import org.junit.Assert;
import org.junit.Test;
import org.mockito.InjectMocks;

import com.otcpay.fx.model.Settlement;
import com.otcpay.fx.model.common.Field;

public class SettlementServiceTest {
	@InjectMocks
	private SettlementService service = new SettlementService();

	@Test
	public void testPopulateFieldsWithMapDataSuccess() {
		Map<String, Object> maps = new HashMap<String, Object>();
		service.populateFields(maps);

		Assert.assertTrue(((String) maps.get(Field.ID)).length() > 0);
		Assert.assertTrue(((String) maps.get(Field.CREATED_DATE)).length() > 0);
		Assert.assertTrue(((String) maps.get(Field.UPDATED_DATE)).length() > 0);
	}

	@Test
	public void testPopulateFieldsWithUserObjectSuccess() {
		Settlement settlement = new Settlement();
		service.populateFields(settlement);

		Assert.assertTrue(settlement.getId().length() > 0);
		Assert.assertTrue(settlement.getCreatedDate().length() > 0);
		Assert.assertTrue(settlement.getUpdatedDate().length() > 0);
	}
}
