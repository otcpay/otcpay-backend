package com.otcpay.fx.service;

import java.util.HashMap;
import java.util.Map;

import org.junit.Assert;
import org.junit.Test;
import org.mockito.InjectMocks;

import com.otcpay.fx.model.BuySellRecord;
import com.otcpay.fx.model.common.Field;

public class BuySellRecordServiceTest {
	@InjectMocks
	private BuySellRecordService service = new BuySellRecordService();

	@Test
	public void testPopulateFieldsWithMapDataSuccess() {
		Map<String, Object> maps = new HashMap<String, Object>();
		service.populateFields(maps);

		Assert.assertTrue(((String) maps.get(Field.ID)).length() > 0);
		Assert.assertTrue(((String) maps.get(Field.CREATED_DATE)).length() > 0);
		Assert.assertTrue(((String) maps.get(Field.UPDATED_DATE)).length() > 0);
		Assert.assertEquals(maps.get(BuySellRecordService.STATUS), BuySellRecord.Status.Processing);
	}

	@Test
	public void testPopulateFieldsWithUserObjectSuccess() {
		BuySellRecord buySellRecord = new BuySellRecord();
		service.populateFields(buySellRecord);

		Assert.assertTrue(buySellRecord.getId().length() > 0);
		Assert.assertTrue(buySellRecord.getCreatedDate().length() > 0);
		Assert.assertTrue(buySellRecord.getUpdatedDate().length() > 0);
		Assert.assertEquals(buySellRecord.getStatus(), BuySellRecord.Status.Processing);
	}
}
