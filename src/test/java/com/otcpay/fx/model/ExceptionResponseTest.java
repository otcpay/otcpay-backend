package com.otcpay.fx.model;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.junit.MockitoJUnitRunner;

import com.otcpay.fx.exception.ExceptionResponse;

@RunWith(MockitoJUnitRunner.class)
public class ExceptionResponseTest {

	@Test
	public void testEqual() {
		ExceptionResponse res1 = new ExceptionResponse("test");
		ExceptionResponse res2 = new ExceptionResponse("test");

		Assert.assertEquals(res1, res2);
		Assert.assertEquals(res1.hashCode(), res2.hashCode());
		Assert.assertTrue(res1.equals(res2));
	}

	@Test
	public void testNotEqual() {
		ExceptionResponse res1 = new ExceptionResponse("test");
		ExceptionResponse res2 = new ExceptionResponse("test2");

		Assert.assertNotEquals(res1, res2);
		Assert.assertNotEquals(res1.hashCode(), res2.hashCode());
		Assert.assertFalse(res1.equals(res2));
	}
}
